package api;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.json.JSONObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MyRunnable implements Runnable {

	ModelData responseData;

	public static int userCount = 1;

	// this is storage path for store files
	public static String Storege_folder_path = "D:\\AAAA\\";

	static String site_url = "https://forms.gov.il/globaldata/getsequence/gethtmlform.aspx?formType=driverapply@police.gov.il";

	MyRunnable(ModelData responseData) {
		this.responseData = responseData;
	}

	@Override
	public void run() {
		// create folder
		methodsclass.Create_folder_and_store_api_response(responseData);
		try {
			FileWriter writer = new FileWriter(
					Storege_folder_path + responseData.getStep1().getReportNumber() + "\\"
							+ responseData.getStep1().getReportNumber() + "_Automation_inserted_data_in_website.txt",
					true);
			BufferedWriter bufferedWriter2 = new BufferedWriter(writer);

			System.err.println("API response Total User Count " + userCount);
			System.err
					.println("API response saved in hasmap values firstId" + responseData.getStep1().getReportNumber());
			System.err.println("===========================================");

			methodsclass.path = System.getProperty("user.dir");
			System.out.println(methodsclass.path);
			System.setProperty("webdriver.chrome.driver", methodsclass.path + "\\dependency\\chromedriver.exe");

			WebDriver driver = new ChromeDriver();
			WebDriverWait wait = new WebDriverWait(driver, 20);
			driver.navigate().to(site_url);
			// String site_url_get = driver.getCurrentUrl();
			driver.manage().window().maximize();
			driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			// this methode is use to check site is open successfully or not.
			methodsclass.check_site_loaded_successfully_or_not(driver, wait, bufferedWriter2, responseData);

			handleResponseDataWithValidationInsert(driver, wait, bufferedWriter2, responseData);
			// closeing the driver
			// driver.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private static void handleResponseDataWithValidationInsert(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData responseData) throws IOException, InterruptedException {
		if (CallApi.statusdata == 200) {
			System.out.println("got valid status code as 200");

			try {
				// if internet is not connected then when internet get back then
				// this
				// condition will refresh the page to continue the automation.
				if (CallApi.refreshpage == true) {
					driver.navigate().refresh();
					System.out.println("Refresh the page successfully");
					CallApi.refreshpage = false;
				}

				// step 1 // Checked validation
				String report_number_validation = methodsclass.insert_report_number_experiment(driver, wait,
						bufferedWriter2, responseData, responseData.getStep1().getReportNumber());

				// //11151555932

				methodsclass.Insert_referel_experiment(driver, wait, bufferedWriter2, responseData,
						responseData.getStep1().getReferral());

				methodsclass.select_checkbox(driver, wait, bufferedWriter2);

				methodsclass.click_next(driver, wait, bufferedWriter2);

				// steps 2
				methodsclass.select_Type_of_applicant(driver, wait, bufferedWriter2, responseData,
						Integer.parseInt(responseData.getStep2().getTypeOfApplicant()));

				methodsclass.select_identity_type_of_report_holder(driver, wait, bufferedWriter2, responseData,
						Integer.parseInt(responseData.getStep2().getIdentityTypeOfReportHolder()));

				String ID_holder_of_the_report_validation = methodsclass.Insert_ID_holder_of_the_report(driver, wait,
						bufferedWriter2, responseData, responseData.getStep2().getIDHolderOfTheReport());// 029036514

				String HF_holder_of_the_report_validation = methodsclass.Insert_HF_holder_of_the_report(driver, wait,
						bufferedWriter2, responseData, responseData.getStep2().getHFHolderOfTheReport());

				String company_name_of_the_report_holder_validation = methodsclass
						.Insert_company_name_of_the_report_holder(driver, wait, bufferedWriter2, responseData,
								responseData.getStep2().getCompanyNameOfTheReportHolder());

				String first_name_of_the_report_owner_validation = methodsclass.insert_first_name_of_the_report_owner(
						driver, wait, bufferedWriter2, responseData,
						responseData.getStep2().getFirstNameOfTheReportOwner());

				String last_name_of_the_report_owner_validation = methodsclass.insert_last_name_of_the_report_owner(
						driver, wait, bufferedWriter2, responseData,
						responseData.getStep2().getLastNameOfTheReportOwner());

				String report_driver_license_number_validation = methodsclass.insert_report_driver_license_number(
						driver, wait, bufferedWriter2, responseData,
						responseData.getStep2().getReportDriverLicenseNumber());

				methodsclass.select_Resettlement_option1(driver, wait, bufferedWriter2, responseData,
						responseData.getStep2().getResettlement());

				methodsclass.insert_street(driver, wait, bufferedWriter2, responseData,
						responseData.getStep2().getStreet());

				String House_number_validation = methodsclass.insert_House_number(driver, wait, bufferedWriter2,
						responseData, responseData.getStep2().getHouseNumber());

				String apartment_number_validation = methodsclass.insert_apartment_number(driver, wait, bufferedWriter2,
						responseData, responseData.getStep2().getApartmentNumber());

				String zip_number_validation = methodsclass.insert_zip(driver, wait, bufferedWriter2, responseData,
						responseData.getStep2().getZip());
				// below methode dont remove
				// methodsclass.insert_other_address(driver, wait,
				// bufferedWriter2, responseData,
				// responseData.getStep2().getOtherAddress());

				String phone_number_validation = methodsclass.insert_phone_num(driver, wait, bufferedWriter2,
						responseData, responseData.getStep2().getPhone());

				String mobile_phone_validation = methodsclass.insert_mobile_phone(driver, wait, bufferedWriter2,
						responseData, responseData.getStep2().getMobilePhone());

				String mail_validation = methodsclass.insert_mail(driver, wait, bufferedWriter2, responseData,
						responseData.getStep2().getMail());

				// below code is use to set the error variable value as null
				String pra_ID_holder_of_the_report_validation = "";
				String pra_HF_holder_of_the_report_validation = "";
				String pra_passport_validation = "";
				String pra_company_name_of_the_report_holder_validation = "";
				String pra_first_name_of_the_report_owner_validation = "";
				String pra_last_name_of_the_report_owner_validation = "";
				String pra_report_driver_license_number_validation = "";
				String pra_resettlement_validation = "";
				String pra_street_validation = "";
				String pra_house_number_validation = "";
				String pra_apartment_number_validation = "";
				String pra_zip_validation = "";
				String pra_other_address_validation = "";
				String pra_phone_validation = "";
				String pra_mobile_phone_validation = "";
				String pra_mail_validation = "";

				// step 2 new data insert // below condition is old condition
				// if(responseData.getStep2().isPra_identity_type_of_report_holder_data_present()
				// == true){
				if (responseData.getStep2().getPra_identity_type_of_report_holder_data() != "") {

					methodsclass.insert_pra_identity_type_of_report_holder(driver, wait, bufferedWriter2, responseData,
							Integer.parseInt(responseData.getStep2().getPra_identity_type_of_report_holder_data()));

					pra_ID_holder_of_the_report_validation = methodsclass.insert_pra_ID_holder_of_the_report(driver,
							wait, bufferedWriter2, responseData,
							responseData.getStep2().getPra_ID_holder_of_the_report_data());

					pra_HF_holder_of_the_report_validation = methodsclass.insert_pra_HF_holder_of_the_report(driver,
							wait, bufferedWriter2, responseData,
							responseData.getStep2().getPra_HF_holder_of_the_report_data());

					pra_passport_validation = methodsclass.insert_pra_passport(driver, wait, bufferedWriter2,
							responseData, responseData.getStep2().getPra_passport_data());

					pra_company_name_of_the_report_holder_validation = methodsclass
							.insert_pra_company_name_of_the_report_holder(driver, wait, bufferedWriter2, responseData,
									responseData.getStep2().getPra_company_name_of_the_report_holder_data());

					pra_first_name_of_the_report_owner_validation = methodsclass
							.insert_pra_first_name_of_the_report_owner(driver, wait, bufferedWriter2, responseData,
									responseData.getStep2().getPra_first_name_of_the_report_owner_data());

					pra_last_name_of_the_report_owner_validation = methodsclass
							.insert_pra_last_name_of_the_report_owner(driver, wait, bufferedWriter2, responseData,
									responseData.getStep2().getPra_last_name_of_the_report_owner_data());

					pra_report_driver_license_number_validation = methodsclass.insert_pra_report_driver_license_number(
							driver, wait, bufferedWriter2, responseData,
							responseData.getStep2().getPra_report_driver_license_number_data());

					pra_resettlement_validation = methodsclass.insert_pra_resettlement(driver, wait, bufferedWriter2,
							responseData, responseData.getStep2().getPra_resettlement_data());

					pra_street_validation = methodsclass.insert_pra_street(driver, wait, bufferedWriter2, responseData,
							responseData.getStep2().getPra_street_data());

					pra_house_number_validation = methodsclass.insert_pra_house_number(driver, wait, bufferedWriter2,
							responseData, responseData.getStep2().getPra_house_number_data());

					pra_apartment_number_validation = methodsclass.insert_pra_apartment_number(driver, wait,
							bufferedWriter2, responseData, responseData.getStep2().getPra_apartment_number_data());

					pra_zip_validation = methodsclass.insert_pra_zip(driver, wait, bufferedWriter2, responseData,
							responseData.getStep2().getPra_zip_data());

					pra_other_address_validation = methodsclass.insert_pra_other_address(driver, wait, bufferedWriter2,
							responseData, responseData.getStep2().getPra_other_address_data());

					pra_phone_validation = methodsclass.insert_pra_phone_holder(driver, wait, bufferedWriter2,
							responseData, responseData.getStep2().getPra_phone_data());

					pra_mobile_phone_validation = methodsclass.insert_pra_mobile_phone(driver, wait, bufferedWriter2,
							responseData, responseData.getStep2().getPra_mobile_phone_data());

					pra_mail_validation = methodsclass.insert_pra_mail(driver, wait, bufferedWriter2, responseData,
							responseData.getStep2().getPra_mail_data());

					methodsclass.click_next(driver, wait, bufferedWriter2);
				} else {
					System.out.println("------------------------------");
					System.out.println("Stpe 2_pra data is not present");
					System.out.println("------------------------------");
					methodsclass.click_next(driver, wait, bufferedWriter2);
				}

				// // step 3
				// System.out.println("-------is Exxsit or not "+
				// response.isStepThreeExsist());

				String step_3_last_name_validation1 = "";
				String step_3_first_name_validation = "";
				String step_3_attorney_license_number_validation = "";
				String step_3_house_number_validation = "";
				String step_3_apartment_number_validation = "";
				String step_3_zip_number_validation = "";
				String step_3_phone_number_validation = "";
				String step_3_mobile_phone_validation = "";
				String step_3_mail_validation = "";

				if (responseData.isStepThreeExsist()) {

					// // System.out.println("this is jigesh data
					// =>"+responseData.getStep3().getLastName());
					// Checked validation

					step_3_last_name_validation1 = methodsclass.insert_step_3_last_name(driver, wait, bufferedWriter2,
							responseData, responseData.getStep3().getLastName());
					// Checked validation
					step_3_first_name_validation = methodsclass.insert_step_3_first_name(driver, wait, bufferedWriter2,
							responseData, responseData.getStep3().getFirstName());
					// Checked validation
					step_3_attorney_license_number_validation = methodsclass.insert_step_3_attorney_license_number(
							driver, wait, bufferedWriter2, responseData,
							responseData.getStep3().getAttorneyLicenseNumber());

					methodsclass.insert_step_3_resettlement(driver, wait, bufferedWriter2, responseData,
							responseData.getStep3().getResettlement());

					methodsclass.insert_step_3_street(driver, wait, bufferedWriter2, responseData,
							responseData.getStep3().getStreet());
					// Checked validation
					step_3_house_number_validation = methodsclass.insert_step_3_house_number(driver, wait,
							bufferedWriter2, responseData, responseData.getStep3().getHouseNumber());
					// Checked validation
					step_3_apartment_number_validation = methodsclass.insert_step_3_apartment_number(driver, wait,
							bufferedWriter2, responseData, responseData.getStep3().getApartmentNumber());
					// Checked validation
					step_3_zip_number_validation = methodsclass.insert_step_3_zip(driver, wait, bufferedWriter2,
							responseData, responseData.getStep3().getZip());

					methodsclass.insert_step_3_other_address(driver, wait, bufferedWriter2, responseData,
							responseData.getStep3().getOtherAddress());
					// Checked validation
					step_3_phone_number_validation = methodsclass.insert_step_3_phone(driver, wait, bufferedWriter2,
							responseData, responseData.getStep3().getPhone());
					// Checked validation
					step_3_mobile_phone_validation = methodsclass.insert_step_3_mobile_phone(driver, wait,
							bufferedWriter2, responseData, responseData.getStep3().getMobilePhone());
					// Checked validation
					step_3_mail_validation = methodsclass.insert_step_3_mail(driver, wait, bufferedWriter2,
							responseData, responseData.getStep3().getMail());

					methodsclass.click_next(driver, wait, bufferedWriter2);
				} else {
					System.out.println("-------------------");
					System.out.println("Step 3 is not Exist");
					System.out.println("-------------------");
					bufferedWriter2.newLine();
					bufferedWriter2.write(
							"----------------------------------------------------------------------------------------");
					bufferedWriter2.newLine();
					bufferedWriter2.write(
							" STEP 3 IS NOT EXIST in REPORT NUM is :=> " + responseData.getStep1().getReportNumber());
					bufferedWriter2.newLine();
					bufferedWriter2.write(
							"----------------------------------------------------------------------------------------");
					bufferedWriter2.newLine();
				}

				String step_4_reason_detail_validation = methodsclass.insert_reason_detail(driver, wait,
						bufferedWriter2, responseData, responseData.getStep4().getReasonDetails());

				if (!responseData.getStep4().getReasonDetails().equals("")) {
					// step 4 // Checked validation
					methodsclass.click_next(driver, wait, bufferedWriter2);
				}

				/*
				 * // step 4 // Checked validation String
				 * step_4_reason_detail_validation =
				 * methodsclass.insert_reason_detail(driver, wait,
				 * bufferedWriter2, responseData,
				 * responseData.getStep4().getReasonDetails());
				 * 
				 * methodsclass.click_next(driver, wait, bufferedWriter2);
				 */

				// step 5
				System.out.println("file_photo_report_image_validation : "+responseData.getStep5().getFilePhotoReport());
				System.out.println("file_photo_id_signature_image_validation : "+responseData.getStep5().getFilePhotoIdSignature());
				System.out.println("Power_of_attorney_from_customer_data_image_validation : "+responseData.getStep5().getFilePowerOfAttorneyFromCustomer());
				System.out.println("file_signed_contract_of_the_tenant_data_image_validation : "+responseData.getStep5().getFileSignedContractOfTheTenant());
				System.out.println("file_rental_company_reporting_data_image_validation : "+responseData.getStep5().getFileRentalCompanyReporting());
				System.out.println("file_actual_driver_committe_the_offence_image_validation : "+responseData.getStep5().getFile_actual_driver_committe_the_offence_data());
				System.out.println("file_driver_license_photo_image_validation : "+responseData.getStep5().getFile_driver_license_photo_data());
				System.out.println("file_request_to_be_judged_image_validation : "+responseData.getStep5().getFile_request_to_be_judged_data());
				System.out.println("file_confirmation_of_payment_image_validation : "+responseData.getStep5().getFile_confirmation_of_payment_data());
				System.out.println("file_proof_image_validation : "+responseData.getStep5().getFile_proof_data());
				System.out.println("file_transfer_of_ownership_image_validation : "+responseData.getStep5().getFile_transfer_of_ownership_data());
				System.out.println("file_protocol_sign_by_lowyer_image_validation : "+responseData.getStep5().getFile_protocol_sign_by_lowyer_data());
				System.out.println("file_address_approval_image_validation : "+responseData.getStep5().getFile_address_approval_data());

				String file_photo_report_image_validation = methodsclass.insert_file_photo_report_image(driver, wait,bufferedWriter2, responseData);

				String file_photo_id_signature_image_validation = methodsclass.insert_file_photo_id_signature(driver,wait, bufferedWriter2, responseData);

				String Power_of_attorney_from_customer_data_image_validation = methodsclass.insert_Power_of_attorney_from_customer(driver, wait, bufferedWriter2, responseData);

				String file_signed_contract_of_the_tenant_data_image_validation = methodsclass.insert_file_signed_contract_of_the_tenant(driver, wait, bufferedWriter2, responseData);

				String file_rental_company_reporting_data_image_validation = methodsclass.insert_file_rental_company_reporting(driver, wait, bufferedWriter2, responseData);

				String file_actual_driver_committe_the_offence_image_validation = methodsclass.insert_file_actual_driver_committe_the_offence(driver, wait, bufferedWriter2, responseData);

				String file_driver_license_photo_image_validation = methodsclass.insert_file_driver_license_photo(driver, wait, bufferedWriter2, responseData);

				String file_request_to_be_judged_image_validation = methodsclass.insert_file_request_to_be_judged(driver, wait, bufferedWriter2, responseData);

				String file_confirmation_of_payment_image_validation = methodsclass.insert_file_confirmation_of_payment(driver, wait, bufferedWriter2, responseData);

				String file_proof_image_validation = methodsclass.insert_file_proof(driver, wait, bufferedWriter2,responseData);

				String file_transfer_of_ownership_image_validation = methodsclass.insert_file_transfer_of_ownership(driver, wait, bufferedWriter2, responseData);

				String file_protocol_sign_by_lowyer_image_validation = methodsclass.insert_file_protocol_sign_by_lowyer(driver, wait, bufferedWriter2, responseData);

				String file_address_approval_image_validation = methodsclass.insert_file_address_approval(driver, wait,bufferedWriter2, responseData);


				
				//after submit all data get message from dialog variable
				String form_submit_message = "";
				
				// CAll Api post method

				// Create JSON
				JSONObject REQUEST_DATA = new JSONObject();

				JSONObject step1 = new JSONObject();

				JSONObject step1_data = new JSONObject();
				step1_data.put("report_number", report_number_validation);

				step1.put("step1", step1_data);

				JSONObject step2 = new JSONObject();
				JSONObject step2_data = new JSONObject();

				step2_data.put("ID_holder_of_the_report", ID_holder_of_the_report_validation);
				step2_data.put("HF_holder_of_the_report", HF_holder_of_the_report_validation);
				step2_data.put("company_name_of_the_report_holder", company_name_of_the_report_holder_validation);
				step2_data.put("first_name_of_the_report_owner", first_name_of_the_report_owner_validation);
				step2_data.put("last_name_of_the_report_owner", last_name_of_the_report_owner_validation);
				step2_data.put("report_driver_license_number", report_driver_license_number_validation);
				step2_data.put("house_number", House_number_validation);
				step2_data.put("apartment_number", apartment_number_validation);
				step2_data.put("zip", zip_number_validation);
				step2_data.put("phone", phone_number_validation);
				step2_data.put("mobile_phone", mobile_phone_validation);
				step2_data.put("mail", mail_validation);

				step2_data.put("pra_ID_holder_of_the_report", pra_ID_holder_of_the_report_validation);
				step2_data.put("pra_HF_holder_of_the_report", pra_HF_holder_of_the_report_validation);
				step2_data.put("pra_passport", pra_passport_validation);
				step2_data.put("pra_company_name_of_the_report_holder",
						pra_company_name_of_the_report_holder_validation);
				step2_data.put("pra_first_name_of_the_report_owner", pra_first_name_of_the_report_owner_validation);
				step2_data.put("pra_last_name_of_the_report_owner", pra_last_name_of_the_report_owner_validation);
				step2_data.put("pra_report_driver_license_number", pra_report_driver_license_number_validation);
				step2_data.put("pra_resettlement", pra_resettlement_validation);
				step2_data.put("pra_street", pra_street_validation);
				step2_data.put("pra_house_number", pra_house_number_validation);
				step2_data.put("pra_apartment_number", pra_apartment_number_validation);
				step2_data.put("pra_zip", pra_zip_validation);
				step2_data.put("pra_other_address", pra_other_address_validation);
				step2_data.put("pra_phone", pra_phone_validation);
				step2_data.put("pra_mobile_phone", pra_mobile_phone_validation);
				step2_data.put("pra_mail", pra_mail_validation);

				step2.put("step2", step2_data);

				JSONObject step3 = new JSONObject();
				JSONObject step3_data = new JSONObject();

				if (responseData.isStepThreeExsist()) {

					step3_data.put("last_name", step_3_last_name_validation1);
					step3_data.put("first_name", step_3_first_name_validation);
					step3_data.put("attorney_license_number", step_3_attorney_license_number_validation);
					step3_data.put("house_number", step_3_house_number_validation);
					step3_data.put("apartment_number", step_3_apartment_number_validation);
					step3_data.put("zip", step_3_zip_number_validation);
					step3_data.put("phone", step_3_phone_number_validation);
					step3_data.put("mobile_phone", step_3_mobile_phone_validation);
					step3_data.put("mail", step_3_mail_validation);
					step3.put("step3", step3_data);

				} else {
					System.out.println("-------------------");
					System.out.println("Step 3 is not Exist");
					System.out.println("-------------------");
				}

				JSONObject step4 = new JSONObject();
				JSONObject step4_data = new JSONObject();

				step4_data.put("reason_details", step_4_reason_detail_validation);

				step4.put("step4", step4_data);

				JSONObject step5 = new JSONObject();
				JSONObject step5_data = new JSONObject();

				step5_data.put("file_photo_report", file_photo_report_image_validation);
				step5_data.put("file_photo_id_signature", file_photo_id_signature_image_validation);
				step5_data.put("file_power_of_attorney_from_customer",
						Power_of_attorney_from_customer_data_image_validation);
				step5_data.put("file_signed_contract_of_the_tenant",
						file_signed_contract_of_the_tenant_data_image_validation);
				step5_data.put("file_rental_company_reporting", file_rental_company_reporting_data_image_validation);
				step5_data.put("file_actual_driver_committe_the_offence",
						file_actual_driver_committe_the_offence_image_validation);
				step5_data.put("file_driver_license_photo", file_driver_license_photo_image_validation);
				step5_data.put("file_request_to_be_judged", file_request_to_be_judged_image_validation);
				step5_data.put("file_confirmation_of_payment", file_confirmation_of_payment_image_validation);
				step5_data.put("file_proof", file_proof_image_validation);
				step5_data.put("file_transfer_of_ownership", file_transfer_of_ownership_image_validation);
				step5_data.put("file_protocol_sign_by_lowyer", file_protocol_sign_by_lowyer_image_validation);
				step5_data.put("file_address_approval", file_address_approval_image_validation);

				step5.put("step5", step5_data);

				REQUEST_DATA.put("step1", step1_data);
				REQUEST_DATA.put("step2", step2_data);

				if (responseData.isStepThreeExsist())
					REQUEST_DATA.put("step3", step3_data);

				REQUEST_DATA.put("step4", step4_data);
				REQUEST_DATA.put("step5", step5_data);

				if (report_number_validation != "" || ID_holder_of_the_report_validation != ""
						|| HF_holder_of_the_report_validation != ""
						|| company_name_of_the_report_holder_validation != ""
						|| first_name_of_the_report_owner_validation != ""
						|| last_name_of_the_report_owner_validation != ""
						|| report_driver_license_number_validation != "" || House_number_validation != ""
						|| apartment_number_validation != "" || zip_number_validation != ""
						|| phone_number_validation != "" || mobile_phone_validation != ""
						|| pra_ID_holder_of_the_report_validation != "" || pra_HF_holder_of_the_report_validation != ""
						|| pra_passport_validation != "" || pra_company_name_of_the_report_holder_validation != ""
						|| pra_first_name_of_the_report_owner_validation != ""
						// resettlement & identity type of report holder data
						// not added here.
						|| pra_last_name_of_the_report_owner_validation != ""
						|| pra_report_driver_license_number_validation != "" || pra_street_validation != ""
						|| pra_house_number_validation != "" || pra_apartment_number_validation != ""
						|| pra_zip_validation != "" || pra_other_address_validation != "" || pra_phone_validation != ""
						|| pra_mobile_phone_validation != "" || pra_mail_validation != ""
						|| step_3_first_name_validation != "" || step_3_attorney_license_number_validation != ""
						|| step_3_house_number_validation != "" || step_3_apartment_number_validation != ""
						|| step_3_zip_number_validation != "" || step_3_phone_number_validation != ""
						|| step_3_mobile_phone_validation != "" || step_3_mail_validation != ""
						|| step_4_reason_detail_validation != "" || file_photo_report_image_validation != ""
						|| file_photo_id_signature_image_validation != ""
						|| Power_of_attorney_from_customer_data_image_validation != ""
						|| file_signed_contract_of_the_tenant_data_image_validation != ""
						|| file_rental_company_reporting_data_image_validation != ""
						|| file_actual_driver_committe_the_offence_image_validation != ""
						|| file_driver_license_photo_image_validation != ""
						|| file_request_to_be_judged_image_validation != ""
						|| file_confirmation_of_payment_image_validation != "" || file_proof_image_validation != ""
						|| file_transfer_of_ownership_image_validation != ""
						|| file_protocol_sign_by_lowyer_image_validation != ""
						|| file_address_approval_image_validation != "") {

					
					System.out.println("**************");
					System.out.println("Error Found");
					System.out.println("**************");
			//		form_submit_message = methodsclass.click_send_btn(driver, wait, bufferedWriter2);
					
					REQUEST_DATA.put("message", form_submit_message);
					methodsclass.InValidDataRequest(responseData.getId(), "error", REQUEST_DATA);
					System.out.println("Request Pramater : "+REQUEST_DATA);
				} else {
					
					System.out.println("**************");
					System.out.println("Error Not Found");
					System.out.println("**************");
					form_submit_message  = methodsclass.click_send_btn(driver, wait, bufferedWriter2);
					JSONObject SUCEESS_REQUEST_DATA = new JSONObject();
					SUCEESS_REQUEST_DATA.put("message", form_submit_message);
					methodsclass.InValidDataRequest(responseData.getId(), "success", SUCEESS_REQUEST_DATA);
					System.out.println("Request Pramater : "+SUCEESS_REQUEST_DATA);

				}
				
				System.out.println("After Submiting form get message from form  ->  : "+form_submit_message );
				
				Constants.PROGRAM_IN_EXECUTION.remove(0);
				// Constants.PROGRAM_IN_EXECUTION.clear();
				System.out.println("This is the total count is : " + Constants.ID_COUNT_LIST.size());
				System.out.println("Processing Users : " + Constants.PROGRAM_IN_EXECUTION.size());
				/*
				 * for (int i = 0; i < REQUEST_DATA.length(); i++)
				 * System.out.println(" Step  :  " + REQUEST_DATA.get(i));
				 */
				bufferedWriter2.close();
				System.out.println("bufferedwriter2 text File is created name as Automation_inserted_data_in_website");
				// TimeUnit.MINUTES.sleep(2);
				System.out.println("=====================================================");
				System.out.println("process " + Constants.processCount + " End From Here");
				System.out.println("=====================================================");

				// driver.close();
				// driver.navigate().refresh();
				// System.out.println("Website refresed successfully");
			} catch (Exception e) {
				// bufferedWriter2 closing in catch if code goes in this block.
				bufferedWriter2.newLine();
				bufferedWriter2.write(
						"BIG_ERROR :- there is something wrong in automation, from My Runnable methode user is in catch part");
				bufferedWriter2.close();
				e.printStackTrace();

				 driver.close();
			}
		} else {
			bufferedWriter2.close();
			System.err.println("=================================");
			System.err.println("status code is :" + CallApi.statusdata);
			System.err.println("automation is waitng for 2 minuts");
			System.err.println("=================================");
			// TimeUnit.MINUTES.sleep(2);
			 driver.close();
		}
	}
}