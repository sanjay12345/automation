package api;

public class ModelStep1 {

	private String reportNumber;
	private String referral;
	
	boolean report_number_data_present;
	boolean referral_data_present;

	public ModelStep1(String reportNumber, boolean report_number_data_present, String referral,
			boolean referral_data_present) {
		
		this.reportNumber = reportNumber;
		this.referral = referral;
		
		this.report_number_data_present = report_number_data_present; 
		this.referral_data_present = referral_data_present; 
	}

	public String getReportNumber() {
		return this.reportNumber;
	}
	
	public boolean isReportNumber() {
		return this.report_number_data_present;
	}

	public void setReportNumber(String reportNumber) {
		this.reportNumber = reportNumber;
	}

	public String getReferral() {
		return referral;
	}
	
	public boolean isReferral() {
		return this.referral_data_present;
	}

	public void setReferral(String referral) {
		this.referral = referral;
	}

}
