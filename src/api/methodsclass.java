package api;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.exec.ExecuteException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class methodsclass extends CallApi {
	// 7cff658398ed94cafed9a2fb6b073447_photo_report.jpg
	// 12222222333
	public static ModelData modelData;

	// this path is globle variable for get the chrome driver location.
	public static String path;
	// experiment
	static Screen screen1;

	public static String path1;
	// static String image_pathl1 =
	// "https://homepages.cae.wisc.edu/~ece533/images/airplane.png";
	// public static String get_image_name1;
	// image_downloaded_in_local_folder1 this variable is used to get the stored
	// image path in local folder.
	static String image_downloaded_in_local_folder1;

	public static String download_image1(ModelData responseData, String imagefile) throws IOException {
		// path1 = System.getProperty("user.dir");
		String get_image_name1 = "";
		URL url;
		try {
			// String str = imagefile;
			System.out.println(get_image_name1 = imagefile.substring(imagefile.lastIndexOf("/") + 1));
			url = new URL(imagefile);
			InputStream in = new BufferedInputStream(url.openStream());
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			byte[] buf = new byte[1024];
			int n = 0;
			while (-1 != (n = in.read(buf))) {
				out.write(buf, 0, n);
			}
			out.close();
			in.close();
			byte[] response = out.toByteArray();

			// valid code FileOutputStream fos = new
			// FileOutputStream("C:\\Users\\Warrior\\zzz\\"+get_image_name1);
			FileOutputStream fos = new FileOutputStream(
					"D:\\AAAA\\" + responseData.getStep1().getReportNumber() + "\\" + get_image_name1);
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			System.out.println("USER ID IS :=> " + responseData.getId());
			System.out.println("IMAGE DOWNLOADED SUCCESSFULLY, IMAGE NAME IS :=> " + get_image_name1);
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			// image_downloaded_in_local_folder1 = "D:\\AAAA\\" +
			// responseData.getStep1().getReportNumber() + "\\" +
			// get_image_name1;
			// valid code image_downloaded_in_local_folder1 =
			// "C:\\Users\\Warrior\\zzz\\"+get_image_name1;
			fos.write(response);
			fos.close();
			return get_image_name1;
		} catch (MalformedURLException e) {
			e.printStackTrace();
			System.out.println("IMAGE NOT DOWNLOADED, IMAGE NAME IS :=> " + get_image_name1);
			return "";
		}

	}

	/*
	 * // sikuli static Screen screen;
	 * 
	 * public static String path; static String image_pathl =
	 * "https://homepages.cae.wisc.edu/~ece533/images/airplane.png"; public
	 * static String get_image_name; static String
	 * image_downloaded_in_local_folder;
	 * 
	 * // Flag For Step Three is null // static boolean isStepThreeExsist =
	 * false;
	 * 
	 * public static void download_image(ModelData responseData,String
	 * imagefile) throws IOException { path1 = System.getProperty("user.dir");
	 * URL url; try { create_folder(responseData); String str = imagefile;
	 * System.out.println(get_image_name1 =
	 * image_pathl1.substring(image_pathl1.lastIndexOf("/") + 1)); url = new
	 * URL(image_pathl1); InputStream in = new
	 * BufferedInputStream(url.openStream()); ByteArrayOutputStream out = new
	 * ByteArrayOutputStream(); byte[] buf = new byte[1024]; int n = 0; while
	 * (-1 != (n = in.read(buf))) { out.write(buf, 0, n); } out.close();
	 * in.close(); byte[] response = out.toByteArray(); // FileOutputStream fos
	 * = new //
	 * FileOutputStream(path+"\\docs\\"+get_image_name);//"C:\Users\Warrior\zzz"
	 * FileOutputStream fos = new FileOutputStream("C:\\Users\\Warrior\\zzz\\" +
	 * get_image_name1); image_downloaded_in_local_folder1 =
	 * "C:\\Users\\Warrior\\zzz\\" + get_image_name1; fos.write(response);
	 * fos.close(); } catch (MalformedURLException e) {
	 * 
	 * e.printStackTrace(); }
	 * 
	 * }
	 */
	public static void Create_folder_and_store_api_response(ModelData response) {
		create_folder(response);

		/*
		 * try { FileWriter writer = new
		 * FileWriter(Storege_folder_path+report_number_data+"MyFile.txt",
		 * true); BufferedWriter bufferedWriter = new BufferedWriter(writer);
		 * 
		 * bufferedWriter.write("report number is : "+report_number_data);
		 * bufferedWriter.newLine(); // bufferedWriter.write("See You Again!");
		 * 
		 * bufferedWriter.close(); } catch (IOException e) {
		 * e.printStackTrace(); }
		 */

		try {
			// int a=3;
			FileWriter writer = new FileWriter(MyRunnable.Storege_folder_path + response.getStep1().getReportNumber()
					+ "\\" + response.getStep1().getReportNumber() + "_API_Response_data_File.txt", true);
			BufferedWriter bufferedWriter = new BufferedWriter(writer);
			bufferedWriter.newLine();
			bufferedWriter.newLine();
			bufferedWriter.write("START===============================================================");
			bufferedWriter.newLine();
			bufferedWriter.write("id is :=> " + response.getId());
			bufferedWriter.newLine();
			// bufferedWriter.write("ST_1 report_number is :=> " +
			// response.getStep1().getReportNumber());
			bufferedWriter.write("ST_1 report_number is :=> " + response.getStep1().getReportNumber());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_1 referral is :=> " + response.getStep1().getReferral());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_2 type_of_applicant is :=> " + response.getStep2().getTypeOfApplicant());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_2 identity_type_of_report_holder is :=> "
					+ response.getStep2().getIdentityTypeOfReportHolder());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_2 ID_holder_of_the_report is :=> " + response.getStep2().getIDHolderOfTheReport());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_2 HF_holder_of_the_report is :=> " + response.getStep2().getHFHolderOfTheReport());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_2 company_name_of_the_report_holder is :=> "
					+ response.getStep2().getCompanyNameOfTheReportHolder());
			bufferedWriter.newLine();
			bufferedWriter.write(
					"ST_2 first_name_of_the_report_owner is :=> " + response.getStep2().getFirstNameOfTheReportOwner());
			bufferedWriter.newLine();
			bufferedWriter.write(
					"ST_2 last_name_of_the_report_owner is :=> " + response.getStep2().getLastNameOfTheReportOwner());
			bufferedWriter.newLine();
			bufferedWriter.write(
					"ST_2 report_driver_license_number is :=> " + response.getStep2().getReportDriverLicenseNumber());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_2 resettlement is :=> " + response.getStep2().getResettlement());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_2 street is :=> " + response.getStep2().getStreet());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_2 house_number is :=> " + response.getStep2().getHouseNumber());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_2 apartment_number is :=> " + response.getStep2().getApartmentNumber());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_2 zip is :=> " + response.getStep2().getZip());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_2 other_address is :=> " + response.getStep2().getOtherAddress());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_2 phone is :=> " + response.getStep2().getPhone());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_2 mobile_phone is :=> " + response.getStep2().getMobilePhone());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_2 mail is :=> " + response.getStep2().getMail());
			bufferedWriter.newLine();
			// step2 new data
			bufferedWriter.write("ST_2 pra_identity_type_of_report_holder is :=> "
					+ response.getStep2().getIdentityTypeOfReportHolder());
			bufferedWriter.newLine();
			bufferedWriter
					.write("ST_2 pra_ID_holder_of_the_report is :=> " + response.getStep2().getIDHolderOfTheReport());
			bufferedWriter.newLine();
			bufferedWriter
					.write("ST_2 pra_HF_holder_of_the_report is :=> " + response.getStep2().getHFHolderOfTheReport());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_2 pra_passport is :=> " + response.getStep2().getPra_passport_data());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_2 pra_company_name_of_the_report_holder is :=> "
					+ response.getStep2().getPra_company_name_of_the_report_holder_data());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_2 pra_first_name_of_the_report_owner is :=> "
					+ response.getStep2().getPra_first_name_of_the_report_owner_data());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_2 pra_last_name_of_the_report_owner is :=> "
					+ response.getStep2().getPra_last_name_of_the_report_owner_data());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_2 pra_report_driver_license_number is :=> "
					+ response.getStep2().getPra_report_driver_license_number_data());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_2 pra_resettlement is :=> " + response.getStep2().getPra_resettlement_data());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_2 pra_street is :=> " + response.getStep2().getPra_street_data());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_2 pra_house_number is :=> " + response.getStep2().getPra_house_number_data());
			bufferedWriter.newLine();
			bufferedWriter
					.write("ST_2 pra_apartment_number is :=> " + response.getStep2().getPra_apartment_number_data());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_2 pra_zip is :=> " + response.getStep2().getPra_zip_data());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_2 pra_other_address is :=> " + response.getStep2().getPra_other_address_data());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_2 pra_phone is :=> " + response.getStep2().getPra_phone_data());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_2 pra_mobile_phone is :=> " + response.getStep2().getPra_mobile_phone_data());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_2 pra_mail is :=> " + response.getStep2().getPra_mail_data());
			bufferedWriter.newLine();

			if (response.isStepThreeExsist() == true) {
				bufferedWriter.write("ST_3 last_name is :=> " + response.getStep3().getLastName());
				bufferedWriter.newLine();
				bufferedWriter.write("ST_3 First_name is :=> " + response.getStep3().getFirstName());
				bufferedWriter.newLine();
				bufferedWriter
						.write("ST_3 attorney_license_number is :=> " + response.getStep3().getAttorneyLicenseNumber());
				bufferedWriter.newLine();
				bufferedWriter.write("ST_3 resettlement is :=> " + response.getStep3().getResettlement());
				bufferedWriter.newLine();
				bufferedWriter.write("ST_3 street is :=> " + response.getStep3().getStreet());
				bufferedWriter.newLine();
				bufferedWriter.write("ST_3 house_number is :=> " + response.getStep3().getHouseNumber());
				bufferedWriter.newLine();
				bufferedWriter.write("ST_3 apartment_number is :=> " + response.getStep3().getApartmentNumber());
				bufferedWriter.newLine();
				bufferedWriter.write("ST_3 zip is :=> " + response.getStep3().getZip());
				bufferedWriter.newLine();
				bufferedWriter.write("ST_3 other_address is :=> " + response.getStep3().getOtherAddress());
				bufferedWriter.newLine();
				bufferedWriter.write("ST_3 phone is :=> " + response.getStep3().getPhone());
				bufferedWriter.newLine();
				bufferedWriter.write("ST_3 mobile_phone is :=> " + response.getStep3().getMobilePhone());
				bufferedWriter.newLine();
				bufferedWriter.write("ST_3 mail is :=> " + response.getStep3().getMail());
				bufferedWriter.newLine();
			} else {
				bufferedWriter
						.write(" STEP 3 IS NOT EXIST in REPORT NUM is :=> " + response.getStep1().getReportNumber());
				bufferedWriter.newLine();
			}
			bufferedWriter.write("ST_4 reason_details is:=> " + response.getStep4().getReasonDetails());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_5 file_photo_report is :=> " + response.getStep5().getFilePhotoReport());
			bufferedWriter.newLine();
			bufferedWriter
					.write("ST_5 file_photo_id_signature is :=> " + response.getStep5().getFilePhotoIdSignature());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_5 Power_of_attorney_from_customer is :=> "
					+ response.getStep5().getFilePowerOfAttorneyFromCustomer());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_5 file_signed_contract_of_the_tenant is :=> "
					+ response.getStep5().getFileSignedContractOfTheTenant());
			bufferedWriter.newLine();
			bufferedWriter.write(
					"ST_5 file_rental_company_reporting is :=> " + response.getStep5().getFileRentalCompanyReporting());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_5 file_actual_driver_committe_the_offence is :=> "
					+ response.getStep5().getFile_actual_driver_committe_the_offence_data());
			bufferedWriter.newLine();
			bufferedWriter.write(
					"ST_5 file_driver_license_photo is :=> " + response.getStep5().getFile_driver_license_photo_data());
			bufferedWriter.newLine();
			bufferedWriter.write(
					"ST_5 file_request_to_be_judged is :=> " + response.getStep5().getFile_request_to_be_judged_data());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_5 file_confirmation_of_payment is :=> "
					+ response.getStep5().getFile_confirmation_of_payment_data());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_5 file_proof is :=> " + response.getStep5().getFile_proof_data());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_5 file_transfer_of_ownership is :=> "
					+ response.getStep5().getFile_transfer_of_ownership_data());
			bufferedWriter.newLine();
			bufferedWriter.write("ST_5 file_protocol_sign_by_lowyer is :=> "
					+ response.getStep5().getFile_protocol_sign_by_lowyer_data());
			bufferedWriter.newLine();
			bufferedWriter
					.write("ST_5 file_address_approval is :=> " + response.getStep5().getFile_address_approval_data());
			bufferedWriter.newLine();

			bufferedWriter.write("END===============================================================");

			// bufferedWriter.write("See You Again!");

			bufferedWriter.close();
			System.out.println("_API_Response_data_File is created successfully");
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("----------------------------------------");
			System.err.println("_API_Response_data_File is Not created");
			System.err.println("----------------------------------------");
		}

	}

	public static void create_folder(ModelData response) {

		File file = new File(MyRunnable.Storege_folder_path, response.getStep1().getReportNumber());
		if (!file.exists()) {
			if (file.mkdir()) {
				System.out.println("Folder is created! As: " + response.getStep1().getReportNumber());
			} else {
				System.out.println("Failed to create directory!");
			}
		} else {
			System.out.println("Folder already Created, folder num is :" + response.getStep1().getReportNumber());
		}
	}

	public static void Click_sikuli_cancel_button() throws InterruptedException, FindFailed {
		Pattern cancel_button = new Pattern("C:\\Users\\Warrior\\workspace\\api\\cancel_button.png");
		waitForImage("C:\\Users\\Warrior\\workspace\\api\\cancel_button.png", 20);
		screen1.click(cancel_button);
	}

	public static void Click_sikuli_open_button() throws InterruptedException, FindFailed {
		Pattern open_button = new Pattern("C:\\Users\\Warrior\\workspace\\api\\open_button.png");
		waitForImage("C:\\Users\\Warrior\\workspace\\api\\open_button.png", 20);
		screen1.click(open_button);
	}

	public static void insert_download_location(String image_downloaded_in_local_folder1, String imageName)
			throws FindFailed, InterruptedException {
		Pattern File_input_box = new Pattern("C:\\Users\\Warrior\\workspace\\api\\Filename_inputbox.png");
		waitForImage("C:\\Users\\Warrior\\workspace\\api\\Filename_inputbox.png", 20);
		screen1.click(File_input_box);

		String imageFullPath = image_downloaded_in_local_folder1 + imageName;
		System.out.println("Image Full Path is : " + imageFullPath);

		screen1.type(imageFullPath);
		System.out.println("inserted Downloaded location successfully");
	}

	public static boolean isImagePresent(String image) {
		boolean status = false;
		screen1 = new Screen();
		try {
			screen1.find(image);
			status = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public static void waitForImage(String image, int time) throws InterruptedException, FindFailed {
		for (int iii = 0; iii < time; iii++) {
			// Thread.sleep(1000); // added new thered here for experiment
			if (isImagePresent(image)) {
				break;
			} else {
				System.out.println("AUTOMATION IS WAITING FOR 5 SEC TO FIND THE ELEMENT TO PERFORM ACTION :" + iii);
				// Thread.sleep(1000);
			}
		}
	}

	// static boolean attachment_image_data_present = true;
	// static String new_image_local_path = path1 + "\\docs\\" +
	// get_image_name1;
	// static String new_image_local_path1;
	static String sikuli_image_filepath = "C:\\Users\\Warrior\\workspace\\api\\";

	public static synchronized String insert_file_photo_report_image(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response) throws IOException {
		if (response.getStep5().isFile_photo_report_data_present() == true) {
			try {
				String image_name = download_image1(response, response.getStep5().getFilePhotoReport());
				String ImagePath = "D:\\AAAA\\" + response.getStep1().getReportNumber() + "\\";

				WebElement file_photo_report_image_Web_Element;
				file_photo_report_image_Web_Element = wait.until(
						ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.file_photo_report_inputbox))));
				file_photo_report_image_Web_Element.click();

				insert_download_location(ImagePath, image_name);

				Click_sikuli_open_button();
				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is getting validation message
					String file_photo_report_image_validation = driver.findElement(By.xpath(xp.validation_popup))
							.getText();
					System.out.println("ST_5 file_photo_report_image_validation Error is :=> "
							+ file_photo_report_image_validation);
					System.out
							.println("ST_5 file_photo_report_image is :=> " + response.getStep5().getFilePhotoReport());
					bufferedWriter2.newLine();
					bufferedWriter2.write("===========");
					bufferedWriter2.newLine();
					bufferedWriter2.write("step 5");
					bufferedWriter2.newLine();
					bufferedWriter2.write("===========");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_photo_report_image_validation Error is :=> "
							+ file_photo_report_image_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_photo_report_image_validation number is :=> "
							+ response.getStep5().getFilePhotoReport());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					// Thread.sleep(1000);

					return file_photo_report_image_validation;
				} else {
					bufferedWriter2.write("===========");
					bufferedWriter2.newLine();
					bufferedWriter2.write("step 5");
					bufferedWriter2.newLine();
					bufferedWriter2.write("===========");
					System.out.println("ST_5 file_photo_report_image attached successfully");
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("ST_5 file_photo_report_image is :=> " + response.getStep5().getFilePhotoReport());
					bufferedWriter2.newLine();

					return "";
				}
			} catch (Exception e) {
				System.out.println("------------------------------------------------------");
				System.out.println("STEP_5_Not able to insert file_photo_report_image");
				System.out.println("------------------------------------------------------");
				bufferedWriter2.write("===========");
				bufferedWriter2.newLine();
				bufferedWriter2.write("step 5");
				bufferedWriter2.newLine();
				bufferedWriter2.write("===========");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####STEP_5_Not able to insert file_photo_report_image :=> "
						+ response.getStep5().getFilePhotoReport());
				bufferedWriter2.newLine();
				return "";
			}
		} else {
			System.out.println("**** STEP_5_file_photo_report_data is not present");
			bufferedWriter2.write("===========");
			bufferedWriter2.newLine();
			bufferedWriter2.write("step 5");
			bufferedWriter2.newLine();
			bufferedWriter2.write("===========");
			bufferedWriter2.newLine();
			bufferedWriter2.write("**** STEP_5_file_photo_report_data is not present :=> "
					+ response.getStep5().getFilePhotoReport());
			bufferedWriter2.newLine();
			bufferedWriter2.write("id is " + response.getId());
			return "";
		}
	}

	public static synchronized String insert_file_photo_id_signature(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response) throws IOException {

		if (response.getStep5().isFile_photo_id_signature_data_present() == true) {
			try {

				String image_name = download_image1(response, response.getStep5().getFilePhotoIdSignature());
				String ImagePath = "D:\\AAAA\\" + response.getStep1().getReportNumber() + "\\";

				WebElement file_photo_id_signature_image_Web_Element;
				file_photo_id_signature_image_Web_Element = wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath(xp.file_photo_id_signature_inputbox))));
				file_photo_id_signature_image_Web_Element.click();

				insert_download_location(ImagePath, image_name);

				Click_sikuli_open_button();
				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is getting validation message
					String file_photo_id_signature_image_validation = driver.findElement(By.xpath(xp.validation_popup))
							.getText();
					System.out.println("ST_5 file_photo_id_signature_image_validation Error is :=> "
							+ file_photo_id_signature_image_validation);
					System.out.println("ST_5 file_photo_id_signature_image is :=> "
							+ response.getStep5().getFilePhotoIdSignature());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_photo_id_signature_image_validation Error is :=> "
							+ file_photo_id_signature_image_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_photo_id_signature_image is :=> "
							+ response.getStep5().getFilePhotoIdSignature());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					// Thread.sleep(1000);

					return file_photo_id_signature_image_validation;
				} else {
					System.out.println("ST_5 file_photo_id_signature_image attached successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_photo_id_signature_image is :=> "
							+ response.getStep5().getFilePhotoIdSignature());
					bufferedWriter2.newLine();

					return "";
				}

			} catch (Exception e) {
				System.out.println("------------------------------------------");
				System.out.println("Not able to insert file_photo_id_signature");
				System.out.println("------------------------------------------");
				bufferedWriter2.newLine();
				bufferedWriter2.write(
						"#### file_photo_id_signature_image is :=> " + response.getStep5().getFilePhotoIdSignature());
				bufferedWriter2.newLine();
				return "";
			}
		} else {
			System.out.println("****file_photo_id_signature is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****file_photo_id_signature_data is Not present :=> "
					+ response.getStep5().getFilePhotoIdSignature());
			bufferedWriter2.newLine();
			return "";
		}
	}

	public static synchronized String insert_Power_of_attorney_from_customer(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response) throws IOException {
		if (response.getStep5().isPower_of_attorney_from_customer_data_present() == true) {
			try {

				String image_name = download_image1(response, response.getStep5().getFilePowerOfAttorneyFromCustomer());
				String ImagePath = "D:\\AAAA\\" + response.getStep1().getReportNumber() + "\\";

				WebElement Power_of_attorney_from_customer_data_image_Web_Element;
				Power_of_attorney_from_customer_data_image_Web_Element = wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath(xp.Power_of_attorney_from_customer_inputbox))));
				Power_of_attorney_from_customer_data_image_Web_Element.click();
				insert_download_location(ImagePath, image_name);
				Click_sikuli_open_button();
				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is getting validation message
					String Power_of_attorney_from_customer_data_image_validation = driver
							.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_5 Power_of_attorney_from_customer_data_image_validation Error is :=> "
							+ Power_of_attorney_from_customer_data_image_validation);
					System.out.println("ST_5 Power_of_attorney_from_customer_data_image is :=> "
							+ response.getStep5().getFilePowerOfAttorneyFromCustomer());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 Power_of_attorney_from_customer_data_image_validation Error is :=> "
							+ Power_of_attorney_from_customer_data_image_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 Power_of_attorney_from_customer_data_image is :=> "
							+ response.getStep5().getFilePowerOfAttorneyFromCustomer());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					// Thread.sleep(1000);

					return Power_of_attorney_from_customer_data_image_validation;
				} else {
					System.out.println("ST_5 Power_of_attorney_from_customer_data_image attached successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 Power_of_attorney_from_customer_data_image is :=> "
							+ response.getStep5().getFilePowerOfAttorneyFromCustomer());
					bufferedWriter2.newLine();

					return "";
				}
			} catch (Exception e) {
				System.out.println("--------------------------------------------------");
				System.out.println("Not able to insert Power_of_attorney_from_customer");
				System.out.println("--------------------------------------------------");
				bufferedWriter2.newLine();
				bufferedWriter2.write("#### Power_of_attorney_from_customer_data is :=> "
						+ response.getStep5().getFilePowerOfAttorneyFromCustomer());
				bufferedWriter2.newLine();
				bufferedWriter2.write("id is " + response.getId());
				return "";
			}
		} else {
			System.out.println("****Power_of_attorney_from_customer_data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****Power_of_attorney_from_customer_data is Not present :=> "
					+ response.getStep5().getFilePowerOfAttorneyFromCustomer());
			bufferedWriter2.newLine();
			bufferedWriter2.write("id is " + response.getId());
			return "";
		}
	}

	public static synchronized String insert_file_rental_company_reporting(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response) throws IOException {
		if (response.getStep5().isFile_rental_company_reporting_data_present() == true) {
			try {

				String image_name = download_image1(response, response.getStep5().getFileRentalCompanyReporting());
				String ImagePath = "D:\\AAAA\\" + response.getStep1().getReportNumber() + "\\";

				WebElement file_rental_company_reporting_data_image_Web_Element;
				file_rental_company_reporting_data_image_Web_Element = wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath(xp.file_rental_company_reporting_inputbox))));
				file_rental_company_reporting_data_image_Web_Element.click();

				insert_download_location(ImagePath, image_name);

				Click_sikuli_open_button();
				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is getting validation message
					String file_rental_company_reporting_data_image_validation = driver
							.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_5 file_rental_company_reporting_data_image_validation Error is :=> "
							+ file_rental_company_reporting_data_image_validation);
					System.out.println("ST_5 file_rental_company_reporting_data_image is :=> "
							+ response.getStep5().getFileRentalCompanyReporting());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_rental_company_reporting_data_image_validation Error is :=> "
							+ file_rental_company_reporting_data_image_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_rental_company_reporting_data_image is :=> "
							+ response.getStep5().getFileRentalCompanyReporting());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					// Thread.sleep(5000);

					return file_rental_company_reporting_data_image_validation;
				} else {
					System.out.println("ST_5 file_rental_company_reporting_data_image attached successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_rental_company_reporting_data_image is :=> "
							+ response.getStep5().getFileRentalCompanyReporting());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println("-----------------------------------------------------");
				System.out.println("Not able to insert file_rental_company_reporting_data");
				System.out.println("-----------------------------------------------------");
				bufferedWriter2.newLine();
				bufferedWriter2.write("#### file_rental_company_reporting_data is :=> "
						+ response.getStep5().getFileRentalCompanyReporting());
				bufferedWriter2.newLine();
				bufferedWriter2.write("id is " + response.getId());
				return "";
			}
		} else {
			System.out.println("****file_rental_company_reporting_data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****file_rental_company_reporting_data is Not present :=> "
					+ response.getStep5().getFileRentalCompanyReporting());
			bufferedWriter2.newLine();
			bufferedWriter2.write("id is " + response.getId());
			return "";
		}
	}

	public static synchronized String insert_file_signed_contract_of_the_tenant(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response) throws IOException {
		if (response.getStep5().isFile_signed_contract_of_the_tenant_data_present() == true) {
			try {
				String image_name = download_image1(response, response.getStep5().getFileSignedContractOfTheTenant());
				String ImagePath = "D:\\AAAA\\" + response.getStep1().getReportNumber() + "\\";

				WebElement file_signed_contract_of_the_tenant_data_image_Web_Element;
				file_signed_contract_of_the_tenant_data_image_Web_Element = wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath(xp.signed_contract_of_the_tenant_inputbox))));
				file_signed_contract_of_the_tenant_data_image_Web_Element.click();
				insert_download_location(ImagePath, image_name);
				Click_sikuli_open_button();
				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is getting validation message
					String file_signed_contract_of_the_tenant_data_image_validation = driver
							.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_5 file_signed_contract_of_the_tenant_data_image_validation Error is :=> "
							+ file_signed_contract_of_the_tenant_data_image_validation);
					System.out.println("ST_5 file_signed_contract_of_the_tenant_data_image is :=> "
							+ response.getStep5().getFileSignedContractOfTheTenant());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_signed_contract_of_the_tenant_data_image Error is :=> "
							+ file_signed_contract_of_the_tenant_data_image_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_signed_contract_of_the_tenant_data_image is :=> "
							+ response.getStep5().getFileSignedContractOfTheTenant());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					// Thread.sleep(1000);

					return file_signed_contract_of_the_tenant_data_image_validation;
				} else {
					System.out.println("ST_5 file_signed_contract_of_the_tenant_data_image attached successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_signed_contract_of_the_tenant_data_image is :=> "
							+ response.getStep5().getFileSignedContractOfTheTenant());
					bufferedWriter2.newLine();

					return "";
				}
			} catch (Exception e) {
				System.out.println("-----------------------------------------------------");
				System.out.println("Not able to insert file_signed_contract_of_the_tenant");
				System.out.println("-----------------------------------------------------");
				bufferedWriter2.newLine();
				bufferedWriter2.write("#### file_signed_contract_of_the_tenant is :=> "
						+ response.getStep5().getFileSignedContractOfTheTenant());
				bufferedWriter2.newLine();
				return "";
			}
		} else {
			System.out.println("****file_signed_contract_of_the_tenant is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****file_signed_contract_of_the_tenant_data is Not present :=> "
					+ response.getStep5().getFileSignedContractOfTheTenant());
			bufferedWriter2.newLine();
			return "";
		}
	}

	public static synchronized String insert_file_actual_driver_committe_the_offence(WebDriver driver,
			WebDriverWait wait, BufferedWriter bufferedWriter2, ModelData response)
			throws IOException, FindFailed, InterruptedException {
		if (response.getStep5().isFile_actual_driver_committe_the_offence_data_present() == true) {
			try {
				String image_name = download_image1(response,
						response.getStep5().getFile_actual_driver_committe_the_offence_data());
				String ImagePath = "D:\\AAAA\\" + response.getStep1().getReportNumber() + "\\";

				WebElement file_actual_driver_committe_the_offence_Web_Element;
				file_actual_driver_committe_the_offence_Web_Element = wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath(xp.file_actual_driver_committe_the_offence))));
				file_actual_driver_committe_the_offence_Web_Element.click();
				insert_download_location(ImagePath, image_name);
				Click_sikuli_open_button();
				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is getting validation message
					String file_actual_driver_committe_the_offence_image_validation = driver
							.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_5 file_actual_driver_committe_the_offence_image_validation Error is :=> "
							+ file_actual_driver_committe_the_offence_image_validation);
					System.out.println("ST_5 file_actual_driver_committe_the_offence_image is :=> "
							+ response.getStep5().getFile_actual_driver_committe_the_offence_data());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_actual_driver_committe_the_offence_image_validation Error is :=> "
							+ file_actual_driver_committe_the_offence_image_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_actual_driver_committe_the_offence is :=> "
							+ response.getStep5().getFile_actual_driver_committe_the_offence_data());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					// Thread.sleep(1000);

					return file_actual_driver_committe_the_offence_image_validation;
				} else {
					System.out.println("ST_5 file_actual_driver_committe_the_offence attached successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_actual_driver_committe_the_offence is :=> "
							+ response.getStep5().getFile_actual_driver_committe_the_offence_data());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println("-----------------------------------------------------");
				System.out.println("Not able to insert file_actual_driver_committe_the_offence");
				System.out.println("-----------------------------------------------------");
				bufferedWriter2.newLine();
				bufferedWriter2.write("#### file_actual_driver_committe_the_offence is :=> "
						+ response.getStep5().getFile_actual_driver_committe_the_offence_data());
				bufferedWriter2.newLine();
				return "";
			}
		} else {
			System.out.println("****file_actual_driver_committe_the_offence is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****file_actual_driver_committe_the_offence is Not present :=> "
					+ response.getStep5().getFile_actual_driver_committe_the_offence_data());
			bufferedWriter2.newLine();
			return "";
		}
	}

	public static synchronized String insert_file_driver_license_photo(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response) throws IOException, FindFailed, InterruptedException {
		if (response.getStep5().isFile_driver_license_photo_data_present() == true) {
			try {

				String image_name = download_image1(response, response.getStep5().getFile_driver_license_photo_data());
				String ImagePath = "D:\\AAAA\\" + response.getStep1().getReportNumber() + "\\";

				WebElement file_driver_license_photo_Web_Element;
				file_driver_license_photo_Web_Element = wait.until(
						ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.file_driver_license_photo))));
				file_driver_license_photo_Web_Element.click();

				insert_download_location(ImagePath, image_name);

				Click_sikuli_open_button();
				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is getting validation message
					String file_driver_license_photo_image_validation = driver
							.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_5 file_driver_license_photo_image_validation Error is :=> "
							+ file_driver_license_photo_image_validation);
					System.out.println("ST_5 file_driver_license_photo_image is :=> "
							+ response.getStep5().getFile_driver_license_photo_data());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_driver_license_photo_image_validation Error is :=> "
							+ file_driver_license_photo_image_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_driver_license_photo_image is :=> "
							+ response.getStep5().getFile_driver_license_photo_data());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return file_driver_license_photo_image_validation;
				} else {
					System.out.println("ST_5 file_driver_license_photo_image_validation attached successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_driver_license_photo_image_validation is :=> "
							+ response.getStep5().getFile_driver_license_photo_data());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println("-----------------------------------------------------");
				System.out.println("Not able to insert file_driver_license_photo_image");
				System.out.println("-----------------------------------------------------");
				bufferedWriter2.newLine();
				bufferedWriter2.write("#### file_driver_license_photo_image is :=> "
						+ response.getStep5().getFile_driver_license_photo_data());
				bufferedWriter2.newLine();
				return "";
			}
		} else {
			System.out.println("****file_driver_license_photo_image is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****file_driver_license_photo_image is Not present :=> "
					+ response.getStep5().getFile_driver_license_photo_data());
			bufferedWriter2.newLine();
			return "";
		}
	}

	public static synchronized String insert_file_request_to_be_judged(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response) throws IOException, FindFailed, InterruptedException {
		if (response.getStep5().isFile_request_to_be_judged_data_present() == true) {
			try {
				String image_name = download_image1(response, response.getStep5().getFile_request_to_be_judged_data());
				String ImagePath = "D:\\AAAA\\" + response.getStep1().getReportNumber() + "\\";

				WebElement file_request_to_be_judged_Web_Element;
				file_request_to_be_judged_Web_Element = wait.until(
						ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.file_request_to_be_judged))));
				file_request_to_be_judged_Web_Element.click();

				insert_download_location(ImagePath, image_name);

				Click_sikuli_open_button();
				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is getting validation message
					String file_request_to_be_judged_image_validation = driver
							.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_5 file_request_to_be_judged_image_validation Error is :=> "
							+ file_request_to_be_judged_image_validation);
					System.out.println("ST_5 file_request_to_be_judged_image is :=> "
							+ response.getStep5().getFile_request_to_be_judged_data());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_request_to_be_judged_image_validation Error is :=> "
							+ file_request_to_be_judged_image_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_request_to_be_judged_image is :=> "
							+ response.getStep5().getFile_request_to_be_judged_data());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					// Thread.sleep(1000);

					return file_request_to_be_judged_image_validation;
				} else {
					System.out.println("ST_5 file_request_to_be_judged_image attached successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_request_to_be_judged_image_ is :=> "
							+ response.getStep5().getFile_request_to_be_judged_data());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println("-----------------------------------------------------");
				System.out.println("Not able to insert file_request_to_be_judged_image");
				System.out.println("-----------------------------------------------------");
				bufferedWriter2.newLine();
				bufferedWriter2.write("#### file_request_to_be_judged_image is :=> "
						+ response.getStep5().getFile_request_to_be_judged_data());
				bufferedWriter2.newLine();
				return "";
			}
		} else {
			System.out.println("****file_request_to_be_judged_image is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****file_request_to_be_judged_image is Not present :=> "
					+ response.getStep5().getFile_request_to_be_judged_data());
			bufferedWriter2.newLine();
			return "";
		}
	}

	public static synchronized String insert_file_confirmation_of_payment(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response) throws IOException, FindFailed, InterruptedException {
		if (response.getStep5().isFile_confirmation_of_payment_data_present() == true) {
			try {

				String image_name = download_image1(response,
						response.getStep5().getFile_confirmation_of_payment_data());
				String ImagePath = "D:\\AAAA\\" + response.getStep1().getReportNumber() + "\\";

				WebElement file_confirmation_of_payment_Web_Element;
				file_confirmation_of_payment_Web_Element = wait.until(
						ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.file_confirmation_of_payment))));
				file_confirmation_of_payment_Web_Element.click();

				insert_download_location(ImagePath, image_name);

				Click_sikuli_open_button();
				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is getting validation message
					String file_confirmation_of_payment_image_validation = driver
							.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_5 file_confirmation_of_payment_image_validation Error is :=> "
							+ file_confirmation_of_payment_image_validation);
					System.out.println("ST_5 file_confirmation_of_payment_image is :=> "
							+ response.getStep5().getFile_confirmation_of_payment_data());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_confirmation_of_payment_image_validation Error is :=> "
							+ file_confirmation_of_payment_image_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_confirmation_of_payment_image is :=> "
							+ response.getStep5().getFile_confirmation_of_payment_data());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					// Thread.sleep(1000);

					return file_confirmation_of_payment_image_validation;
				} else {
					System.out.println("ST_5 file_confirmation_of_payment_image attached successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_confirmation_of_payment_image is :=> "
							+ response.getStep5().getFile_confirmation_of_payment_data());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println("-----------------------------------------------------");
				System.out.println("Not able to insert file_confirmation_of_payment_image");
				System.out.println("-----------------------------------------------------");
				bufferedWriter2.newLine();
				bufferedWriter2.write("#### file_confirmation_of_payment_image is :=> "
						+ response.getStep5().getFile_confirmation_of_payment_data());
				bufferedWriter2.newLine();
				return "";
			}
		} else {
			System.out.println("****file_confirmation_of_payment_image is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****file_confirmation_of_payment_image is Not present :=> "
					+ response.getStep5().getFile_confirmation_of_payment_data());
			bufferedWriter2.newLine();
			return "";
		}
	}

	public static synchronized String insert_file_proof(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response) throws IOException, FindFailed, InterruptedException {
		if (response.getStep5().isFile_proof_data_present() == true) {
			try {
				String image_name = download_image1(response, response.getStep5().getFile_proof_data());
				String ImagePath = "D:\\AAAA\\" + response.getStep1().getReportNumber() + "\\";

				WebElement file_proof_Web_Element;
				file_proof_Web_Element = wait
						.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.file_proof))));
				file_proof_Web_Element.click();

				insert_download_location(ImagePath, image_name);

				Click_sikuli_open_button();
				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is getting validation message
					String file_proof_image_validation = driver.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_5 file_proof_image_validation Error is :=> " + file_proof_image_validation);
					System.out.println("ST_5 file_proof_image is :=> " + response.getStep5().getFile_proof_data());
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("ST_5 file_proof_image_validation Error is :=> " + file_proof_image_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_proof_image is :=> " + response.getStep5().getFile_proof_data());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					// Thread.sleep(1000);

					return file_proof_image_validation;
				} else {
					System.out.println("ST_5 file_proof_image attached successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_proof_image is :=> " + response.getStep5().getFile_proof_data());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println("-----------------------------------------------------");
				System.out.println("Not able to insert file_proof_image");
				System.out.println("-----------------------------------------------------");
				bufferedWriter2.newLine();
				bufferedWriter2.write("#### file_proof_image is :=> " + response.getStep5().getFile_proof_data());
				bufferedWriter2.newLine();
				return "";
			}
		} else {
			System.out.println("****file_proof_image is not present");
			bufferedWriter2.newLine();
			bufferedWriter2
					.write("****file_proof_image is Not present :=> " + response.getStep5().getFile_proof_data());
			bufferedWriter2.newLine();
			return "";
		}
	}

	public static synchronized String insert_file_transfer_of_ownership(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response) throws IOException, FindFailed, InterruptedException {
		if (response.getStep5().isFile_transfer_of_ownership_data_present() == true) {
			try {

				String image_name = download_image1(response, response.getStep5().getFile_transfer_of_ownership_data());
				String ImagePath = "D:\\AAAA\\" + response.getStep1().getReportNumber() + "\\";

				WebElement file_transfer_of_ownership_Web_Element;
				file_transfer_of_ownership_Web_Element = wait.until(
						ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.file_transfer_of_ownership))));
				file_transfer_of_ownership_Web_Element.click();

				insert_download_location(ImagePath, image_name);

				Click_sikuli_open_button();
				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is getting validation message
					String file_transfer_of_ownership_image_validation = driver
							.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_5 file_transfer_of_ownership_image Error is :=> "
							+ file_transfer_of_ownership_image_validation);
					System.out.println("ST_5 file_transfer_of_ownership_image is :=> "
							+ response.getStep5().getFile_transfer_of_ownership_data());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_transfer_of_ownership_image Error is :=> "
							+ file_transfer_of_ownership_image_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_transfer_of_ownership_image is :=> "
							+ response.getStep5().getFile_transfer_of_ownership_data());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					// Thread.sleep(1000);

					return file_transfer_of_ownership_image_validation;
				} else {
					System.out.println("ST_5 file_transfer_of_ownership_image attached successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_transfer_of_ownership_image is :=> "
							+ response.getStep5().getFile_transfer_of_ownership_data());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println("-----------------------------------------------------");
				System.out.println("Not able to insert file_transfer_of_ownership_image");
				System.out.println("-----------------------------------------------------");
				bufferedWriter2.newLine();
				bufferedWriter2.write("#### file_transfer_of_ownership_image is :=> "
						+ response.getStep5().getFile_transfer_of_ownership_data());
				bufferedWriter2.newLine();
				return "";
			}
		} else {
			System.out.println("****file_transfer_of_ownership_image is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****file_transfer_of_ownership_image is Not present :=> "
					+ response.getStep5().getFile_transfer_of_ownership_data());
			bufferedWriter2.newLine();
			return "";
		}
	}

	public static synchronized String insert_file_protocol_sign_by_lowyer(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response) throws IOException, FindFailed, InterruptedException {
		if (response.getStep5().isFile_protocol_sign_by_lowyer_data_present() == true) {
			try {

				String image_name = download_image1(response,
						response.getStep5().getFile_protocol_sign_by_lowyer_data());
				String ImagePath = "D:\\AAAA\\" + response.getStep1().getReportNumber() + "\\";

				WebElement file_protocol_sign_by_lowyer_Web_Element;
				file_protocol_sign_by_lowyer_Web_Element = wait.until(
						ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.file_protocol_sign_by_lowyer))));
				file_protocol_sign_by_lowyer_Web_Element.click();

				insert_download_location(ImagePath, image_name);

				Click_sikuli_open_button();
				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is getting validation message
					String file_protocol_sign_by_lowyer_image_validation = driver
							.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_5 file_protocol_sign_by_lowyer_image_validation Error is :=> "
							+ file_protocol_sign_by_lowyer_image_validation);
					System.out.println("ST_5 file_protocol_sign_by_lowyer_image is :=> "
							+ response.getStep5().getFile_protocol_sign_by_lowyer_data());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_protocol_sign_by_lowyer_image_validation Error is :=> "
							+ file_protocol_sign_by_lowyer_image_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_protocol_sign_by_lowyer_image is :=> "
							+ response.getStep5().getFile_protocol_sign_by_lowyer_data());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					// Thread.sleep(1000);

					return file_protocol_sign_by_lowyer_image_validation;
				} else {
					System.out.println("ST_5 file_protocol_sign_by_lowyer_image attached successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_protocol_sign_by_lowyer_image is :=> "
							+ response.getStep5().getFile_protocol_sign_by_lowyer_data());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println("-----------------------------------------------------");
				System.out.println("Not able to insert file_protocol_sign_by_lowyer_image");
				System.out.println("-----------------------------------------------------");
				bufferedWriter2.newLine();
				bufferedWriter2.write("#### file_protocol_sign_by_lowyer_image is :=> "
						+ response.getStep5().getFile_protocol_sign_by_lowyer_data());
				bufferedWriter2.newLine();
				return "";
			}
		} else {
			System.out.println("****file_protocol_sign_by_lowyer_image is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****file_protocol_sign_by_lowyer_image is Not present :=> "
					+ response.getStep5().getFile_protocol_sign_by_lowyer_data());
			bufferedWriter2.newLine();
			return "";
		}
	}

	public static synchronized String insert_file_address_approval(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response) throws IOException, FindFailed, InterruptedException {
		if (response.getStep5().isFile_address_approval_data_present() == true) {
			try {
				String image_name = download_image1(response, response.getStep5().getFile_address_approval_data());
				String ImagePath = "D:\\AAAA\\" + response.getStep1().getReportNumber() + "\\";

				WebElement file_address_approval_Web_Element;
				file_address_approval_Web_Element = wait
						.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.file_address_approval))));
				file_address_approval_Web_Element.click();

				insert_download_location(ImagePath, image_name);

				Click_sikuli_open_button();
				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is getting validation message
					String file_address_approval_image_validation = driver.findElement(By.xpath(xp.validation_popup))
							.getText();
					System.out.println("ST_5 file_address_approval_image_validation Error is :=> "
							+ file_address_approval_image_validation);
					System.out.println("ST_5 file_address_approval_image is :=> "
							+ response.getStep5().getFile_address_approval_data());
					bufferedWriter2.newLine();
					bufferedWriter2.write(
							"ST_5 file_address_approval_image Error is :=> " + file_address_approval_image_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_address_approval_image is :=> "
							+ response.getStep5().getFile_address_approval_data());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return file_address_approval_image_validation;
				} else {
					System.out.println("ST_5 file_address_approval_image attached successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_5 file_address_approval_image is :=> "
							+ response.getStep5().getFile_address_approval_data());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println("-----------------------------------------------------");
				System.out.println("Not able to insert file_address_approval_image");
				System.out.println("-----------------------------------------------------");
				bufferedWriter2.newLine();
				bufferedWriter2.write("#### file_address_approval_image is :=> "
						+ response.getStep5().getFile_address_approval_data());
				bufferedWriter2.newLine();
				return "";
			}
		} else {
			System.out.println("****file_address_approval_image is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****file_address_approval_image is Not present :=> "
					+ response.getStep5().getFile_address_approval_data());
			bufferedWriter2.newLine();
			return "";
		}
	}

	// Remove One Element From List
	public static void removeElementFromList() {
		List<Integer> count_list = new ArrayList<Integer>();
		count_list.addAll(Constants.ID_COUNT_LIST);
		count_list.remove(0);

		Constants.ID_COUNT_LIST.clear();
		Constants.ID_COUNT_LIST.addAll(count_list);
	}

	public static void popupDilogClose(WebDriver driver, WebDriverWait wait) {
		WebElement popupDilog_Web_Element;
		popupDilog_Web_Element = wait
				.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.error_dilog_popup))));
		popupDilog_Web_Element.click();

	}

	public static void InValidDataRequest(String id_data, String status, JSONObject message)
			throws IOException, JSONException {
		// URL obj = new URL("http://500shekel.co.il/api/set-status-form-data");
		URL obj = new URL("http://500shekel.co.il/api/set-status-fullform-data");
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		// con.setRequestProperty("User-Agent", USER_AGENT);

		JSONObject REQUEST_PARAMS = new JSONObject();

		REQUEST_PARAMS.put("id", id_data);
		REQUEST_PARAMS.put("status", status);
		REQUEST_PARAMS.put("message", message);

		// For POST only - START
		con.setDoOutput(true);
		OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
		wr.write(REQUEST_PARAMS.toString());
		wr.flush();
		wr.close();
		// For POST only - END

		int responseCode = con.getResponseCode();
		System.out.println("POST Response Code :: " + responseCode);

		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println(response.toString());
		} else {
			System.out.println("POST request not worked");
		}
	}

	public static String insert_report_number_experiment(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response, String report_number_insert) throws IOException {

		if (response.getStep1().isReportNumber() == true) {
			try {
				WebElement report_number_Web_Element;
				report_number_Web_Element = wait
						.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.report_number))));
				report_number_Web_Element.sendKeys(report_number_insert);
				Thread.sleep(1000);
				report_number_Web_Element.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is use to get the validation error of
					// report_number
					String report_number_validation = driver.findElement(By.xpath(xp.validation_popup)).getText();

					System.out.println("ST_1 got validation error :=> " + report_number_validation);
					System.out.println("report_number :=> " + response.getStep1().getReportNumber());

					// Send Message
					// InValidDataRequest(response.getId(), "step1", "error",
					// report_number_validation);
					bufferedWriter2.write("im right now inside report_number IF part start");
					bufferedWriter2.newLine();
					bufferedWriter2.write("START===============================================================");
					bufferedWriter2.write("===========");
					bufferedWriter2.newLine();
					bufferedWriter2.write("Step 1 data");
					bufferedWriter2.newLine();
					bufferedWriter2.write("===========");
					bufferedWriter2.newLine();
					bufferedWriter2.write("id is :=> " + response.getId());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_1 validation error is :=> " + report_number_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("report_number_is :=> " + response.getStep1().getReportNumber());
					bufferedWriter2.newLine();
					bufferedWriter2.write("im right now inside report_number IF part end");
					bufferedWriter2.newLine();
					System.out.println("------------------List After Deleting Elements" + Constants.ID_COUNT_LIST);
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return report_number_validation;

				} else {
					System.out.println("ST_1 report_number_inserted successfully");
					bufferedWriter2.write("im right now inside report_number ELSE part start");
					bufferedWriter2.newLine();
					bufferedWriter2.newLine();
					bufferedWriter2.write("START===============================================================");
					bufferedWriter2.write("===========");
					bufferedWriter2.newLine();
					bufferedWriter2.write("Step 1 data");
					bufferedWriter2.newLine();
					bufferedWriter2.write("===========");
					bufferedWriter2.newLine();
					bufferedWriter2.write("id is :=> " + response.getId());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_1 report_number_is :=> " + response.getStep1().getReportNumber());
					bufferedWriter2.write("im right now inside report_number ELSE part END");
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println("------------------------------------------");
				System.out.println("ST_1 Not able to insert report_number_Data");
				System.out.println("------------------------------------------");
				bufferedWriter2.write("===========");
				bufferedWriter2.newLine();
				bufferedWriter2.write("Step 1 data");
				bufferedWriter2.newLine();
				bufferedWriter2.write("===========");
				bufferedWriter2.newLine();
				bufferedWriter2.write("#### ST_1 NOT ABLE TO INSERT REPORT_NUMBER, REPORT_NUMBER IS :=> "
						+ response.getStep1().getReportNumber());
				bufferedWriter2.newLine();

				return "";
			}
		} else {
			System.out.println("****ST_1 report_number_Data is not present");
			bufferedWriter2.write("im right now inside report_number NOT exit part start");
			bufferedWriter2.newLine();
			bufferedWriter2.write("===========");
			bufferedWriter2.newLine();
			bufferedWriter2.write("Step 1 data");
			bufferedWriter2.newLine();
			bufferedWriter2.write("===========");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****ST_1 report_number_is_not exist :=> " + response.getStep1().getReportNumber());
			bufferedWriter2.write("im right now inside report_number NOT exit part END");
			bufferedWriter2.newLine();

			return "";

		}
	}

	public static void Insert_referel_experiment(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String referel) throws IOException {
		if (response.getStep1().referral_data_present == true) {
			try {
				WebElement referel_inputbox;
				referel_inputbox = wait.until(
						ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.Experiment_referel_inputbox))));
				referel_inputbox.sendKeys(referel);
				Thread.sleep(1000);
				referel_inputbox.sendKeys(Keys.TAB);
				System.out.println("ST_1 referel text is inserted successfully");
				bufferedWriter2.newLine();
				bufferedWriter2.write("ST_1 referral_is :=> " + response.getStep1().getReferral());
			} catch (Exception e) {
				System.out.println("=====================================================");
				System.out.println("ST_1 NOT ABLE TO insert referel text");
				System.out.println("=====================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####ST_1 NOT ABLE TO insert referral :=> " + response.getStep1().getReferral());
			}
		} else {
			System.out.println("****ST_1 referel_data not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****ST_1 referral_data_is not present :=> " + response.getStep1().getReferral());
		}
	}

	public static void select_checkbox(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2)
			throws IOException {
		try {
			WebElement check_box;
			check_box = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.check_box))));
			check_box.click();
			System.out.println("Checkbox Selected ");
			bufferedWriter2.newLine();
			bufferedWriter2.write("step_1 Checkbox Selected successfully");
		} catch (Exception e) {
			System.out.println("===============================");
			System.out.println("step_1 NOT ABLE TO SELECT THE CHECKBOX");
			System.out.println("===============================");
			driver.findElement(By.xpath("step_1 not able to select checkbox 1"));
			bufferedWriter2.newLine();
			bufferedWriter2.write("step_1 NOT ABLE TO SELECT THE STEP_1 Checkbox");
		}
	}

	public static void click_next(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2)
			throws IOException {
		try {
			WebElement next_btn;

			next_btn = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.Next_BTN))));
			next_btn.click();
			System.out.println("Clicked on the next button");
			bufferedWriter2.newLine();
			bufferedWriter2.write("clicked on next button successfully");
		} catch (Exception e) {
			System.out.println("====================================");
			System.out.println("NOT ABLE TO CLICK ON THE NEXT BUTTON");
			System.out.println("====================================");
			bufferedWriter2.newLine();
			bufferedWriter2.write("NOT ABLE TO CLICK ON THE NEXT BUTTON");
			// driver.findElement(By.xpath("not able to click on the next
			// button"));
		}
	}

	public static String click_send_btn(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2)
			throws IOException {
		try {
			System.out.println("step 5 Click Next button method started");
			WebElement click_send_btn_web_element;

			click_send_btn_web_element = wait
					.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.send_button))));
			click_send_btn_web_element.click();
			System.out.println("===========================");
			System.out.println("Clicked on the send _button");
			System.out.println("===========================");
			bufferedWriter2.newLine();
			bufferedWriter2.write("clicked on Send button successfully");
			Thread.sleep(7000);
			try {
				WebElement validation_popup_web_element;

				validation_popup_web_element = wait
						.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.validation_popup))));
				if (validation_popup_web_element.isDisplayed()) {
					String send_form_validation_msg = driver.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println(
							"send_form_validation_msg : >>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + send_form_validation_msg);
					bufferedWriter2.newLine();
					bufferedWriter2.write("after click on the send button, got this validation message => "
							+ send_form_validation_msg);
					if (send_form_validation_msg.contains("המצויינת בטופס. מספר הבקשה לברורים הוא")) {
						System.out.println("+++++++++++++++++++++++++++");
						System.out.println("Form Submitted Successfully");
						System.out.println("+++++++++++++++++++++++++++");
						driver.findElement(By.className("ui-dialog-buttonset")).click();
						System.out.println("clicked on the ok button from form");
						bufferedWriter2.newLine();
						bufferedWriter2.write("clicked on the ok button from form");
						bufferedWriter2.newLine();
						bufferedWriter2.write("+++++++++++++++++++++++++++");
						bufferedWriter2.newLine();
						bufferedWriter2.write("Form Submitted Successfully");
						bufferedWriter2.newLine();
						bufferedWriter2.write("+++++++++++++++++++++++++++");

						return send_form_validation_msg;
					} else {
						try {
							WebElement confirm_button_from_popup;

							confirm_button_from_popup = wait.until(
									ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.Confirm_button))));
							confirm_button_from_popup.click();
							System.out.println("========================================");
							System.out.println("Clicked on the Confirm button from popup");
							System.out.println("========================================");
							bufferedWriter2.newLine();
							bufferedWriter2.write("Clicked on the Confirm button from popup");
							try {
								driver.findElement(By.xpath(xp.Heder_send_button)).click();
								try {
									if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
										String send_form_2nd_time_validation_msg = driver
												.findElement(By.xpath(xp.validation_popup)).getText();
										bufferedWriter2.newLine();
										bufferedWriter2
												.write("after click on the Heder_send_button, got this validation message => "
														+ send_form_validation_msg);
										if (send_form_2nd_time_validation_msg.contains("כבר נשלח.")) {
											System.out.println("+++++++++++++++++++++++++++");
											System.out.println("Form Submitted Successfully");
											System.out.println("+++++++++++++++++++++++++++");
											bufferedWriter2.newLine();
											bufferedWriter2.write("+++++++++++++++++++++++++++");
											bufferedWriter2.newLine();
											bufferedWriter2.write("Form Submitted Successfully");
											bufferedWriter2.newLine();
											bufferedWriter2.write("+++++++++++++++++++++++++++");
										} else {
											System.out.println("3nd else block");
											bufferedWriter2.newLine();
											bufferedWriter2.write("3nd else block");
										}
										return send_form_2nd_time_validation_msg;
									} else {
										System.out.println("2nd else block");
										bufferedWriter2.newLine();
										bufferedWriter2.write("2nd else block");
									}
								} catch (ExecuteException e) {
									System.err.println(
											"================================================================");
									System.err
											.println("after click on the heder_send_button, there is no popup visible");
									System.err.println(
											"================================================================");
									bufferedWriter2.newLine();
									bufferedWriter2.write(
											"=>>> ERROR2 :=> after click on the heder_send_button, there is no popup visible");

									return "";
								}

							} catch (Exception e) {
								System.err.println("==========================================");
								System.err.println("NOT ABLE TO CLICK ON THE HEDER SEND BUTTON");
								System.err.println("==========================================");
								bufferedWriter2.newLine();
								bufferedWriter2.write(" error :- NOT ABLE TO CLICK ON THE HEDER SEND BUTTON");

								return "";
							}
						} catch (Exception e) {

							System.err.println(
									"=========================================================================");
							System.err.println(
									"NOT ABLE TO CLICK ON THE CONFIRM BUTTON FROM SUBMIT FORM VALIDATION POPUP");
							System.err.println(
									"=========================================================================");
							bufferedWriter2.newLine();
							bufferedWriter2.write(
									"error :- NOT ABLE TO CLICK ON THE CONFIRM BUTTON FROM SUBMIT FORM VALIDATION POPUP");
							driver.findElement(By.xpath(xp.validation_popup)).sendKeys(Keys.TAB, Keys.TAB);
							driver.findElement(By.xpath(xp.validation_popup)).sendKeys(Keys.ENTER);

							return "";
						}
					}
				} else {
					System.out.println("after click on the send button, no validation popup found");
				}
			} catch (Exception e) {

				System.err.println("================================================================");
				System.err.println("after click on the send_button, there is no popup visible");
				System.err.println("================================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("=>>> ERROR1 :=> after click on the send_button, there is no popup visible");
				return "";
			}
		} catch (Exception e) {

			System.out.println("====================================");
			System.out.println("NOT ABLE TO CLICK ON THE send BUTTON");
			System.out.println("====================================");
			bufferedWriter2.newLine();
			bufferedWriter2.write("NOT ABLE TO CLICK ON THE send BUTTON");

			return "";
		}
		return "";
	}

	public static void select_Type_of_applicant(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, int Type_of_applicant) throws IOException {
		if (response.getStep2().isType_of_applicant_data_present() == true) {
			try {
				Thread.sleep(1000);
				switch (Type_of_applicant) {

				case 1:
					WebElement Type_of_applicant1;
					Type_of_applicant1 = wait.until(
							ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.Type_of_applicant1))));
					Type_of_applicant1.click();
					System.out.println("step_2 Type_of_applicant selected as Report owner");
					bufferedWriter2.newLine();
					bufferedWriter2.write("===========");
					bufferedWriter2.newLine();
					bufferedWriter2.write("Step 2 data");
					bufferedWriter2.newLine();
					bufferedWriter2.write("===========");
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("step_2 type_of_applicant is :=> " + response.getStep2().getTypeOfApplicant());
					bufferedWriter2.write("  :=> Type_of_applicant selected as Report owner");

					break;
				case 2:
					WebElement Type_of_applicant2;
					Type_of_applicant2 = wait.until(
							ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.Type_of_applicant2))));
					Type_of_applicant2.click();
					System.out.println("step_2 Type_of_applicant selected as Lawyer");
					bufferedWriter2.newLine();
					bufferedWriter2.write("===========");
					bufferedWriter2.newLine();
					bufferedWriter2.write("Step 2 data");
					bufferedWriter2.newLine();
					bufferedWriter2.write("===========");
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("step_2 type_of_applicant is :=> " + response.getStep2().getTypeOfApplicant());
					bufferedWriter2.write("  :=> Type_of_applicant selected as Lawyer");

					break;
				case 3:
					WebElement Type_of_applicant3;
					Type_of_applicant3 = wait.until(
							ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.Type_of_applicant3))));
					Type_of_applicant3.click();
					System.out.println("step_2 Type_of_applicant selected as Relative");
					bufferedWriter2.newLine();
					bufferedWriter2.write("===========");
					bufferedWriter2.newLine();
					bufferedWriter2.write("Step 2 data");
					bufferedWriter2.newLine();
					bufferedWriter2.write("===========");
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("step_2 type_of_applicant is :=> " + response.getStep2().getTypeOfApplicant());
					bufferedWriter2.write("  :=> Type_of_applicant selected as Relative");

					break;
				case 4:
					WebElement Type_of_applicant4;
					Type_of_applicant4 = wait.until(
							ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.Type_of_applicant4))));
					Type_of_applicant4.click();
					System.out.println("step_2 Type_of_applicant as selected Rental company");
					bufferedWriter2.newLine();
					bufferedWriter2.write("===========");
					bufferedWriter2.newLine();
					bufferedWriter2.write("Step 2 data");
					bufferedWriter2.newLine();
					bufferedWriter2.write("===========");
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("step_2 type_of_applicant is :=> " + response.getStep2().getTypeOfApplicant());
					bufferedWriter2.write("  :=> Type_of_applicant selected as Rental company");

					break;
				case 5:
					WebElement Type_of_applicant5;
					Type_of_applicant5 = wait.until(
							ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.Type_of_applicant5))));
					Type_of_applicant5.click();
					System.out.println("step_2 Type_of_applicant as selected Welfare worker");
					bufferedWriter2.newLine();
					bufferedWriter2.write("===========");
					bufferedWriter2.newLine();
					bufferedWriter2.write("Step 2 data");
					bufferedWriter2.newLine();
					bufferedWriter2.write("===========");
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("step_2 type_of_applicant is :=> " + response.getStep2().getTypeOfApplicant());
					bufferedWriter2.write("  :=> Type_of_applicant selected as Welfare worker");

					break;
				case 6:
					WebElement Type_of_applicant6;
					Type_of_applicant6 = wait.until(
							ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.Type_of_applicant6))));
					Type_of_applicant6.click();
					System.out.println("step_2 Type_of_applicant selected as KMR");
					bufferedWriter2.newLine();
					bufferedWriter2.write("===========");
					bufferedWriter2.newLine();
					bufferedWriter2.write("Step 2 data");
					bufferedWriter2.newLine();
					bufferedWriter2.write("===========");
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("step_2 type_of_applicant is :=> " + response.getStep2().getTypeOfApplicant());
					bufferedWriter2.write("  :=> Type_of_applicant selected as KMR");

					break;
				case 7:
					WebElement Type_of_applicant7;
					Type_of_applicant7 = wait.until(
							ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.Type_of_applicant7))));
					Type_of_applicant7.click();
					System.out.println("step_2 Type_of_applicant selected as K. Safety");
					bufferedWriter2.newLine();
					bufferedWriter2.write("===========");
					bufferedWriter2.newLine();
					bufferedWriter2.write("Step 2 data");
					bufferedWriter2.newLine();
					bufferedWriter2.write("===========");
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("step_2 type_of_applicant is :=> " + response.getStep2().getTypeOfApplicant());
					bufferedWriter2.write("  :=> Type_of_applicant selected as K. Safety");

					break;

				default:
					System.out.println(
							"-------------------------------------------------------------------------------------------------------------------");
					System.out.println("Type_of_applicant got different from API, got response as :=> "
							+ response.getStep2().getTypeOfApplicant());
					System.out.println(
							"-------------------------------------------------------------------------------------------------------------------");
					break;
				}
			} catch (Exception e) {
				System.out.println("============================================");
				System.out.println("step_2 NOT ABLE TO SELECT Type_of_applicant ");
				System.out.println("============================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("===========");
				bufferedWriter2.newLine();
				bufferedWriter2.write("Step 2 data");
				bufferedWriter2.newLine();
				bufferedWriter2.write("===========");
				bufferedWriter2.newLine();
				bufferedWriter2.write("#### step_2 NOT ABLE TO SELECT type_of_applicant AS :=> "
						+ response.getStep2().getTypeOfApplicant());
			}
		} else {
			System.out.println("****step_2 type_of_applicant_data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("===========");
			bufferedWriter2.newLine();
			bufferedWriter2.write("Step 2 data");
			bufferedWriter2.newLine();
			bufferedWriter2.write("===========");
			bufferedWriter2.newLine();
			bufferedWriter2.write(
					"****step_2 type_of_applicant_data is not present :=> " + response.getStep2().getTypeOfApplicant());
		}
	}

	public static void select_identity_type_of_report_holder(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response, int report_holder) throws IOException {
		if (response.getStep2().isIdentity_type_of_report_holder_data_present() == true) {
			try {
				switch (report_holder) {

				case 1:
					WebElement identity_type_of_report_holder1;
					identity_type_of_report_holder1 = wait.until(ExpectedConditions
							.visibilityOf(driver.findElement(By.xpath(xp.identity_type_of_report_holder1))));
					identity_type_of_report_holder1.click();
					System.out.println("step_2 identity_type_of_report_holder selected as ID");
					bufferedWriter2.newLine();
					bufferedWriter2.write("step_2 identity_type_of_report_holder is (ID) :=> "
							+ response.getStep2().getIdentityTypeOfReportHolder());

					break;
				case 3:
					WebElement identity_type_of_report_holder2;
					identity_type_of_report_holder2 = wait.until(ExpectedConditions
							.visibilityOf(driver.findElement(By.xpath(xp.identity_type_of_report_holder2))));
					identity_type_of_report_holder2.click();
					System.out.println("step_2 identity_type_of_report_holder selected as private company");
					bufferedWriter2.newLine();
					bufferedWriter2.write("step_2 identity_type_of_report_holder is (private company) :=> "
							+ response.getStep2().getIdentityTypeOfReportHolder());
					break;
				default:
					System.out.println(
							"-------------------------------------------------------------------------------------------------------------------");
					System.out.println("identity_type_of_report_holder is not 1 or 3, got response as :=> "
							+ response.getStep2().getIdentityTypeOfReportHolder());
					System.out.println(
							"-------------------------------------------------------------------------------------------------------------------");
					break;
				}
			} catch (Exception e) {
				System.out.println("=========================================================");
				System.out.println("step_2 NOT ABLE TO SELECT identity_type_of_report_holder ");
				System.out.println("=========================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_2 NOT ABLE TO SELECT identity_type_of_report_holder :=> "
						+ response.getStep2().getIdentityTypeOfReportHolder());
			}
		} else {
			System.out.println("****step_2 identity_type_of_report_holder_data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****step_2 identity_type_of_report_holder data is not present :=> "
					+ response.getStep2().getIdentityTypeOfReportHolder());
		}
	}

	public static String Insert_ID_holder_of_the_report(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response, String id_holder_report_number) throws IOException {
		if (response.getStep2().isID_holder_of_the_report_data_present() == true) {
			try {
				WebElement ID_holder_of_the_report_webelement;
				ID_holder_of_the_report_webelement = wait
						.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.ID_holders_report))));
				ID_holder_of_the_report_webelement.sendKeys(id_holder_report_number);
				Thread.sleep(1000);
				ID_holder_of_the_report_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is getting validation message
					String ID_holder_of_the_report_validation = driver.findElement(By.xpath(xp.validation_popup))
							.getText();
					System.out.println("ST_2 ID_holder_of_the_report_validation Error is :=> "
							+ ID_holder_of_the_report_validation);
					System.out.println("ST_2 ID_holder_of_the_report number is :=> "
							+ response.getStep2().getIDHolderOfTheReport());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 ID_holder_of_the_report_validation Error is :=> "
							+ ID_holder_of_the_report_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 ID_holder_of_the_report number is :=> "
							+ response.getStep2().getIDHolderOfTheReport());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
//					Thread.sleep(1000);

					return ID_holder_of_the_report_validation;
				} else {
					System.out.println("ST_2 ID_holder_of_the_report inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("id is :=> " + response.getId());
					bufferedWriter2.newLine();
					bufferedWriter2.write(
							"ST_2 ID_holder_of_the_report is :=> " + response.getStep2().getIDHolderOfTheReport());
					bufferedWriter2.newLine();

					return "";
				}
			} catch (Exception e) {
				System.out.println("==================================================");
				System.out.println("step_2 NOT ABLE TO SELECT ID_holder_of_the_report ");
				System.out.println("==================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_2 NOT ABLE TO SELECT ID_holder_of_the_report :=> "
						+ response.getStep2().getIDHolderOfTheReport());
				return "";
			}

		} else {
			System.out.println("****step_2 ID_holder_of_the_report data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****step_2 ID_holder_of_the_report data is not present :=> "
					+ response.getStep2().getIDHolderOfTheReport());
			return "";
		}

	}

	public static String Insert_HF_holder_of_the_report(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response, String HF_holder_of_the_report) throws IOException {
		if (response.getStep2().isHF_holder_of_the_report_data_present() == true) {
			try {
				WebElement HF_holder_of_the_report_webelement;
				HF_holder_of_the_report_webelement = wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath(xp.HF_holder_of_the_report_inputbox))));
				HF_holder_of_the_report_webelement.sendKeys(HF_holder_of_the_report);
				Thread.sleep(1000);
				HF_holder_of_the_report_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String HF_holder_of_the_report_validation = driver.findElement(By.xpath(xp.validation_popup))
							.getText();
					System.out.println("ST_2 HF_holder_of_the_report_validation Error is :=> "
							+ HF_holder_of_the_report_validation);
					System.out.println(
							"ST_2 HF_holder_of_the_report data is :=> " + response.getStep2().getHFHolderOfTheReport());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 HF_holder_of_the_report_validation Error is :=> "
							+ HF_holder_of_the_report_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write(
							"ST_2 HF_holder_of_the_report data is :=> " + response.getStep2().getHFHolderOfTheReport());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return HF_holder_of_the_report_validation;
				} else {
					System.out.println("ST_2 HF_holder_of_the_report inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write(
							"ST_2 HF_holder_of_the_report is :=> " + response.getStep2().getHFHolderOfTheReport());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println("=========================================================");
				System.out.println("step_2 NOT ABLE TO SELECT HF_holder_of_the_report_number ");
				System.out.println("=========================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_2  NOT ABLE TO SELECT HF_holder_of_the_report :=> "
						+ response.getStep2().getHFHolderOfTheReport());
				return "";
			}

		} else {
			System.out.println("****step_2 HF_holder_of_the_report_data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****step_2 HF_holder_of_the_report data is not present :=> "
					+ response.getStep2().getHFHolderOfTheReport());
			return "";
		}

	}

	public static String Insert_company_name_of_the_report_holder(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response, String company_name_of_the_report_holder_peram)
			throws IOException {
		if (response.getStep2().isCompany_name_of_the_report_holder_data_present() == true) {
			try {
				WebElement company_name_of_the_report_holder_Webelement;
				company_name_of_the_report_holder_Webelement = wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath(xp.company_name_of_the_report_holder_inputbox))));
				company_name_of_the_report_holder_Webelement.sendKeys(company_name_of_the_report_holder_peram);
				Thread.sleep(1000);
				company_name_of_the_report_holder_Webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String company_name_of_the_report_holder_validation = driver
							.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_2 company_name_of_the_report_holder_validation Error is :=> "
							+ company_name_of_the_report_holder_validation);
					System.out.println("ST_2 company_name_of_the_report_holder data is :=> "
							+ response.getStep2().getCompanyNameOfTheReportHolder());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 company_name_of_the_report_holder_validation Error is :=> "
							+ company_name_of_the_report_holder_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 company_name_of_the_report_holder data is :=> "
							+ response.getStep2().getCompanyNameOfTheReportHolder());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return company_name_of_the_report_holder_validation;
				} else {
					System.out.println("ST_2 company_name_of_the_report_holder inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 company_name_of_the_report_holder is :=> "
							+ response.getStep2().getCompanyNameOfTheReportHolder());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println("============================================================");
				System.out.println("step_2 NOT ABLE TO SELECT company_name_of_the_report_holder ");
				System.out.println("============================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_2 NOT ABLE TO SELECT company_name_of_the_report_holder :=> "
						+ response.getStep2().getCompanyNameOfTheReportHolder());
				return "";
			}
		} else {
			System.out.println("****step_2 company_name_of_the_report_holder_data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****step_2 company_name_of_the_report_holder data is not present :=> "
					+ response.getStep2().getCompanyNameOfTheReportHolder());
			return "";
		}

	}

	public static String insert_first_name_of_the_report_owner(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response, String first_name) throws IOException {
		if (response.getStep2().isFirst_name_of_the_report_owner_data_present() == true) {
			try {
				WebElement first_name_of_the_report_owner_webelement;
				first_name_of_the_report_owner_webelement = wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath(xp.first_name_of_the_report_owner_inputbox))));
				first_name_of_the_report_owner_webelement.sendKeys(first_name);
				Thread.sleep(1000);
				first_name_of_the_report_owner_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String first_name_of_the_report_owner_validation = driver.findElement(By.xpath(xp.validation_popup))
							.getText();
					System.out.println("ST_2 first_name_of_the_report_owner_validation Error is :=> "
							+ first_name_of_the_report_owner_validation);
					System.out.println("ST_2 first_name_of_the_report_owner_validation data is :=> "
							+ response.getStep2().getFirstNameOfTheReportOwner());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 first_name_of_the_report_owner_validation Error is :=> "
							+ first_name_of_the_report_owner_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 first_name_of_the_report_owner data is :=> "
							+ response.getStep2().getFirstNameOfTheReportOwner());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return first_name_of_the_report_owner_validation;
				} else {
					System.out.println("ST_2 first_name_of_the_report_owner inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 first_name_of_the_report_owner is :=> "
							+ response.getStep2().getFirstNameOfTheReportOwner());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println("=========================================================");
				System.out.println("step_2 NOT ABLE TO insert_first_name_of_the_report_owner ");
				System.out.println("=========================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("#### step_2 NOT ABLE TO insert first_name_of_the_report_owner :=> "
						+ response.getStep2().getFirstNameOfTheReportOwner());
				return "";
			}
		} else {
			System.out.println("****step_2 first_name_of_the_report_owner_data_is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****step_2 first_name_of_the_report_owner data_is not present :=> "
					+ response.getStep2().getFirstNameOfTheReportOwner());
			return "";
		}
	}

	public static String insert_last_name_of_the_report_owner(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response, String last_name) throws IOException {
		if (response.getStep2().isLast_name_of_the_report_owner_data_present() == true) {
			try {
				WebElement last_name_of_the_report_owner_webelement;
				last_name_of_the_report_owner_webelement = wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath(xp.last_name_of_the_report_owner_inputbox))));
				last_name_of_the_report_owner_webelement.sendKeys(last_name);
				Thread.sleep(1000);
				last_name_of_the_report_owner_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String last_name_of_the_report_owner_validation = driver.findElement(By.xpath(xp.validation_popup))
							.getText();
					System.out.println("ST_2 last_name_of_the_report_owner_validation Error is :=> "
							+ last_name_of_the_report_owner_validation);
					System.out.println("ST_2 last_name_of_the_report_owner_validation data is :=> "
							+ response.getStep2().getLastNameOfTheReportOwner());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 last_name_of_the_report_owner_validation Error is :=> "
							+ response.getStep2().getLastNameOfTheReportOwner());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 last_name_of_the_report_owner data is :=> "
							+ response.getStep2().getLastNameOfTheReportOwner());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return last_name_of_the_report_owner_validation;
				} else {
					System.out.println("ST_2 last_name_of_the_report_owner inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 last_name_of_the_report_owner is :=> "
							+ response.getStep2().getFirstNameOfTheReportOwner());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println("=====================================================");
				System.out.println("step_2 NOT ABLE TO insert_last_name_of_the_report_owner ");
				System.out.println("=====================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_2 NOT ABLE TO insert last_name_of_the_report_owner :=> "
						+ response.getStep2().getLastNameOfTheReportOwner());
				return "";
			}
		} else {
			System.out.println("step_2 last_name_of_the_report_owner_data_is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****step_2 last_name_of_the_report_owner data is not present :=> "
					+ response.getStep2().getLastNameOfTheReportOwner());
			return "";
		}
	}

	public static String insert_report_driver_license_number(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response, String driver_license_number) throws IOException {
		if (response.getStep2().isReport_driver_license_number_data_present() == true) {
			try {
				WebElement report_driver_license_number_Webelement;
				report_driver_license_number_Webelement = wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath(xp.report_driver_license_number_inputbox))));
				report_driver_license_number_Webelement.sendKeys(driver_license_number);
				Thread.sleep(1000);
				report_driver_license_number_Webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String report_driver_license_number_validation = driver.findElement(By.xpath(xp.validation_popup))
							.getText();
					System.out.println("ST_2 report_driver_license_number_validation Error is :=> "
							+ report_driver_license_number_validation);
					System.out.println("ST_2 report_driver_license_number_validation data is :=> "
							+ response.getStep2().getReportDriverLicenseNumber());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 report_driver_license_number_validation Error is :=> "
							+ response.getStep2().getReportDriverLicenseNumber());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 report_driver_license_number data is :=> "
							+ response.getStep2().getReportDriverLicenseNumber());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return report_driver_license_number_validation;
				} else {
					System.out.println("ST_2 last_name_of_the_report_owner inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 last_name_of_the_report_owner is :=> "
							+ response.getStep2().getReportDriverLicenseNumber());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println("=====================================================");
				System.out.println("step_2 NOT ABLE TO insert report_driver_license_number ");
				System.out.println("=====================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_2 NOT ABLE TO insert report_driver_license_number is :=> "
						+ response.getStep2().getReportDriverLicenseNumber());
				return "";
			}
		} else {
			System.out.println("****step_2 report_driver_license_number not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****step_2 report_driver_license_number data not present :=> "
					+ response.getStep2().getReportDriverLicenseNumber());
			return "";
		}
	}

	public static void select_Resettlement_option1(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String Resettlement_option) throws IOException {
		if (response.getStep2().isResettlement_data_present() == true) {
			try {
				WebElement Resettlement_optioninputbox;
				Resettlement_optioninputbox = wait
						.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.resettlement_inputbox))));
				Resettlement_optioninputbox.sendKeys(Resettlement_option);
				Thread.sleep(1000);
				Resettlement_optioninputbox.sendKeys(Keys.TAB);
				System.out.println("step_2 Resettlement_option is inserted successfully");
				bufferedWriter2.newLine();
				bufferedWriter2.write("step_2 resettlement is :=> " + response.getStep2().getResettlement());
			} catch (Exception e) {
				System.out.println("=====================================================");
				System.out.println("step_2 NOT ABLE TO insert Resettlement_option ");
				System.out.println("=====================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write(
						"####step_2 NOT ABLE TO insert resettlement :=> " + response.getStep2().getResettlement());
			}
		} else {
			System.out.println("****step_2 Resettlement_data not present");
			bufferedWriter2.newLine();
			bufferedWriter2
					.write("****step_2 resettlement data not present :=> " + response.getStep2().getResettlement());
		}
	}

	public static void insert_street(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String street) throws IOException {
		if (response.getStep2().isStreet_data_present() == true) {
			try {
				WebElement street_inputbox;
				street_inputbox = wait
						.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.Street_input_box))));
				street_inputbox.sendKeys(street);
				Thread.sleep(1000);
				street_inputbox.sendKeys(Keys.TAB);
				System.out.println("step_2 street text inserted successfully");
				bufferedWriter2.newLine();
				bufferedWriter2.write("step_2 street is :=> " + response.getStep2().getStreet());
			} catch (Exception e) {
				System.out.println("=====================================================");
				System.out.println("step_2 NOT ABLE TO insert street text ");
				System.out.println("=====================================================");
				bufferedWriter2.newLine();
				bufferedWriter2
						.write("####step_2 NOT ABLE TO insert street data :=> " + response.getStep2().getStreet());
			}
		} else {
			System.out.println("****step_2 street data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****step_2 street data is not present :=> " + response.getStep2().getStreet());
		}
	}

	public static String insert_House_number(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String House_number) throws IOException {
		if (response.getStep2().isHouse_number_data_present() == true) {
			try {
				WebElement House_number_inputbox_webElement;
				House_number_inputbox_webElement = wait
						.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.House_number_inputbox))));
				House_number_inputbox_webElement.sendKeys(House_number);
				Thread.sleep(1000);
				House_number_inputbox_webElement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String House_number_validation = driver.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_2 House_number_validation Error is :=> " + House_number_validation);
					System.out.println(
							"ST_2 House_number_validation data is :=> " + response.getStep2().getHouseNumber());
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("ST_2 House_number_validation_validation Error is :=> " + House_number_validation);
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("ST_2 House_number_validation data is :=> " + response.getStep2().getHouseNumber());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return House_number_validation;
				} else {
					System.out.println("ST_2 House_number inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 House_number is :=> " + response.getStep2().getHouseNumber());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println("=====================================================");
				System.out.println("step_2 NOT ABLE TO insert House_number ");
				System.out.println("=====================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write(
						"####step_2 NOT ABLE TO insert house_number :=> " + response.getStep2().getHouseNumber());
				return "";
			}
		} else {
			System.out.println("****step_2 House_number data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2
					.write("****step_2 house_number data is not present :=> " + response.getStep2().getHouseNumber());
			return "";
		}
	}

	public static String insert_apartment_number(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String apartment_number) throws IOException {
		if (response.getStep2().isApartment_number_data_present() == true) {
			try {
				WebElement apartment_number_webElement;
				apartment_number_webElement = wait
						.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.apartment_number))));
				apartment_number_webElement.sendKeys(apartment_number);
				Thread.sleep(1000);
				apartment_number_webElement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String apartment_number_validation = driver.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_2 apartment_number_validation Error is :=> " + apartment_number_validation);
					System.out.println(
							"ST_2 apartment_number_validation data is :=> " + response.getStep2().getApartmentNumber());
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("ST_2 apartment_number_validation Error is :=> " + apartment_number_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 apartment_number is :=> " + response.getStep2().getApartmentNumber());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return apartment_number_validation;
				} else {
					System.out.println("ST_2 apartment_number inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 apartment_number is :=> " + response.getStep2().getApartmentNumber());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println("=====================================================");
				System.out.println("step_2 NOT ABLE TO insert apartment_number ");
				System.out.println("=====================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_2 NOT ABLE TO insert apartment_number :=> "
						+ response.getStep2().getApartmentNumber());
				return "";
			}
		} else {
			System.out.println("****step_2 apartment_number data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write(
					"****step_2 apartment_number data is not present :=> " + response.getStep2().getApartmentNumber());
			return "";
		}
	}

	public static String insert_zip(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String zip) throws IOException {
		if (response.getStep2().isZip_data_present() == true) {
			try {
				WebElement zip_webelement;
				// driver.findElement(By.xpath(xp.step_3_zip_input_box)).click();
				// driver.findElement(By.xpath(xp.step_3_zip_input_box)).sendKeys(zip);
				zip_webelement = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.zip))));

				zip_webelement.sendKeys(zip);
				Thread.sleep(1000);
				zip_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String zip_number_validation = driver.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_2 zip_number_validation Error is :=> " + zip_number_validation);
					System.out.println("ST_2 zip_number is :=> " + response.getStep2().getZip());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 zip_number_validation Error is :=> " + zip_number_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 zip_number is :=> " + response.getStep2().getZip());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return zip_number_validation;
				} else {
					System.out.println("ST_2 zip_number inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 zip_number_ is :=> " + response.getStep2().getZip());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println("=====================================================");
				System.out.println("step_2 NOT ABLE TO insert zip code " + response.getStep2().getZip());
				System.out.println("=====================================================");
				System.out.println(e);
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_2 NOT ABLE TO insert zip code :=> " + response.getStep2().getZip());
				return "";
			}
		} else {
			System.out.println("****step_2 zip data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****step_2 zip data is not present :=> " + response.getStep2().getZip());
			return "";
		}
	}

	public static void insert_other_address(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String other_address) throws IOException {
		if (response.getStep2().isOther_address_data_present() == true) {
			try {
				WebElement other_address_webelement;
				other_address_webelement = wait
						.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.other_address))));
				other_address_webelement.sendKeys(other_address);
				Thread.sleep(1000);
				// House_number_inputbox_webElement.sendKeys(Keys.TAB);
				System.out.println("step_2 other_address inserted successfully");
				bufferedWriter2.newLine();
				bufferedWriter2.write("step_2 other_address is :=> " + response.getStep2().getOtherAddress());
			} catch (Exception e) {
				System.out.println("=====================================================");
				System.out.println("step_2 NOT ABLE TO insert other_address ");
				System.out.println("=====================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write(
						"####step_2 NOT ABLE TO insert other_address :=> " + response.getStep2().getOtherAddress());
			}
		} else {
			System.out.println("****step_2 other_address data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2
					.write("****step_2 other_address data is not present :=> " + response.getStep2().getOtherAddress());
		}
	}

	public static String insert_phone_num(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String phone_num) throws IOException {
		if (response.getStep2().isPhone_data_present() == true) {
			try {
				WebElement phone_number_webelement;
				phone_number_webelement = wait
						.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.phone_num))));
				phone_number_webelement.sendKeys(phone_num);
				Thread.sleep(1000);
				phone_number_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String phone_number_validation = driver.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_2 phone_number_validation Error is :=> " + phone_number_validation);
					System.out.println("ST_2 phone_number is :=> " + response.getStep2().getPhone());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 phone_number Error is :=> " + phone_number_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 phone_number is :=> " + response.getStep2().getPhone());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return phone_number_validation;
				} else {
					System.out.println("ST_2 phone_number inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 phone_number is :=> " + response.getStep2().getPhone());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println("=====================================================");
				System.out.println("step_2 NOT ABLE TO insert phone_num ");
				System.out.println("=====================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_2 NOT ABLE TO insert phone_num :=> " + response.getStep2().getPhone());
				return "";
			}
		} else {
			System.out.println("****step_2 phone_num data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****step_2 phone_num data is not present :=> " + response.getStep2().getPhone());
			return "";
		}
	}

	public static String insert_mobile_phone(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String mobile_phone) throws IOException {
		if (response.getStep2().isMobile_phone_data_present() == true) {
			try {
				WebElement mobile_phone_webelement;
				mobile_phone_webelement = wait
						.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.mobile_number))));
				mobile_phone_webelement.sendKeys(mobile_phone);
				Thread.sleep(1000);
				mobile_phone_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String mobile_phone_validation = driver.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_2 mobile_phone_validation Error is :=> " + mobile_phone_validation);
					System.out.println("ST_2 mobile_phone is :=> " + response.getStep2().getMobilePhone());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 mobile_phone_validation Error is :=> " + mobile_phone_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 mobile_phone is :=> " + response.getStep2().getMobilePhone());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return mobile_phone_validation;
				} else {
					System.out.println("ST_2 mobile_phone inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 mobile_phone is :=> " + response.getStep2().getMobilePhone());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println("=====================================================");
				System.out.println("step_2 NOT ABLE TO insert mobile_phone ");
				System.out.println("=====================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write(
						"####step_2  NOT ABLE TO insert mobile_phone :=> " + response.getStep2().getMobilePhone());
				return "";
			}
		} else {
			System.out.println("****step_2 mobile_phone data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2
					.write("****step_2 mobile_phone data is not present :=> " + response.getStep2().getMobilePhone());
			return "";
		}
	}

	public static String insert_mail(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String mail) throws IOException {
		if (response.getStep2().isMail_data_present() == true) {
			try {
				WebElement mail_webelement;
				mail_webelement = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.mail))));
				mail_webelement.sendKeys(mail);
				Thread.sleep(1000);
				mail_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String mail_validation = driver.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_2 mail_validation Error is :=> " + mail_validation);
					System.out.println("ST_2 mail is :=> " + response.getStep2().getMail());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 mail_validation Error is :=> " + mail_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 mail is :=> " + response.getStep2().getMail());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return mail_validation;
				} else {
					System.out.println("ST_2 mail inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 mail is :=> " + response.getStep2().getMail());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println("===============================================================");
				System.out.println("step_2 NOT ABLE TO insert mail " + response.getStep2().getMail());
				System.out.println("===============================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_2  NOT ABLE TO insert mail :=> " + response.getStep2().getMail());
				return "";
			}
		} else {
			System.out.println("****step_2 Mail data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****step_2 Mail data is not present :=> " + mail);
			return "";
		}
	}

	// i dont know about this methode is used is somewhere or not

	/*
	 * public static String insert_pra_identity_type_of_report_holder(WebDriver
	 * driver,WebDriverWait wait,BufferedWriter bufferedWriter2,ModelData
	 * response ,String pra_identity_type_of_report_holder) throws IOException {
	 * if
	 * (response.getStep2().isPra_identity_type_of_report_holder_data_present()
	 * == true) { try { WebElement
	 * pra_identity_type_of_report_holder_webelement;
	 * pra_identity_type_of_report_holder_webelement =
	 * wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp
	 * .mail)))); pra_identity_type_of_report_holder_webelement.sendKeys(
	 * pra_identity_type_of_report_holder); Thread.sleep(1000);
	 * pra_identity_type_of_report_holder_webelement.sendKeys(Keys.ENTER);
	 * 
	 * if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) { //
	 * below 1st line is used to get error message and store in variable. String
	 * pra_identity_type_of_report_holder_validation =
	 * driver.findElement(By.xpath(xp.validation_popup)).getText(); System.out.
	 * println("ST_2 pra_identity_type_of_report_holder_validation Error is :=> "
	 * + pra_identity_type_of_report_holder_validation);
	 * System.out.println("ST_2 pra_identity_type_of_report_holder is :=> "
	 * +response.getStep2().getPra_identity_type_of_report_holder_data());
	 * bufferedWriter2.newLine(); bufferedWriter2.
	 * write("ST_2 pra_identity_type_of_report_holder_validation Error is :=> "
	 * +pra_identity_type_of_report_holder_validation);
	 * bufferedWriter2.newLine();
	 * bufferedWriter2.write("ST_2 pra_identity_type_of_report_holder is :=> "
	 * +response.getStep2().getPra_identity_type_of_report_holder_data());
	 * bufferedWriter2.newLine(); Thread.sleep(1000);
	 * popupDilogClose(driver,wait); Thread.sleep(1000);
	 * 
	 * return pra_identity_type_of_report_holder_validation; } else {
	 * System.out.
	 * println("ST_2 pra_identity_type_of_report_holder inserted successfully");
	 * bufferedWriter2.newLine();
	 * bufferedWriter2.write("ST_2 pra_identity_type_of_report_holder is :=> "
	 * +response.getStep2().getPra_identity_type_of_report_holder_data());
	 * bufferedWriter2.newLine(); return ""; } } catch (Exception e) {
	 * System.out.println(
	 * "=============================================================================================================================="
	 * ); System.out.
	 * println("step_2 NOT ABLE TO insert pra_identity_type_of_report_holder "
	 * +response.getStep2().getPra_identity_type_of_report_holder_data());
	 * System.out.println(
	 * "=============================================================================================================================="
	 * ); bufferedWriter2.newLine(); bufferedWriter2.
	 * write("####step_2  NOT ABLE TO insert pra_identity_type_of_report_holder :=> "
	 * +response.getStep2().getPra_identity_type_of_report_holder_data());
	 * return ""; } } else { System.out.
	 * println("****step_2 pra_identity_type_of_report_holder data is not present"
	 * ); bufferedWriter2.newLine(); bufferedWriter2.
	 * write("****step_2 pra_identity_type_of_report_holder data is not present :=> "
	 * + pra_identity_type_of_report_holder); return ""; } }
	 */
	public static void insert_pra_identity_type_of_report_holder(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response, int pra_identity_type_of_report_holder)
			throws IOException {
		if (response.getStep2().isPra_identity_type_of_report_holder_data_present() == true) {
			try {
				Thread.sleep(2000);
				switch (pra_identity_type_of_report_holder) {

				case 1:
					WebElement pra_ID_holder_of_the_report_is_id;
					pra_ID_holder_of_the_report_is_id = wait.until(ExpectedConditions
							.visibilityOf(driver.findElement(By.xpath(xp.pra_ID_holder_of_the_report_is_id))));
					pra_ID_holder_of_the_report_is_id.click();
					System.out.println("step_2_pra_ID_holder_of_the_report_is_id");
					bufferedWriter2.newLine();
					bufferedWriter2.write("step_2 pra_ID_holder_of_the_report_is :=> "
							+ response.getStep2().getPra_identity_type_of_report_holder_data());
					bufferedWriter2.write("  :=> pra_ID_holder_of_the_report_is selected as ID");

					break;
				case 2:
					WebElement pra_ID_holder_of_the_report_is_passport;
					pra_ID_holder_of_the_report_is_passport = wait.until(ExpectedConditions
							.visibilityOf(driver.findElement(By.xpath(xp.pra_ID_holder_of_the_report_is_passport))));
					pra_ID_holder_of_the_report_is_passport.click();
					System.out.println("step_2 pra_ID_holder_of_the_report_is_passport");
					bufferedWriter2.newLine();
					bufferedWriter2.write("step_2 pra_ID_holder_of_the_report_is :=> "
							+ response.getStep2().getPra_identity_type_of_report_holder_data());
					bufferedWriter2.write("  :=> pra_ID_holder_of_the_report_is selected as passport");

					break;
				case 3:
					WebElement pra_ID_holder_of_the_report_hf_private_company;
					pra_ID_holder_of_the_report_hf_private_company = wait.until(ExpectedConditions.visibilityOf(
							driver.findElement(By.xpath(xp.pra_ID_holder_of_the_report_hf_private_company))));
					pra_ID_holder_of_the_report_hf_private_company.click();
					System.out.println("step_2 pra_ID_holder_of_the_report_is selected as Company");
					bufferedWriter2.newLine();
					bufferedWriter2.write("step_2 type_of_applicant is :=> "
							+ response.getStep2().getPra_identity_type_of_report_holder_data());
					bufferedWriter2.write("  :=> pra_ID_holder_of_the_report_is selected as Company");

					break;

				default:
					System.out.println(
							"-------------------------------------------------------------------------------------------------------------------");
					System.out.println("Type_of_applicant got different from API, got response as :=> "
							+ response.getStep2().getPra_ID_holder_of_the_report_data());
					System.out.println(
							"-------------------------------------------------------------------------------------------------------------------");
					break;
				}
			} catch (Exception e) {
				System.out.println("=========================================================");
				System.out.println("step_2pra NOT ABLE TO SELECT pra_ID_holder_of_the_report ");
				System.out.println("step2_pra ID_holder_of_the_report is => "
						+ response.getStep2().getPra_ID_holder_of_the_report_data());
				System.out.println("=========================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("#### step_2_pra NOT ABLE TO SELECT pra_ID_holder_of_the_report AS :=> "
						+ response.getStep2().getPra_ID_holder_of_the_report_data());
			}
		} else {
			System.out.println("****step_2 pra_ID_holder_of_the_report DATA is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****step_2 pra_ID_holder_of_the_report DATA is not present :=> "
					+ response.getStep2().getPra_ID_holder_of_the_report_data());
		}
	}

	public static String insert_pra_ID_holder_of_the_report(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response, String pra_ID_holder_of_the_report) throws IOException {
		if (response.getStep2().isPra_ID_holder_of_the_report_data_present() == true) {
			try {
				WebElement pra_ID_holder_of_the_report_webelement;
				pra_ID_holder_of_the_report_webelement = wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath(xp.pra_ID_holder_of_the_report_is_id))));
				pra_ID_holder_of_the_report_webelement.sendKeys(pra_ID_holder_of_the_report);
				Thread.sleep(1000);
				pra_ID_holder_of_the_report_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String pra_ID_holder_of_the_report_validation = driver.findElement(By.xpath(xp.validation_popup))
							.getText();
					System.out.println("ST_2 pra_ID_holder_of_the_report_validation Error is :=> "
							+ pra_ID_holder_of_the_report_validation);
					System.out.println("ST_2 pra_ID_holder_of_the_report is :=> "
							+ response.getStep2().getPra_ID_holder_of_the_report_data());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_ID_holder_of_the_report_validation Error is :=> "
							+ pra_ID_holder_of_the_report_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_ID_holder_of_the_report is :=> "
							+ response.getStep2().getPra_ID_holder_of_the_report_data());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return pra_ID_holder_of_the_report_validation;
				} else {
					System.out.println("ST_2 pra_ID_holder_of_the_report inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_ID_holder_of_the_report is :=> "
							+ response.getStep2().getPra_ID_holder_of_the_report_data());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println(
						"==============================================================================================================================");
				System.out.println("step_2 NOT ABLE TO insert pra_ID_holder_of_the_report "
						+ response.getStep2().getPra_ID_holder_of_the_report_data());
				System.out.println(
						"==============================================================================================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_2  NOT ABLE TO insert pra_ID_holder_of_the_report :=> "
						+ response.getStep2().getPra_ID_holder_of_the_report_data());
				return "";
			}
		} else {
			System.out.println("****step_2 pra_ID_holder_of_the_report data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write(
					"****step_2 pra_ID_holder_of_the_report data is not present :=> " + pra_ID_holder_of_the_report);
			return "";
		}
	}

	public static String insert_pra_HF_holder_of_the_report(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response, String pra_HF_holder_of_the_report) throws IOException {
		if (response.getStep2().isPra_HF_holder_of_the_report_data_present() == true) {
			try {
				WebElement pra_HF_holder_of_the_report_webelement;
				pra_HF_holder_of_the_report_webelement = wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath(xp.pra_ID_holder_of_the_report_hf_private_company))));
				pra_HF_holder_of_the_report_webelement.sendKeys(pra_HF_holder_of_the_report);
				Thread.sleep(1000);
				pra_HF_holder_of_the_report_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String pra_HF_holder_of_the_report_validation = driver.findElement(By.xpath(xp.validation_popup))
							.getText();
					System.out.println("ST_2 pra_HF_holder_of_the_report_validation Error is :=> "
							+ pra_HF_holder_of_the_report_validation);
					System.out.println("ST_2 pra_HF_holder_of_the_report is :=> "
							+ response.getStep2().getPra_HF_holder_of_the_report_data());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_HF_holder_of_the_report_validation Error is :=> "
							+ pra_HF_holder_of_the_report_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_HF_holder_of_the_report is :=> "
							+ response.getStep2().getPra_HF_holder_of_the_report_data());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return pra_HF_holder_of_the_report_validation;
				} else {
					System.out.println("ST_2 pra_HF_holder_of_the_report inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_HF_holder_of_the_report is :=> "
							+ response.getStep2().getPra_HF_holder_of_the_report_data());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println(
						"==============================================================================================================================");
				System.out.println("step_2 NOT ABLE TO insert pra_HF_holder_of_the_report "
						+ response.getStep2().getPra_HF_holder_of_the_report_data());
				System.out.println(
						"==============================================================================================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_2  NOT ABLE TO insert pra_HF_holder_of_the_report :=> "
						+ response.getStep2().getPra_HF_holder_of_the_report_data());
				return "";
			}
		} else {
			System.out.println("****step_2 pra_HF_holder_of_the_report data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write(
					"****step_2 pra_HF_holder_of_the_report data is not present :=> " + pra_HF_holder_of_the_report);
			return "";
		}
	}

	public static String insert_pra_passport(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String pra_passport) throws IOException {
		if (response.getStep2().isPra_passport_data_present() == true) {
			try {
				WebElement pra_passport_webelement;
				pra_passport_webelement = wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath(xp.pra_ID_holder_of_the_report_is_passport))));
				pra_passport_webelement.sendKeys(pra_passport);
				Thread.sleep(1000);
				pra_passport_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String pra_passport_validation = driver.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_2 pra_passport_validation Error is :=> " + pra_passport_validation);
					System.out.println("ST_2 pra_passport is :=> " + response.getStep2().getPra_passport_data());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_passport_validation Error is :=> " + pra_passport_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_passport is :=> " + response.getStep2().getPra_passport_data());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return pra_passport_validation;
				} else {
					System.out.println("ST_2 pra_passport inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_passport is :=> " + response.getStep2().getPra_passport_data());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println(
						"==============================================================================================================================");
				System.out.println(
						"step_2 NOT ABLE TO insert pra_passport " + response.getStep2().getPra_passport_data());
				System.out.println(
						"==============================================================================================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_2  NOT ABLE TO insert pra_passport :=> "
						+ response.getStep2().getPra_passport_data());
				return "";
			}
		} else {
			System.out.println("****step_2 pra_passport data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****step_2 pra_passport data is not present :=> " + pra_passport);
			return "";
		}
	}

	public static String insert_pra_company_name_of_the_report_holder(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response, String pra_company_name_of_the_report_holder)
			throws IOException {
		if (response.getStep2().isPra_company_name_of_the_report_holder_data_present() == true) {
			try {
				WebElement pra_company_name_of_the_report_holder_webelement;
				pra_company_name_of_the_report_holder_webelement = wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath(xp.pra_company_name_of_the_report_holder))));
				pra_company_name_of_the_report_holder_webelement.sendKeys(pra_company_name_of_the_report_holder);
				Thread.sleep(1000);
				pra_company_name_of_the_report_holder_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String pra_company_name_of_the_report_holder_validation = driver
							.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_2 pra_company_name_of_the_report_holder_validation Error is :=> "
							+ pra_company_name_of_the_report_holder_validation);
					System.out.println("ST_2 pra_company_name_of_the_report_holder is :=>"
							+ response.getStep2().getPra_company_name_of_the_report_holder_data());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_company_name_of_the_report_holder_validation Error is :=> "
							+ pra_company_name_of_the_report_holder_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_company_name_of_the_report_holder is :=> "
							+ response.getStep2().getPra_company_name_of_the_report_holder_data());
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return pra_company_name_of_the_report_holder_validation;
				} else {
					System.out.println("ST_2 pra_company_name_of_the_report_holder inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_company_name_of_the_report_holder is :=> "
							+ response.getStep2().getPra_company_name_of_the_report_holder_data());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println(
						"==============================================================================================================================");
				System.out.println("step_2 NOT ABLE TO insert pra_company_name_of_the_report_holder "
						+ response.getStep2().getPra_company_name_of_the_report_holder_data());
				System.out.println(
						"==============================================================================================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_2  NOT ABLE TO insert pra_company_name_of_the_report_holder :=> "
						+ response.getStep2().getPra_company_name_of_the_report_holder_data());
				return "";
			}
		} else {
			System.out.println("****step_2 pra_company_name_of_the_report_holder data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****step_2 pra_company_name_of_the_report_holder data is not present :=> "
					+ pra_company_name_of_the_report_holder);
			return "";
		}
	}

	public static String insert_pra_first_name_of_the_report_owner(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response, String pra_first_name_of_the_report_owner)
			throws IOException {
		if (response.getStep2().isPra_first_name_of_the_report_owner_data_present() == true) {
			try {
				WebElement pra_first_name_of_the_report_owner_webelement;
				pra_first_name_of_the_report_owner_webelement = wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath(xp.pra_first_name_of_the_report_owner))));
				pra_first_name_of_the_report_owner_webelement.sendKeys(pra_first_name_of_the_report_owner);
				Thread.sleep(1000);
				pra_first_name_of_the_report_owner_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String pra_first_name_of_the_report_owner_validation = driver
							.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_2 pra_first_name_of_the_report_owner_validation Error is :=> "
							+ pra_first_name_of_the_report_owner_validation);
					System.out.println("ST_2 pra_first_name_of_the_report_owner is :=> "
							+ response.getStep2().getPra_first_name_of_the_report_owner_data());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_first_name_of_the_report_owner_validation Error is :=> "
							+ pra_first_name_of_the_report_owner_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_first_name_of_the_report_owner is :=> "
							+ response.getStep2().getPra_first_name_of_the_report_owner_data());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return pra_first_name_of_the_report_owner_validation;
				} else {
					System.out.println("ST_2 pra_first_name_of_the_report_owner inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_first_name_of_the_report_owner is :=> "
							+ response.getStep2().getPra_first_name_of_the_report_owner_data());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println(
						"==============================================================================================================================");
				System.out.println("step_2 NOT ABLE TO insert pra_first_name_of_the_report_owner "
						+ response.getStep2().getPra_first_name_of_the_report_owner_data());
				System.out.println(
						"==============================================================================================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_2  NOT ABLE TO insert pra_first_name_of_the_report_owner :=> "
						+ response.getStep2().getPra_first_name_of_the_report_owner_data());
				return "";
			}
		} else {
			System.out.println("****step_2 pra_first_name_of_the_report_owner data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****step_2 pra_first_name_of_the_report_owner data is not present :=> "
					+ pra_first_name_of_the_report_owner);
			return "";
		}
	}

	public static String insert_pra_last_name_of_the_report_owner(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response, String pra_last_name_of_the_report_owner)
			throws IOException {
		if (response.getStep2().isPra_last_name_of_the_report_owner_data_present() == true) {
			try {
				WebElement pra_last_name_of_the_report_owner_webelement;
				pra_last_name_of_the_report_owner_webelement = wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath(xp.pra_last_name_of_the_report_owner))));
				pra_last_name_of_the_report_owner_webelement.sendKeys(pra_last_name_of_the_report_owner);
				Thread.sleep(1000);
				pra_last_name_of_the_report_owner_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String pra_last_name_of_the_report_owner_validation = driver
							.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_2 pra_last_name_of_the_report_owner_validation Error is :=> "
							+ pra_last_name_of_the_report_owner_validation);
					System.out.println("ST_2 pra_last_name_of_the_report_owner is :=> "
							+ response.getStep2().getPra_last_name_of_the_report_owner_data());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_last_name_of_the_report_owner Error is :=> "
							+ pra_last_name_of_the_report_owner_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_last_name_of_the_report_owner is :=> "
							+ response.getStep2().getPra_last_name_of_the_report_owner_data());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return pra_last_name_of_the_report_owner_validation;
				} else {
					System.out.println("ST_2 pra_last_name_of_the_report_owner inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_last_name_of_the_report_owner is :=> "
							+ response.getStep2().getPra_last_name_of_the_report_owner_data());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println(
						"==============================================================================================================================");
				System.out.println("step_2 NOT ABLE TO insert pra_last_name_of_the_report_owner "
						+ response.getStep2().getPra_last_name_of_the_report_owner_data());
				System.out.println(
						"==============================================================================================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_2  NOT ABLE TO insert pra_last_name_of_the_report_owner :=> "
						+ response.getStep2().getPra_last_name_of_the_report_owner_data());
				return "";
			}
		} else {
			System.out.println("****step_2 pra_last_name_of_the_report_owner data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****step_2 pra_last_name_of_the_report_owner data is not present :=> "
					+ pra_last_name_of_the_report_owner);
			return "";
		}
	}

	public static String insert_pra_report_driver_license_number(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response, String pra_report_driver_license_number)
			throws IOException {
		if (response.getStep2().isPra_report_driver_license_number_data_present() == true) {
			try {
				WebElement pra_report_driver_license_number_webelement;
				pra_report_driver_license_number_webelement = wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath(xp.pra_report_driver_license_number))));
				pra_report_driver_license_number_webelement.sendKeys(pra_report_driver_license_number);
				Thread.sleep(1000);
				pra_report_driver_license_number_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String pra_report_driver_license_number_validation = driver
							.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_2 pra_report_driver_license_number_validation Error is :=> "
							+ pra_report_driver_license_number_validation);
					System.out.println("ST_2 pra_report_driver_license_number is :=> "
							+ response.getStep2().getPra_report_driver_license_number_data());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_report_driver_license_number_validation Error is :=> "
							+ pra_report_driver_license_number_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_report_driver_license_number_validation is :=> "
							+ response.getStep2().getPra_report_driver_license_number_data());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return pra_report_driver_license_number_validation;
				} else {
					System.out.println("ST_2 pra_report_driver_license_number inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_report_driver_license_number is :=> "
							+ response.getStep2().getPra_report_driver_license_number_data());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println(
						"==============================================================================================================================");
				System.out.println("step_2 NOT ABLE TO insert pra_report_driver_license_number "
						+ response.getStep2().getPra_report_driver_license_number_data());
				System.out.println(
						"==============================================================================================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_2  NOT ABLE TO insert pra_report_driver_license_number :=> "
						+ response.getStep2().getPra_report_driver_license_number_data());
				return "";
			}
		} else {
			System.out.println("****step_2 pra_report_driver_license_number data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****step_2 pra_report_driver_license_number data is not present :=> "
					+ pra_report_driver_license_number);
			return "";
		}
	}

	public static String insert_pra_resettlement(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String pra_resettlement) throws IOException {
		if (response.getStep2().isPra_resettlement_data_present() == true) {
			try {
				WebElement pra_resettlement_webelement;
				pra_resettlement_webelement = wait
						.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.pra_resettlement))));
				pra_resettlement_webelement.sendKeys(pra_resettlement);
				Thread.sleep(1000);
				pra_resettlement_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String pra_resettlement_validation = driver.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_2 pra_resettlement_validation Error is :=> " + pra_resettlement_validation);
					System.out
							.println("ST_2 pra_resettlement is :=> " + response.getStep2().getPra_resettlement_data());
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("ST_2 pra_resettlement_validation Error is :=> " + pra_resettlement_validation);
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("ST_2 pra_resettlement is :=> " + response.getStep2().getPra_resettlement_data());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return pra_resettlement_validation;
				} else {
					System.out.println("ST_2 pra_resettlement inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("ST_2 pra_resettlement is :=> " + response.getStep2().getPra_resettlement_data());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println(
						"==============================================================================================================================");
				System.out.println(
						"step_2 NOT ABLE TO insert pra_resettlement " + response.getStep2().getPra_resettlement_data());
				System.out.println(
						"==============================================================================================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_2  NOT ABLE TO insert pra_resettlement :=> "
						+ response.getStep2().getPra_resettlement_data());
				return "";
			}
		} else {
			System.out.println("****step_2 pra_resettlement data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****step_2 pra_resettlement data is not present :=> " + pra_resettlement);
			return "";
		}
	}

	public static String insert_pra_street(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String pra_street) throws IOException {
		if (response.getStep2().isPra_street_data_present() == true) {
			try {
				WebElement pra_street_webelement;
				pra_street_webelement = wait
						.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.pra_street))));
				pra_street_webelement.sendKeys(pra_street);
				Thread.sleep(1000);
				pra_street_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String pra_street_validation = driver.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_2 pra_street_validation Error is :=> " + pra_street_validation);
					System.out.println("ST_2 pra_street_validation is :=> " + response.getStep2().getPra_street_data());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_street_validation Error is :=> " + pra_street_validation);
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("ST_2 pra_street_validation is :=> " + response.getStep2().getPra_street_data());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return pra_street_validation;
				} else {
					System.out.println("ST_2 pra_street inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_street is :=> " + response.getStep2().getPra_street_data());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println(
						"==============================================================================================================================");
				System.out.println("step_2 NOT ABLE TO insert pra_street " + response.getStep2().getPra_street_data());
				System.out.println(
						"==============================================================================================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write(
						"####step_2  NOT ABLE TO insert pra_street :=> " + response.getStep2().getPra_street_data());
				return "";
			}
		} else {
			System.out.println("****step_2 pra_street data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****step_2 pra_street data is not present :=> " + pra_street);
			return "";
		}
	}

	public static String insert_pra_house_number(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String pra_house_number) throws IOException {
		if (response.getStep2().isPra_house_number_data_present() == true) {
			try {
				WebElement pra_house_number_webelement;
				pra_house_number_webelement = wait
						.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.pra_house_number))));
				pra_house_number_webelement.sendKeys(pra_house_number);
				Thread.sleep(1000);
				pra_house_number_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String pra_house_number_validation = driver.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_2 pra_house_number_validation Error is :=> " + pra_house_number_validation);
					System.out
							.println("ST_2 pra_house_number is :=> " + response.getStep2().getPra_house_number_data());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_house_number Error is :=> " + pra_house_number_validation);
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("ST_2 pra_house_number is :=> " + response.getStep2().getPra_house_number_data());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return pra_house_number_validation;
				} else {
					System.out.println("ST_2 pra_house_number inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("ST_2 pra_house_number is :=> " + response.getStep2().getPra_house_number_data());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println(
						"==============================================================================================================================");
				System.out.println(
						"step_2 NOT ABLE TO insert pra_house_number " + response.getStep2().getPra_house_number_data());
				System.out.println(
						"==============================================================================================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_2  NOT ABLE TO insert pra_house_number :=> "
						+ response.getStep2().getPra_house_number_data());
				return "";
			}
		} else {
			System.out.println("****step_2 pra_house_number data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****step_2 pra_house_number data is not present :=> " + pra_house_number);
			return "";
		}
	}

	public static String insert_pra_apartment_number(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response, String pra_apartment_number) throws IOException {
		if (response.getStep2().isPra_apartment_number_data_present() == true) {
			try {
				WebElement pra_apartment_number_webelement;
				pra_apartment_number_webelement = wait
						.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.pra_apartment_number))));
				pra_apartment_number_webelement.sendKeys(pra_apartment_number);
				Thread.sleep(1000);
				pra_apartment_number_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String pra_apartment_number_validation = driver.findElement(By.xpath(xp.validation_popup))
							.getText();
					System.out.println(
							"ST_2 pra_apartment_number_validation Error is :=> " + pra_apartment_number_validation);
					System.out.println(
							"ST_2 pra_apartment_number is :=> " + response.getStep2().getPra_apartment_number_data());
					bufferedWriter2.newLine();
					bufferedWriter2.write(
							"ST_2 pra_apartment_number_validation Error is :=> " + pra_apartment_number_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write(
							"ST_2 pra_apartment_number is :=> " + response.getStep2().getPra_apartment_number_data());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return pra_apartment_number_validation;
				} else {
					System.out.println("ST_2 pra_apartment_number inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write(
							"ST_2 pra_apartment_number is :=> " + response.getStep2().getPra_apartment_number_data());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println(
						"==============================================================================================================================");
				System.out.println("step_2 NOT ABLE TO insert pra_apartment_number "
						+ response.getStep2().getPra_apartment_number_data());
				System.out.println(
						"==============================================================================================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_2  NOT ABLE TO insert pra_apartment_number :=> "
						+ response.getStep2().getPra_apartment_number_data());
				return "";
			}
		} else {
			System.out.println("****step_2 pra_apartment_number data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****step_2 pra_apartment_number data is not present :=> " + pra_apartment_number);
			return "";
		}
	}

	public static String insert_pra_zip(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String pra_zip) throws IOException {
		if (response.getStep2().isPra_zip_data_present() == true) {
			try {
				WebElement pra_zip_webelement;
				pra_zip_webelement = wait
						.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.pra_zip))));
				pra_zip_webelement.sendKeys(pra_zip);
				Thread.sleep(1000);
				pra_zip_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String pra_zip_validation = driver.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_2 pra_zip_validation Error is :=> " + pra_zip_validation);
					System.out.println("ST_2 pra_zip is :=> " + response.getStep2().getPra_zip_data());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_zip_validation Error is :=> " + pra_zip_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_zip is :=> " + response.getStep2().getPra_zip_data());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return pra_zip_validation;
				} else {
					System.out.println("ST_2 pra_zip inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_zip is :=> " + response.getStep2().getPra_zip_data());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println(
						"==============================================================================================================================");
				System.out.println("step_2 NOT ABLE TO insert pra_zip " + response.getStep2().getPra_zip_data());
				System.out.println(
						"==============================================================================================================================");
				bufferedWriter2.newLine();
				bufferedWriter2
						.write("####step_2  NOT ABLE TO insert pra_zip :=> " + response.getStep2().getPra_zip_data());
				return "";
			}
		} else {
			System.out.println("****step_2 pra_zip data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****step_2 pra_zip data is not present :=> " + pra_zip);
			return "";
		}
	}

	public static String insert_pra_other_address(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String pra_other_address) throws IOException {
		if (response.getStep2().isPra_other_address_data_present() == true) {
			try {
				WebElement pra_other_address_webelement;
				pra_other_address_webelement = wait
						.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.pra_other_address))));
				pra_other_address_webelement.sendKeys(pra_other_address);
				Thread.sleep(1000);
				pra_other_address_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String pra_other_address_validation = driver.findElement(By.xpath(xp.validation_popup)).getText();
					System.out
							.println("ST_2 pra_other_address_validation Error is :=> " + pra_other_address_validation);
					System.out.println(
							"ST_2 pra_other_address is :=> " + response.getStep2().getPra_other_address_data());
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("ST_2 pra_other_address_validation Error is :=> " + pra_other_address_validation);
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("ST_2 pra_other_address is :=> " + response.getStep2().getPra_other_address_data());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return pra_other_address_validation;
				} else {
					System.out.println("ST_2 pra_other_address inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("ST_2 pra_other_address is :=> " + response.getStep2().getPra_other_address_data());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println(
						"==============================================================================================================================");
				System.out.println("step_2 NOT ABLE TO insert pra_other_address "
						+ response.getStep2().getPra_other_address_data());
				System.out.println(
						"==============================================================================================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_2  NOT ABLE TO insert pra_other_address :=> "
						+ response.getStep2().getPra_other_address_data());
				return "";
			}
		} else {
			System.out.println("****step_2 pra_other_address data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****step_2 pra_other_address data is not present :=> " + pra_other_address);
			return "";
		}
	}

	public static String insert_pra_phone_holder(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String pra_phone) throws IOException {
		if (response.getStep2().isPra_phone_data_present() == true) {
			try {
				WebElement pra_phone_webelement;
				pra_phone_webelement = wait
						.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.pra_phone))));
				pra_phone_webelement.sendKeys(pra_phone);
				Thread.sleep(1000);
				pra_phone_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String pra_phone_validation = driver.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_2 pra_phone_validation Error is :=> " + pra_phone_validation);
					System.out.println("ST_2 pra_phone is :=> " + response.getStep2().getPra_phone_data());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_phone_validation Error is :=> " + pra_phone_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_phone is :=> " + response.getStep2().getPra_phone_data());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return pra_phone_validation;
				} else {
					System.out.println("ST_2 pra_phone inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_phone is :=> " + response.getStep2().getPra_phone_data());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println(
						"==============================================================================================================================");
				System.out.println("step_2 NOT ABLE TO insert pra_phone " + response.getStep2().getPra_phone_data());
				System.out.println(
						"==============================================================================================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write(
						"####step_2  NOT ABLE TO insert pra_phone :=> " + response.getStep2().getPra_phone_data());
				return "";
			}
		} else {
			System.out.println("****step_2 pra_phone data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****step_2 pra_phone data is not present :=> " + pra_phone);
			return "";
		}
	}

	public static String insert_pra_mobile_phone(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String pra_mobile_phone) throws IOException {
		if (response.getStep2().isPra_mobile_phone_data_present() == true) {
			try {
				WebElement pra_mobile_phone_webelement;
				pra_mobile_phone_webelement = wait
						.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.pra_mobile_phone))));
				pra_mobile_phone_webelement.sendKeys(pra_mobile_phone);
				Thread.sleep(1000);
				pra_mobile_phone_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String pra_mobile_phone_validation = driver.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_2 pra_mobile_phone_validation Error is :=> " + pra_mobile_phone_validation);
					System.out
							.println("ST_2 pra_mobile_phone is :=> " + response.getStep2().getPra_mobile_phone_data());
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("ST_2 pra_mobile_phone_validation Error is :=> " + pra_mobile_phone_validation);
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("ST_2 pra_mobile_phone is :=> " + response.getStep2().getPra_mobile_phone_data());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return pra_mobile_phone_validation;
				} else {
					System.out.println("ST_2 pra_mobile_phone inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("ST_2 pra_mobile_phone is :=> " + response.getStep2().getPra_mobile_phone_data());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println(
						"==============================================================================================================================");
				System.out.println(
						"step_2 NOT ABLE TO insert pra_mobile_phone " + response.getStep2().getPra_mobile_phone_data());
				System.out.println(
						"==============================================================================================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_2  NOT ABLE TO insert pra_mobile_phone :=> "
						+ response.getStep2().getPra_mobile_phone_data());
				return "";
			}
		} else {
			System.out.println("****step_2 pra_mobile_phone data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****step_2 pra_mobile_phone data is not present :=> " + pra_mobile_phone);
			return "";
		}
	}

	public static String insert_pra_mail(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String pra_mail) throws IOException {
		if (response.getStep2().isPra_mail_data_present() == true) {
			try {
				WebElement pra_mail_webelement;
				pra_mail_webelement = wait
						.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.pra_mail))));
				pra_mail_webelement.sendKeys(pra_mail);
				Thread.sleep(1000);
				pra_mail_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String pra_mail_validation = driver.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("ST_2 pra_mail_validation Error is :=> " + pra_mail_validation);
					System.out.println("ST_2 pra_mail is :=> " + response.getStep2().getPra_mail_data());
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_mail_validation Error is :=> " + pra_mail_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_mail is :=> " + response.getStep2().getPra_mail_data());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					// Thread.sleep(1000);

					return pra_mail_validation;
				} else {
					System.out.println("ST_2 pra_mail inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("ST_2 pra_mail is :=> " + response.getStep2().getPra_mail_data());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println(
						"==============================================================================================================================");
				System.out.println("step_2 NOT ABLE TO insert pra_mail " + response.getStep2().getPra_mail_data());
				System.out.println(
						"==============================================================================================================================");
				bufferedWriter2.newLine();
				bufferedWriter2
						.write("####step_2  NOT ABLE TO insert pra_mail :=> " + response.getStep2().getPra_mail_data());
				return "";
			}
		} else {
			System.out.println("****step_2 pra_mail data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("****step_2 pra_mail data is not present :=> " + pra_mail);
			return "";
		}
	}

	//
	// // step 3 methodes is here below ======>>>>>
	//
	public static String insert_step_3_last_name(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String step_3_last_name) throws IOException {
		if (response.getStep3().isStep_3_last_name_data_present() == true) {
			try {
				WebElement step_3_last_name_webelement;
				step_3_last_name_webelement = wait.until(
						ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.step_3_last_name_input_box))));
				step_3_last_name_webelement.sendKeys(step_3_last_name);
				Thread.sleep(1000);
				step_3_last_name_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String step_3_last_name_validation1 = driver.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("step_3_last_name validation Error is :=> " + step_3_last_name_validation1);
					System.out.println("step_3_last_name is :=> " + response.getStep3().getLastName());
					bufferedWriter2.newLine();
					bufferedWriter2.write("===========");
					bufferedWriter2.newLine();
					bufferedWriter2.write("step 3 data");
					bufferedWriter2.newLine();
					bufferedWriter2.write("===========");
					bufferedWriter2.newLine();
					bufferedWriter2.write("step_3_last_name validation Error is :=> " + step_3_last_name_validation1);
					bufferedWriter2.newLine();
					bufferedWriter2.write("step_3_last_name is :=> " + response.getStep3().getLastName());
					bufferedWriter2.newLine();
					popupDilogClose(driver, wait);

					return step_3_last_name_validation1;
				} else {
					System.out.println("step_3_last_name inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("step_3_last_name is :=> " + response.getStep3().getLastName());
					bufferedWriter2.newLine();
					bufferedWriter2.write("===========");
					bufferedWriter2.newLine();
					bufferedWriter2.write("step 3 data");
					bufferedWriter2.newLine();
					bufferedWriter2.write("===========");
					bufferedWriter2.newLine();
					bufferedWriter2.write("step_3_last_name is :=> " + response.getStep3().getLastName());
					return "";
				}

			} catch (Exception e) {
				System.out.println("=====================================================");
				System.out.println("step_3 NOT ABLE TO insert step_3_last_name ");
				System.out.println("=====================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("===========");
				bufferedWriter2.newLine();
				bufferedWriter2.write("step 3 data");
				bufferedWriter2.newLine();
				bufferedWriter2.write("===========");
				bufferedWriter2.newLine();
				bufferedWriter2.write(
						"####step_3 NOT ABLE TO insert step_3_last_name :=> " + response.getStep3().getLastName());
				return "";
			}
		} else {
			System.out.println("**** step_3_last_name data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("===========");
			bufferedWriter2.newLine();
			bufferedWriter2.write("step 3 data");
			bufferedWriter2.newLine();
			bufferedWriter2.write("===========");
			bufferedWriter2.newLine();
			bufferedWriter2.write("**** step_3_last_name data is not present :=> " + response.getStep3().getLastName());
			return "";
		}
	}

	public static String insert_step_3_first_name(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String step_3_first_name) throws IOException {
		if (response.getStep3().isStep_3_first_name_data_present() == true) {
			try {
				WebElement step_3_first_name_webelement;
				step_3_first_name_webelement = wait.until(
						ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.step_3_first_name_input_box))));
				step_3_first_name_webelement.sendKeys(step_3_first_name);
				Thread.sleep(1000);
				step_3_first_name_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String step_3_first_name_validation = driver.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("step_3_first_name validation Error is :=> " + step_3_first_name_validation);
					System.out.println("step_3_first_name is :=> " + response.getStep3().getFirstName());
					bufferedWriter2.newLine();
					bufferedWriter2.write("step_3_first_name Validation Error is :=> " + step_3_first_name_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("step_3_first_name is :=> " + response.getStep3().getFirstName());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return step_3_first_name_validation;
				} else {
					System.out.println("step_3_first_name inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("step_3_first_name is :=> " + response.getStep3().getFirstName());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println("=====================================================");
				System.out.println("NOT ABLE TO insert step_3_first_name ");
				System.out.println("=====================================================");
				bufferedWriter2.newLine();
				bufferedWriter2
						.write("#### NOT ABLE TO insert step_3_first_name :=> " + response.getStep3().getFirstName());
				return "";
			}
		} else {
			System.out.println("**** step_3_first_name data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2
					.write("**** step_3_first_name data is not present :=> " + response.getStep3().getFirstName());
			return "";
		}
	}

	public static String insert_step_3_attorney_license_number(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response, String step_3_attorney_license_number)
			throws IOException {
		if (response.getStep3().isStep_3_attorney_license_number_data_present() == true) {
			try {
				WebElement step_3_attorney_license_number_webelement;
				step_3_attorney_license_number_webelement = wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath(xp.step_3_attorney_license_number_input_box))));
				step_3_attorney_license_number_webelement.sendKeys(step_3_attorney_license_number);
				Thread.sleep(1000);
				step_3_attorney_license_number_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String step_3_attorney_license_number_validation = driver.findElement(By.xpath(xp.validation_popup))
							.getText();
					System.out.println("step_3_attorney_license_number_ validation Error is :=> "
							+ step_3_attorney_license_number_validation);
					System.out.println(
							"step_3_attorney_license_number is :=> " + response.getStep3().getAttorneyLicenseNumber());
					bufferedWriter2.newLine();
					bufferedWriter2.write("step_3_attorney_license_number Validation Error is :=> "
							+ step_3_attorney_license_number_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write(
							"step_3_attorney_license_number is :=> " + response.getStep3().getAttorneyLicenseNumber());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return step_3_attorney_license_number_validation;
				} else {
					System.out.println("step_3_attorney_license_number inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write(
							"step_3_attorney_license_number is :=> " + response.getStep3().getAttorneyLicenseNumber());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println("=========================================================");
				System.out.println("step_3 NOT ABLE TO insert step_3_attorney_license_number ");
				System.out.println("=========================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_3 NOT ABLE TO insert step_3_attorney_license_number is :=> "
						+ response.getStep3().getAttorneyLicenseNumber());
				return "";
			}
		} else {
			System.out.println("**** step_3_attorney_license_number data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("**** step_3_attorney_license_number data is not present :=> "
					+ response.getStep3().getAttorneyLicenseNumber());
			return "";
		}
	}

	public static void insert_step_3_resettlement(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String step_3_resettlement) throws IOException {
		if (response.getStep3().step_3_resettlement_data_present == true) {
			try {
				WebElement step_3_resettlement_webelement;
				step_3_resettlement_webelement = wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath(xp.step_3_resettlement_input_box))));
				step_3_resettlement_webelement.sendKeys(step_3_resettlement);
				Thread.sleep(1000);
				// House_number_inputbox_webElement.sendKeys(Keys.TAB);
				System.out.println("step_3_resettlement inserted successfully");
				bufferedWriter2.newLine();
				bufferedWriter2.write("step_3_resettlement is :=> " + response.getStep3().getResettlement());
			} catch (Exception e) {
				System.out.println("=====================================================");
				System.out.println("step_3 NOT ABLE TO insert step_3_resettlement ");
				System.out.println("=====================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_3 NOT ABLE TO insert step_3_resettlement :=> "
						+ response.getStep3().getResettlement());
			}
		} else {
			System.out.println("**** step_3_resettlement data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2
					.write("**** step_3_resettlement data is not present :=> " + response.getStep3().getResettlement());
		}
	}

	public static void insert_step_3_street(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String step_3_street) throws IOException {
		if (response.getStep3().isStep_3_street_data_present() == true) {
			try {
				WebElement step_3_street_webelement;
				step_3_street_webelement = wait.until(
						ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.step_3_street_input_box))));
				step_3_street_webelement.sendKeys(step_3_street);
				Thread.sleep(1000);
				// House_number_inputbox_webElement.sendKeys(Keys.TAB);
				System.out.println("step_3_street inserted successfully");
				bufferedWriter2.newLine();
				bufferedWriter2.write("step_3_street is :=> " + response.getStep3().getStreet());
			} catch (Exception e) {
				System.out.println("=====================================================");
				System.out.println("step_3 NOT ABLE TO insert step_3_street ");
				System.out.println("=====================================================");
				bufferedWriter2.newLine();
				bufferedWriter2
						.write("####step_3 NOT ABLE TO insert step_3_street :=> " + response.getStep3().getStreet());
			}
		} else {
			System.out.println("**** step_3_street data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("**** step_3_street data is not present :=> " + response.getStep3().getStreet());
		}
	}

	public static String insert_step_3_house_number(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response, String step_3_house_number) throws IOException {
		if (response.getStep3().isStep_3_house_number_data_present() == true) {
			try {
				WebElement step_3_house_number_webelement;
				step_3_house_number_webelement = wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath(xp.step_3_house_number_input_box))));
				step_3_house_number_webelement.sendKeys(step_3_house_number);
				Thread.sleep(1000);
				step_3_house_number_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String step_3_house_number_validation = driver.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("step_3_house_number validation Error is :=> " + step_3_house_number_validation);
					System.out.println("step_3_house_number is :=> " + response.getStep3().getHouseNumber());
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("step_3_house_number Validation Error is :=> " + step_3_house_number_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("step_3_house_number is :=> " + response.getStep3().getHouseNumber());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return step_3_house_number_validation;
				} else {
					System.out.println("step_3_attorney_license_number inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("step_3_attorney_license_number is :=> " + response.getStep3().getHouseNumber());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println("=====================================================");
				System.out.println("step_3 NOT ABLE TO insert step_3_house_number ");
				System.out.println("=====================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_3 NOT ABLE TO insert step_3_house_number :=> "
						+ response.getStep3().getHouseNumber());
				return "";
			}
		} else {
			System.out.println("**** step_3_house_number data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2
					.write("**** step_3_house_number data is not present :=> " + response.getStep3().getHouseNumber());
			return "";
		}
	}

	public static String insert_step_3_apartment_number(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response, String step_3_apartment_number) throws IOException {
		if (response.getStep3().isStep_3_apartment_number_data_present() == true) {
			try {
				WebElement step_3_apartment_number_webelement;
				step_3_apartment_number_webelement = wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath(xp.step_3_apartment_number_input_box))));
				step_3_apartment_number_webelement.sendKeys(step_3_apartment_number);
				Thread.sleep(1000);
				step_3_apartment_number_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String step_3_apartment_number_validation = driver.findElement(By.xpath(xp.validation_popup))
							.getText();
					System.out.println(
							"step_3_apartment_number validation Error is :=> " + step_3_apartment_number_validation);
					System.out.println("step_3_apartment_number is :=> " + response.getStep3().getApartmentNumber());
					bufferedWriter2.newLine();
					bufferedWriter2.write(
							"step_3_apartment_number Validation Error is :=> " + step_3_apartment_number_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("step_3_apartment_number is :=> " + response.getStep3().getApartmentNumber());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return step_3_apartment_number_validation;
				} else {
					System.out.println("step_3_apartment_number inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("step_3_apartment_number is :=> " + response.getStep3().getApartmentNumber());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println("=====================================================");
				System.out.println("step_3 NOT ABLE TO insert step_3_apartment_number ");
				System.out.println("=====================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_3 NOT ABLE TO insert step_3_apartment_number :=> "
						+ response.getStep3().getApartmentNumber());
				return "";
			}
		} else {
			System.out.println("**** step_3_apartment_number data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write(
					"**** step_3_apartment_number data is not present :=> " + response.getStep3().getApartmentNumber());
			return "";
		}
	}

	public static String insert_step_3_zip(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String step_3_zip) throws IOException {
		if (response.getStep3().isStep_3_zip_data_present() == true) {
			try {
				WebElement step_3_zip_webelement;
				step_3_zip_webelement = wait
						.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.step_3_zip_input_box))));
				step_3_zip_webelement.sendKeys(step_3_zip);
				Thread.sleep(1000);
				step_3_zip_webelement.sendKeys(Keys.ENTER);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String step_3_zip_number_validation = driver.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("step_3_zip_number validation Error is :=> " + step_3_zip_number_validation);
					System.out.println("step_3_zip_number is :=> " + response.getStep3().getZip());
					bufferedWriter2.newLine();
					bufferedWriter2.write("step_3_zip_number Validation Error is :=> " + step_3_zip_number_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("step_3_zip_number is :=> " + response.getStep3().getZip());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return step_3_zip_number_validation;
				} else {
					System.out.println("step_3_zip_number inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("step_3_zip_number is :=> " + response.getStep3().getZip());
					bufferedWriter2.newLine();
					return "";
				}
			} catch (Exception e) {
				System.out.println("=====================================================");
				System.out.println("step_3 NOT ABLE TO insert step_3_zip " + response.getStep3().getZip());
				System.out.println("=====================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("#### step_3_NOT ABLE TO insert step_3_zip :=> " + response.getStep3().getZip());
				return "";
			}
		} else {
			System.out.println("**** step_3_zip data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("**** step_3_zip is not present :=> " + response.getStep3().getZip());
			return "";
		}
	}

	public static void insert_step_3_other_address(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String step_3_other_address) throws IOException {
		if (response.getStep3().isStep_3_other_address_data_present() == true) {
			try {
				WebElement step_3_other_address_webelement;
				step_3_other_address_webelement = wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath(xp.step_3_other_address_input_box))));
				step_3_other_address_webelement.sendKeys(step_3_other_address);
				Thread.sleep(1000);
				// House_number_inputbox_webElement.sendKeys(Keys.TAB);
				System.out.println("step_3_other_address inserted successfully");
				bufferedWriter2.newLine();
				bufferedWriter2.write("step_3_other_address is :=> " + response.getStep3().getOtherAddress());
			} catch (Exception e) {
				System.out.println("==============================================================================");
				System.out.println(
						"step_3 NOT ABLE TO insert step_3_other_address " + response.getStep3().getOtherAddress());
				System.out.println("==============================================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("#### step_3 NOT ABLE TO insert step_3_other_address :=> "
						+ response.getStep3().getOtherAddress());
			}
		} else {
			System.out.println("**** step_3_other_address data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write(
					"**** step_3_other_address data is not present :=> " + response.getStep3().getOtherAddress());
		}
	}

	public static String insert_step_3_phone(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String step_3_phone) throws IOException {
		if (response.getStep3().isStep_3_phone_data_present() == true) {
			try {
				WebElement step_3_phone_webelement;
				step_3_phone_webelement = wait.until(
						ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.step_3_phone_input_box))));
				step_3_phone_webelement.sendKeys(step_3_phone);
				Thread.sleep(1000);

				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String step_3_phone_number_validation = driver.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("step_3_phone_number validation Error is :=> " + step_3_phone_number_validation);
					System.out.println("step_3_phone_number is :=> " + response.getStep3().getPhone());
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("step_3_phone_number Validation Error is :=> " + step_3_phone_number_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("step_3_phone_number is :=> " + response.getStep3().getPhone());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return step_3_phone_number_validation;

				} else {
					System.out.println("step_3_phone_number inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("step_3_phone_number is :=> " + response.getStep3().getPhone());
					return "";
				}
			} catch (Exception e) {
				System.out.println("=====================================================");
				System.out.println("step_3 NOT ABLE TO insert step_3_phone ");
				System.out.println("=====================================================");
				bufferedWriter2.newLine();
				bufferedWriter2
						.write("####step_3 NOT ABLE TO insert step_3_phone :=> " + response.getStep3().getPhone());
				return "";
			}
		} else {
			System.out.println("**** step_3_phone data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("**** step_3_phone data is not present :=> " + response.getStep3().getPhone());
			return "";
		}
	}

	public static String insert_step_3_mobile_phone(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData response, String step_3_mobile_phone) throws IOException {
		if (response.getStep3().isStep_3_mobile_phone_data_present() == true) {
			try {
				WebElement step_3_mobile_phone_webelement;
				step_3_mobile_phone_webelement = wait.until(ExpectedConditions
						.visibilityOf(driver.findElement(By.xpath(xp.step_3_mobile_phone_input_box))));
				step_3_mobile_phone_webelement.sendKeys(step_3_mobile_phone);
				Thread.sleep(1000);
				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String step_3_mobile_phone_validation = driver.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("step_3_mobile_phone validation Error is :=> " + step_3_mobile_phone_validation);
					System.out.println("step_3_mobile_phone is :=> " + response.getStep3().getMobilePhone());
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("step_3_mobile_phone Validation Error is :=> " + step_3_mobile_phone_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("step_3_mobile_phone is :=> " + response.getStep3().getMobilePhone());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);

					return step_3_mobile_phone_validation;

				} else {
					System.out.println("step_3_mobile_phone inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("step_3_mobile_phone is :=> " + response.getStep3().getMobilePhone());
					return "";
				}
			} catch (Exception e) {
				System.out.println("=====================================================");
				System.out.println("step_3 NOT ABLE TO insert step_3_mobile_phone ");
				System.out.println("=====================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_3 NOT ABLE TO insert step_3_mobile_phone :=> "
						+ response.getStep3().getMobilePhone());
				return "";
			}
		} else {
			System.out.println("**** step_3_mobile_phone data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2
					.write("**** step_3_mobile_phone data is not present :=> " + response.getStep3().getMobilePhone());
			return "";
		}
	}

	public static String insert_step_3_mail(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String step_3_mail) throws IOException {
		if (response.getStep3().isStep_3_mail_data_present() == true) {
			try {
				WebElement step_3_mail_webelement;
				step_3_mail_webelement = wait
						.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.step_3_mail_input_box))));
				step_3_mail_webelement.sendKeys(step_3_mail);
				Thread.sleep(1000);
				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String step_3_mail_validation = driver.findElement(By.xpath(xp.validation_popup)).getText();
					System.out.println("step_3_mail validation Error is :=> " + step_3_mail_validation);
					System.out.println("step_3_mail is :=> " + response.getStep3().getMail());
					bufferedWriter2.newLine();
					bufferedWriter2.write("step_3_mail Validation Error is :=> " + step_3_mail_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("step_3_mail is :=> " + response.getStep3().getMail());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);
					return step_3_mail_validation;
				} else {
					System.out.println("step_3_mail inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("step_3_mail is :=> " + response.getStep3().getMail());
					return "";
				}
			} catch (Exception e) {
				System.out.println("=====================================================");
				System.out.println("step_3 NOT ABLE TO insert step_3_mail ");
				System.out.println("=====================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step_3 NOT ABLE TO insert step_3_mail :=> " + response.getStep3().getMail());
				return "";
			}
		} else {
			System.out.println("**** step_3_mail data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("**** step_3_mail data is not present :=> " + response.getStep3().getMail());
			return "";
		}
	}

	// in step 4 reason_details_data
	public static String insert_reason_detail(WebDriver driver, WebDriverWait wait, BufferedWriter bufferedWriter2,
			ModelData response, String reason_detail) throws IOException {
		if (response.getStep4().isReasonDetails() == true) {
			try {
				WebElement reason_detail_webelement;
				reason_detail_webelement = wait
						.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(xp.step4_Detail_inputbox))));
				reason_detail_webelement.sendKeys(reason_detail);
				Thread.sleep(1000);
				if (driver.findElement(By.xpath(xp.validation_popup)).isDisplayed()) {
					// below 1st line is used to get error message and store in
					// variable.
					String step_4_reason_detail_validation = driver.findElement(By.xpath(xp.validation_popup))
							.getText();
					System.out
							.println("step_4_reason_detail validation Error is :=> " + step_4_reason_detail_validation);
					System.out.println("step_4_reason_detail is :=> " + response.getStep3().getMail());
					bufferedWriter2.newLine();
					bufferedWriter2
							.write("step_4_reason_detail Validation Error is :=> " + step_4_reason_detail_validation);
					bufferedWriter2.newLine();
					bufferedWriter2.write("step_4_reason_detail is :=> " + response.getStep3().getMail());
					bufferedWriter2.newLine();
					Thread.sleep(1000);
					popupDilogClose(driver, wait);
					Thread.sleep(1000);
					return step_4_reason_detail_validation;
				} else {
					System.out.println("step_4_reason_detail inserted successfully");
					bufferedWriter2.newLine();
					bufferedWriter2.write("===========");
					bufferedWriter2.newLine();
					bufferedWriter2.write("step 4");
					bufferedWriter2.newLine();
					bufferedWriter2.write("===========");
					bufferedWriter2.newLine();
					bufferedWriter2.write("step 4 detail is :=> " + response.getStep4().getReasonDetails());
					return "";
				}

			} catch (Exception e) {
				System.out.println("=====================================================");
				System.out.println("step 4 NOT ABLE TO insert reason_detail ");
				System.out.println("=====================================================");
				bufferedWriter2.newLine();
				bufferedWriter2.write("===========");
				bufferedWriter2.newLine();
				bufferedWriter2.write("step 4");
				bufferedWriter2.newLine();
				bufferedWriter2.write("===========");
				bufferedWriter2.newLine();
				bufferedWriter2.write("####step 4 reason_detail is :=> " + response.getStep4().getReasonDetails());
				return "";
			}
		} else {
			System.out.println("****step 4 reason_detail data is not present");
			bufferedWriter2.newLine();
			bufferedWriter2.write("===========");
			bufferedWriter2.newLine();
			bufferedWriter2.write("step 4");
			bufferedWriter2.newLine();
			bufferedWriter2.write("===========");
			bufferedWriter2.newLine();
			bufferedWriter2.write(
					"****step 4 reason_detail data is not present :=> " + response.getStep4().getReasonDetails());
			return "";
		}
	}

	public static void call_me4() throws IOException {
		// String url =
		// "http://api.ipinfodb.com/v3/ip-city/?key=d64fcfdfacc213c7ddf4ef911dfe97b55e4696be3532bf8302876c09ebd06b&ip=74.125.45.100&format=json";
		String url = "http://500shekel.co.il/api/get-form-data";
		// String url = "http://api/users?page=2";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		// optional default is GET
		con.setRequestMethod("GET");
		// add request header
		// con.setRequestProperty("User-Agent", "Mozilla/5.0");
		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		// print in String
		System.out.println(response.toString());
		// Read JSON response and print
		try {
			JSONObject jsonObject = new JSONObject(response.toString());
			System.out.print(jsonObject);

			if (!jsonObject.isNull("status")) {
				System.out.println("status-: " + jsonObject.getLong("status")); // Stored
				statusdata = jsonObject.getLong("status");
			} else {
				System.err.println("status is NULL");
			}
			// System.out.println("id-: " + jsonObject.getString("id"));
			System.out.println("message is-: " + jsonObject.getString("message"));

			// String statusi = String.valueOf(jsonObject.get("status"));
			// System.out.println("status is "+statusi);

			String data = String.valueOf(jsonObject.get("data"));
			// System.out.println("im printing data =>>>>>>>>>>>>>>>>
			// "+statusi);

			JSONArray jsonArrayData = new JSONArray(data);
			System.out.println("---------------- Data length" + jsonArrayData.length());
			MyRunnable.userCount = jsonArrayData.length();
			for (int i = 0; i < jsonArrayData.length(); i++) {

				String items = jsonArrayData.get(i).toString();
				JSONObject step = new JSONObject(items);
				if (step.has("step1") && !step.isNull("step1")) {
					// get id
					String id = String.valueOf(step.get("id"));

					if (!Constants.ID_COUNT_LIST.contains((Integer) step.get("id")))
						Constants.ID_COUNT_LIST.add((Integer) step.get("id"));

					System.out.println("===========");
					System.out.println("id is " + id);
					System.out.println("===========");
					id_data = String.valueOf(step.get("id"));

					String step1str = String.valueOf(step.get("step1"));
					// System.out.println("step1"+step1str);
					JSONObject step1Objdata = new JSONObject(step1str);
					// System.out.println(step1Objdata.get("report_number"));

					// get report number
					System.out.println("=======================================================================");
					System.out.println("Step 1 data");
					System.out.println("=======================================================================");

					if (!step1Objdata.isNull("report_number")) {
						System.out.println("report_number-: " + step1Objdata.getString("report_number")); // Stored
						report_number_data = step1Objdata.getString("report_number");
						report_number_data_present = true;
					} else {
						System.err.println("report_number is NULL");
						report_number_data_present = false;
					}

					// get referral
					if (!step1Objdata.isNull("referral")) {
						System.out.println("referral-: " + step1Objdata.getString("referral")); // Stored
						referral_data = step1Objdata.getString("referral");
						referral_data_present = true;
					} else {
						System.err.println("referral is NULL");
						referral_data_present = false;
					}

				}
				if (step.has("step2") && !step.isNull("step2")) {
					String step1str = String.valueOf(step.get("step2"));
					// System.out.println("step2"+step1str);
					JSONObject step1Objdata = new JSONObject(step1str);
					System.out.println("=======================================================================");
					System.out.println("Step 2 data");
					System.out.println("=======================================================================");
					if (!step1Objdata.isNull("type_of_applicant")) {
						System.out.println("type_of_applicant-: " + step1Objdata.getString("type_of_applicant")); // Stored
						type_of_applicant_data = step1Objdata.getString("type_of_applicant");
						type_of_applicant_data_present = true;
					} else {
						System.err.println("type_of_applicant is NULL");
						type_of_applicant_data_present = false;
					}

					if (!step1Objdata.isNull("identity_type_of_report_holder")) {
						System.out.println("identity_type_of_report_holder-: "
								+ step1Objdata.getString("identity_type_of_report_holder")); // Stored
						identity_type_of_report_holder_data = step1Objdata.getString("identity_type_of_report_holder");
						identity_type_of_report_holder_data_present = true;
					} else {
						System.err.println("identity_type_of_report_holder is NULL");
						identity_type_of_report_holder_data_present = false;
					}

					// adding big data from
					// here==================================================================>>>>>>>>>>>>>
					if (!step1Objdata.isNull("ID_holder_of_the_report")) {
						System.out.println(
								"ID_holder_of_the_report-: " + step1Objdata.getString("ID_holder_of_the_report")); // Stored
						ID_holder_of_the_report_data = step1Objdata.getString("ID_holder_of_the_report");
						ID_holder_of_the_report_data_present = true;
					} else {
						System.err.println("ID_holder_of_the_report is NULL");
						ID_holder_of_the_report_data_present = false;
					}

					if (!step1Objdata.isNull("HF_holder_of_the_report")) {
						System.out.println("HF_holder_of_the_report-: " + step1Objdata.get("HF_holder_of_the_report"));
						HF_holder_of_the_report_data = step1Objdata.getString("HF_holder_of_the_report");
						HF_holder_of_the_report_data_present = true;// data
																	// found
					} else {
						System.err.println("HF_holder_of_the_report is NULL");
						HF_holder_of_the_report_data_present = false;
					}

					if (!step1Objdata.isNull("company_name_of_the_report_holder")) {
						System.out.println("company_name_of_the_report_holder-: "
								+ step1Objdata.getString("company_name_of_the_report_holder"));
						company_name_of_the_report_holder_data = step1Objdata
								.getString("company_name_of_the_report_holder");
						company_name_of_the_report_holder_data_present = true;
					} else {
						System.err.println("company_name_of_the_report_holder is NULL");
						company_name_of_the_report_holder_data_present = false;
					}

					if (!step1Objdata.isNull("first_name_of_the_report_owner")) {
						System.out.println("first_name_of_the_report_owner-: "
								+ step1Objdata.getString("first_name_of_the_report_owner"));
						first_name_of_the_report_owner_data = step1Objdata.getString("first_name_of_the_report_owner");
						first_name_of_the_report_owner_data_present = true;
					} else {
						System.err.println("first_name_of_the_report_owner is NULL");
						first_name_of_the_report_owner_data_present = false;
					}

					if (!step1Objdata.isNull("last_name_of_the_report_owner")) {
						System.out.println("last_name_of_the_report_owner-: "
								+ step1Objdata.getString("last_name_of_the_report_owner"));
						last_name_of_the_report_owner_data = step1Objdata.getString("last_name_of_the_report_owner");
						last_name_of_the_report_owner_data_present = true;
					} else {
						System.err.println("last_name_of_the_report_owner is NULL");
						last_name_of_the_report_owner_data_present = false;
					}

					if (!step1Objdata.isNull("report_driver_license_number")) {
						System.out.println("report_driver_license_number-: "
								+ step1Objdata.getString("report_driver_license_number"));
						report_driver_license_number_data = step1Objdata.getString("report_driver_license_number");
						report_driver_license_number_data_present = true;
					} else {
						System.err.println("report_driver_license_number is NULL");
						report_driver_license_number_data_present = false;
					}

					if (!step1Objdata.isNull("resettlement")) {
						System.out.println("resettlement-: " + step1Objdata.getString("resettlement"));
						resettlement_data = step1Objdata.getString("resettlement");
						resettlement_data_present = true;
					} else {
						System.err.println("resettlement is NULL");
						resettlement_data_present = false;
					}

					if (!step1Objdata.isNull("street")) {
						System.out.println("street-: " + step1Objdata.getString("street"));
						street_data = step1Objdata.getString("street");
						street_data_present = true;
					} else {
						System.err.println("street is NULL");
						street_data_present = false;
					}

					if (!step1Objdata.isNull("house_number")) {
						System.out.println("house_number-: " + step1Objdata.getString("house_number"));
						house_number_data = step1Objdata.getString("house_number");
						house_number_data_present = true;
					} else {
						System.err.println("house_number is NULL");
						house_number_data_present = false;
					}

					if (!step1Objdata.isNull("apartment_number")) {
						System.out.println("apartment_number-: " + step1Objdata.getString("apartment_number"));
						apartment_number_data = step1Objdata.getString("apartment_number");
						apartment_number_data_present = true;
					} else {
						System.err.println("apartment_number is NULL");
						apartment_number_data_present = false;
					}
					if (!step1Objdata.isNull("zip")) {
						System.out.println("zip-: " + step1Objdata.getString("zip"));
						zip_data = step1Objdata.getString("zip");
						zip_data_present = true;
					} else {
						System.err.println("zip is NULL");
						zip_data_present = false;
					}

					if (!step1Objdata.isNull("other_address")) {
						System.out.println("other_address-: " + step1Objdata.getString("other_address"));
						other_address_data = step1Objdata.getString("other_address");
						other_address_data_present = true;
					} else {
						System.err.println("other_address is NULL");
						other_address_data_present = false;
					}

					if (!step1Objdata.isNull("phone")) {
						System.out.println("phone-: " + step1Objdata.getString("phone"));
						phone_data = step1Objdata.getString("phone");
						phone_data_present = true;
					} else {
						System.err.println("phone is NULL");
						phone_data_present = false;
					}
					if (!step1Objdata.isNull("mobile_phone")) {
						System.out.println("mobile_phone-: " + step1Objdata.getString("mobile_phone"));
						mobile_phone_data = step1Objdata.getString("mobile_phone");
						mobile_phone_data_present = true;
					} else {
						System.err.println("mobile_phone is NULL");
						mobile_phone_data_present = false;
					}

					if (!step1Objdata.isNull("mail")) {
						System.out.println("mail-: " + step1Objdata.getString("mail"));
						mail_data = step1Objdata.getString("mail");
						mail_data_present = true;
					} else {
						System.err.println("mail is NULL");
						mail_data_present = false;
					}

					// adding new code to get new data in form
					// 2======================================>>>>>>>>>>>>>>>>>>>>>>>>>

					if (!step1Objdata.isNull("pra_identity_type_of_report_holder")) {
						System.out.println("pra_identity_type_of_report_holder-: "
								+ step1Objdata.getString("pra_identity_type_of_report_holder"));
						pra_identity_type_of_report_holder_data = step1Objdata
								.getString("pra_identity_type_of_report_holder");
						pra_identity_type_of_report_holder_data_present = true;
					} else {
						System.err.println("pra_identity_type_of_report_holder is NULL");
						pra_identity_type_of_report_holder_data_present = false;
					}

					if (!step1Objdata.isNull("pra_ID_holder_of_the_report")) {
						System.out.println("pra_ID_holder_of_the_report-: "
								+ step1Objdata.getString("pra_ID_holder_of_the_report"));
						pra_ID_holder_of_the_report_data = step1Objdata.getString("pra_ID_holder_of_the_report");
						pra_ID_holder_of_the_report_data_present = true;
					} else {
						System.err.println("pra_ID_holder_of_the_report is NULL");
						pra_ID_holder_of_the_report_data_present = false;
					}

					if (!step1Objdata.isNull("pra_HF_holder_of_the_report")) {
						System.out.println("mail-: " + step1Objdata.getString("pra_HF_holder_of_the_report"));
						pra_HF_holder_of_the_report_data = step1Objdata.getString("pra_HF_holder_of_the_report");
						pra_HF_holder_of_the_report_data_present = true;
					} else {
						System.err.println("pra_HF_holder_of_the_report is NULL");
						pra_HF_holder_of_the_report_data_present = false;
					}

					if (!step1Objdata.isNull("pra_passport")) {
						System.out.println("pra_passport-: " + step1Objdata.getString("pra_passport"));
						pra_passport_data = step1Objdata.getString("pra_passport");
						pra_passport_data_present = true;
					} else {
						System.err.println("pra_passport is NULL");
						pra_passport_data_present = false;
					}
					if (!step1Objdata.isNull("pra_company_name_of_the_report_holder")) {
						System.out.println("pra_company_name_of_the_report_holder-: "
								+ step1Objdata.getString("pra_company_name_of_the_report_holder"));
						pra_company_name_of_the_report_holder_data = step1Objdata
								.getString("pra_company_name_of_the_report_holder");
						pra_company_name_of_the_report_holder_data_present = true;
					} else {
						System.err.println("pra_company_name_of_the_report_holder is NULL");
						pra_company_name_of_the_report_holder_data_present = false;
					}
					if (!step1Objdata.isNull("pra_first_name_of_the_report_owner")) {
						System.out.println("pra_first_name_of_the_report_owner-: "
								+ step1Objdata.getString("pra_first_name_of_the_report_owner"));
						pra_first_name_of_the_report_owner_data = step1Objdata
								.getString("pra_first_name_of_the_report_owner");
						pra_first_name_of_the_report_owner_data_present = true;
					} else {
						System.err.println("pra_first_name_of_the_report_owner is NULL");
						pra_first_name_of_the_report_owner_data_present = false;
					}
					if (!step1Objdata.isNull("pra_last_name_of_the_report_owner")) {
						System.out.println("pra_last_name_of_the_report_owner-: "
								+ step1Objdata.getString("pra_last_name_of_the_report_owner"));
						pra_last_name_of_the_report_owner_data = step1Objdata
								.getString("pra_last_name_of_the_report_owner");
						pra_last_name_of_the_report_owner_data_present = true;
					} else {
						System.err.println("pra_last_name_of_the_report_owner is NULL");
						pra_last_name_of_the_report_owner_data_present = false;
					}
					if (!step1Objdata.isNull("pra_report_driver_license_number")) {
						System.out.println("pra_report_driver_license_number-: "
								+ step1Objdata.getString("pra_report_driver_license_number"));
						pra_report_driver_license_number_data = step1Objdata
								.getString("pra_report_driver_license_number");
						pra_report_driver_license_number_data_present = true;
					} else {
						System.err.println("pra_report_driver_license_number is NULL");
						pra_report_driver_license_number_data_present = false;
					}
					if (!step1Objdata.isNull("pra_resettlement")) {
						System.out.println("pra_resettlement-: " + step1Objdata.getString("pra_resettlement"));
						pra_resettlement_data = step1Objdata.getString("pra_resettlement");
						pra_resettlement_data_present = true;
					} else {
						System.err.println("pra_resettlement is NULL");
						pra_resettlement_data_present = false;
					}
					if (!step1Objdata.isNull("pra_street")) {
						System.out.println("pra_street-: " + step1Objdata.getString("pra_street"));
						pra_street_data = step1Objdata.getString("pra_street");
						pra_street_data_present = true;
					} else {
						System.err.println("pra_street is NULL");
						pra_street_data_present = false;
					}
					if (!step1Objdata.isNull("pra_house_number")) {
						System.out.println("pra_house_number-: " + step1Objdata.getString("pra_house_number"));
						pra_house_number_data = step1Objdata.getString("pra_house_number");
						pra_house_number_data_present = true;
					} else {
						System.err.println("pra_house_number is NULL");
						pra_house_number_data_present = false;
					}
					if (!step1Objdata.isNull("pra_apartment_number")) {
						System.out.println("pra_apartment_number-: " + step1Objdata.getString("pra_apartment_number"));
						pra_apartment_number_data = step1Objdata.getString("pra_apartment_number");
						pra_apartment_number_data_present = true;
					} else {
						System.err.println("pra_apartment_number is NULL");
						pra_apartment_number_data_present = false;
					}
					if (!step1Objdata.isNull("pra_zip")) {
						System.out.println("pra_zip-: " + step1Objdata.getString("pra_zip"));
						pra_zip_data = step1Objdata.getString("pra_zip");
						pra_zip_data_present = true;
					} else {
						System.err.println("pra_zip is NULL");
						pra_zip_data_present = false;
					}
					if (!step1Objdata.isNull("pra_other_address")) {
						System.out.println("pra_other_address-: " + step1Objdata.getString("pra_other_address"));
						pra_other_address_data = step1Objdata.getString("pra_other_address");
						pra_other_address_data_present = true;
					} else {
						System.err.println("pra_other_address is NULL");
						pra_other_address_data_present = false;
					}
					if (!step1Objdata.isNull("pra_phone")) {
						System.out.println("pra_phone-: " + step1Objdata.getString("pra_phone"));
						pra_phone_data = step1Objdata.getString("pra_phone");
						pra_phone_data_present = true;
					} else {
						System.err.println("pra_phone is NULL");
						pra_phone_data_present = false;
					}
					if (!step1Objdata.isNull("pra_mobile_phone")) {
						System.out.println("pra_mobile_phone-: " + step1Objdata.getString("pra_mobile_phone"));
						pra_mobile_phone_data = step1Objdata.getString("pra_mobile_phone");
						pra_mobile_phone_data_present = true;
					} else {
						System.err.println("pra_mobile_phone is NULL");
						pra_mobile_phone_data_present = false;
					}

					if (!step1Objdata.isNull("pra_mail")) {
						System.out.println("pra_mail-: " + step1Objdata.getString("pra_mail"));
						pra_mail_data = step1Objdata.getString("pra_mail");
						pra_mail_data_present = true;
					} else {
						System.err.println("pra_mail is NULL");
						pra_mail_data_present = false;
					}

				}
				// getting step 3 data from API code start from below.
				if (step.has("step3") && !step.isNull("step3")) {
					isStepThreeExsist = true;
					String step1str = String.valueOf(step.get("step3"));
					// System.out.println("step3"+step1str);
					JSONObject step1Objdata = new JSONObject(step1str);

					// step 3 last name get >>>>>>>>>>>>>>>>>>>>>>>>>>>
					System.out.println("=======================================================================");
					System.out.println("Step 3 data");
					System.out.println("=======================================================================");
					if (!step1Objdata.isNull("last_name")) {
						System.out.println("last_name-: " + step1Objdata.getString("last_name"));
						step_3_last_name_data = step1Objdata.getString("last_name");
						step_3_last_name_data_present = true;
					} else {
						System.err.println("step_3_last_name_data is NULL");
						step_3_last_name_data_present = false;
					}

					if (!step1Objdata.isNull("first_name")) {
						System.out.println("first_name-: " + step1Objdata.getString("first_name"));
						step_3_first_name_data = step1Objdata.getString("first_name");
						step_3_first_name_data_present = true;
					} else {
						System.err.println("step_3_first_name_data is NULL");
						step_3_first_name_data_present = false;
					}

					if (!step1Objdata.isNull("attorney_license_number")) {
						System.out.println(
								"attorney_license_number-: " + step1Objdata.getString("attorney_license_number"));
						step_3_attorney_license_number_data = step1Objdata.getString("attorney_license_number");
						step_3_attorney_license_number_data_present = true;
					} else {
						System.err.println("step_3_attorney_license_number_data is NULL");
						step_3_attorney_license_number_data_present = false;
					}

					if (!step1Objdata.isNull("resettlement")) {
						System.out.println("resettlement-: " + step1Objdata.getString("resettlement"));
						step_3_resettlement_data = step1Objdata.getString("resettlement");
						step_3_resettlement_data_present = true;
					} else {
						System.err.println("step_3_resettlement_data is NULL");
						step_3_resettlement_data_present = false;
					}

					if (!step1Objdata.isNull("street")) {
						System.out.println("street-: " + step1Objdata.getString("street"));
						step_3_street_data = step1Objdata.getString("street");
						step_3_street_data_present = true;
					} else {
						System.err.println("step_3_street_data is NULL");
						step_3_street_data_present = false;
					}

					if (!step1Objdata.isNull("house_number")) {
						System.out.println("house_number-: " + step1Objdata.getString("house_number"));
						step_3_house_number_data = step1Objdata.getString("house_number");
						step_3_house_number_data_present = true;
					} else {
						System.err.println("step_3_house_number_data is NULL");
						step_3_house_number_data_present = false;
					}

					if (!step1Objdata.isNull("apartment_number")) {
						System.out.println("apartment_number-: " + step1Objdata.getString("apartment_number"));
						step_3_apartment_number_data = step1Objdata.getString("apartment_number");
						step_3_apartment_number_data_present = true;
					} else {
						System.err.println("step_3_apartment_number_data is NULL");
						step_3_apartment_number_data_present = false;
					}

					if (!step1Objdata.isNull("zip")) {
						System.out.println("zip-: " + step1Objdata.getString("zip"));
						step_3_zip_data = step1Objdata.getString("zip");
						step_3_zip_data_present = true;
					} else {
						System.err.println("step_3_zip_data is NULL");
						step_3_zip_data_present = false;
					}

					if (!step1Objdata.isNull("other_address")) {
						System.out.println("other_address-: " + step1Objdata.getString("other_address"));
						step_3_other_address_data = step1Objdata.getString("other_address");
						step_3_other_address_data_present = true;
					} else {
						System.err.println("step_3_other_address_data is NULL");
						step_3_other_address_data_present = false;
					}

					if (!step1Objdata.isNull("phone")) {
						System.out.println("phone-: " + step1Objdata.getString("phone"));
						step_3_phone_data = step1Objdata.getString("phone");
						step_3_phone_data_present = true;
					} else {
						System.err.println("step_3_phone_data is NULL");
						step_3_phone_data_present = false;
					}

					if (!step1Objdata.isNull("mobile_phone")) {
						System.out.println("mobile_phone-: " + step1Objdata.getString("mobile_phone"));
						step_3_mobile_phone_data = step1Objdata.getString("mobile_phone");
						step_3_mobile_phone_data_present = true;
					} else {
						System.err.println("step_3_mobile_phone_data is NULL");
						step_3_mobile_phone_data_present = false;
					}

					if (!step1Objdata.isNull("mail")) {
						System.out.println("mail-: " + step1Objdata.getString("mail"));
						step_3_mail_data = step1Objdata.getString("mail");
						step_3_mail_data_present = true;
					} else {
						System.err.println("step_3_mail_data is NULL");
						step_3_mail_data_present = false;
					}
					// step 3 data get ending ---------->>>>>>>>>>>>>>
				} else {
					isStepThreeExsist = false;
				}
				if (step.has("step4") && !step.isNull("step4")) {
					String step1str = String.valueOf(step.get("step4"));
					JSONObject step1Objdata = new JSONObject(step1str);
					// System.out.println("step4"+step1str);
					System.out.println("=======================================================================");
					System.out.println("Step 4 data");
					System.out.println("=======================================================================");
					if (!step1Objdata.isNull("reason_details")) {
						System.out.println("reason_details-: " + step1Objdata.getString("reason_details"));
						reason_details_data = step1Objdata.getString("reason_details");
						reason_details_data_present = true;
					} else {
						System.err.println("reason_details is NULL");
						reason_details_data_present = false;
					}

				}
				if (step.has("step5") && !step.isNull("step5")) {
					String step1str = String.valueOf(step.get("step5"));
					// System.out.println("step5"+step1str);
					JSONObject step1Objdata = new JSONObject(step1str);
					System.out.println("=======================================================================");
					System.out.println("Step 5 data");
					System.out.println("=======================================================================");
					// get file_photo_report
					if (!step1Objdata.isNull("file_photo_report")) {
						System.out.println("file_photo_report-: " + step1Objdata.getString("file_photo_report"));
						file_photo_report_data = step1Objdata.getString("file_photo_report");
						file_photo_report_data_present = true;
					} else {
						System.err.println("file_photo_report is NULL ");
						file_photo_report_data_present = false;
					}

					// get file_photo_id_signature
					if (!step1Objdata.isNull("file_photo_id_signature")) {
						System.out.println(
								"file_photo_id_signature-: " + step1Objdata.getString("file_photo_id_signature"));
						file_photo_id_signature_data = step1Objdata.getString("file_photo_id_signature");
						file_photo_id_signature_data_present = true;
					} else {
						System.err.println("file_photo_id_signature is NULL");
						file_photo_id_signature_data_present = false;
					}

					// get Power_of_attorney_from_customer
					if (!step1Objdata.isNull("file_power_of_attorney_from_customer")) {

						System.out.println("file_power_of_attorney_from_customer -: "
								+ step1Objdata.getString("file_power_of_attorney_from_customer"));

						Power_of_attorney_from_customer_data = step1Objdata
								.getString("file_power_of_attorney_from_customer");

						Power_of_attorney_from_customer_data_present = true;
					} else {
						System.err.println("Power_of_attorney_from_customer is NULL");
						Power_of_attorney_from_customer_data_present = false;
					}

					// get file_signed_contract_of_the_tenant_data
					if (!step1Objdata.isNull("file_signed_contract_of_the_tenant")) {
						System.out.println("file_signed_contract_of_the_tenant-: "
								+ step1Objdata.getString("file_signed_contract_of_the_tenant"));
						file_signed_contract_of_the_tenant_data = step1Objdata
								.getString("file_signed_contract_of_the_tenant");
						file_signed_contract_of_the_tenant_data_present = true;
					} else {
						System.err.println("file_signed_contract_of_the_tenant is NULL");
						file_signed_contract_of_the_tenant_data_present = false;
					}

					// get file_rental_company_reporting
					if (!step1Objdata.isNull("file_rental_company_reporting")) {
						System.out.println("file_rental_company_reporting-: "
								+ step1Objdata.getString("file_rental_company_reporting"));
						file_rental_company_reporting_data = step1Objdata.getString("file_rental_company_reporting");
						file_rental_company_reporting_data_present = true;
					} else {
						System.err.println("file_rental_company_reporting is NULL");
						file_rental_company_reporting_data_present = false;
					}
					// new step5 data
					if (!step1Objdata.isNull("file_actual_driver_committe_the_offence")) {
						System.out.println("file_actual_driver_committe_the_offence-: "
								+ step1Objdata.getString("file_actual_driver_committe_the_offence"));
						file_actual_driver_committe_the_offence_data = step1Objdata
								.getString("file_actual_driver_committe_the_offence");
						file_actual_driver_committe_the_offence_data_present = true;
						System.err.println(
								"=================================================================================================================================");
						System.err.println("file_actual_driver_committe_the_offence_data_present is"
								+ file_actual_driver_committe_the_offence_data_present);
						System.err.println(
								"=================================================================================================================================");
					} else {
						System.err.println("file_actual_driver_committe_the_offence is NULL");
						file_actual_driver_committe_the_offence_data_present = false;
					}

					if (!step1Objdata.isNull("file_driver_license_photo")) {
						System.out.println(
								"file_driver_license_photo-: " + step1Objdata.getString("file_driver_license_photo"));
						file_driver_license_photo_data = step1Objdata.getString("file_driver_license_photo");
						file_driver_license_photo_data_present = true;
					} else {
						System.err.println("file_driver_license_photo is NULL");
						file_driver_license_photo_data_present = false;
					}

					if (!step1Objdata.isNull("file_request_to_be_judged")) {
						System.out.println(
								"file_request_to_be_judged-: " + step1Objdata.getString("file_request_to_be_judged"));
						file_request_to_be_judged_data = step1Objdata.getString("file_request_to_be_judged");
						file_request_to_be_judged_data_present = true;
					} else {
						System.err.println("file_request_to_be_judged is NULL");
						file_request_to_be_judged_data_present = false;
					}

					if (!step1Objdata.isNull("file_confirmation_of_payment")) {
						System.out.println("file_confirmation_of_payment-: "
								+ step1Objdata.getString("file_confirmation_of_payment"));
						file_confirmation_of_payment_data = step1Objdata.getString("file_confirmation_of_payment");
						file_confirmation_of_payment_data_present = true;
					} else {
						System.err.println("file_confirmation_of_payment is NULL");
						file_confirmation_of_payment_data_present = false;
					}

					if (!step1Objdata.isNull("file_proof")) {
						System.out.println("file_proof-: " + step1Objdata.getString("file_proof"));
						file_proof_data = step1Objdata.getString("file_proof");
						file_proof_data_present = true;
					} else {
						System.err.println("file_proof is NULL");
						file_proof_data_present = false;
					}

					if (!step1Objdata.isNull("file_transfer_of_ownership")) {
						System.out.println(
								"file_transfer_of_ownership-: " + step1Objdata.getString("file_transfer_of_ownership"));
						file_transfer_of_ownership_data = step1Objdata.getString("file_transfer_of_ownership");
						file_transfer_of_ownership_data_present = true;
					} else {
						System.err.println("file_transfer_of_ownership is NULL");
						file_transfer_of_ownership_data_present = false;
					}

					if (!step1Objdata.isNull("file_protocol_sign_by_lowyer")) {
						System.out.println("file_protocol_sign_by_lowyer-: "
								+ step1Objdata.getString("file_protocol_sign_by_lowyer"));
						file_protocol_sign_by_lowyer_data = step1Objdata.getString("file_protocol_sign_by_lowyer");
						file_protocol_sign_by_lowyer_data_present = true;
					} else {
						System.err.println("file_protocol_sign_by_lowyer is NULL");
						file_protocol_sign_by_lowyer_data_present = false;
					}

					if (!step1Objdata.isNull("file_address_approval")) {
						System.out
								.println("file_address_approval-: " + step1Objdata.getString("file_address_approval"));
						file_address_approval_data = step1Objdata.getString("file_address_approval");
						file_address_approval_data_present = true;
					} else {
						System.err.println("file_address_approval is NULL");
						file_address_approval_data_present = false;
					}
					// =================>>>>>>>> step 5 new data end here

				}

				if (!Constants.PROGRAM_IN_EXECUTION.contains(id_data)) {
					// step 1
					ModelStep1 step1 = new ModelStep1(report_number_data, report_number_data_present, referral_data,
							referral_data_present);

					// step2
					ModelStep2 step2 = new ModelStep2(type_of_applicant_data, type_of_applicant_data_present,
							ID_holder_of_the_report_data, ID_holder_of_the_report_data_present,
							HF_holder_of_the_report_data, HF_holder_of_the_report_data_present,
							company_name_of_the_report_holder_data, company_name_of_the_report_holder_data_present,
							first_name_of_the_report_owner_data, first_name_of_the_report_owner_data_present,
							last_name_of_the_report_owner_data, last_name_of_the_report_owner_data_present,
							report_driver_license_number_data, report_driver_license_number_data_present,
							resettlement_data, resettlement_data_present, street_data, street_data_present,
							house_number_data, house_number_data_present, apartment_number_data,
							apartment_number_data_present, zip_data, zip_data_present, other_address_data,
							other_address_data_present, phone_data, phone_data_present, mobile_phone_data,
							mobile_phone_data_present, mail_data, mail_data_present,
							identity_type_of_report_holder_data, identity_type_of_report_holder_data_present,

							pra_identity_type_of_report_holder_data, pra_identity_type_of_report_holder_data_present,
							pra_ID_holder_of_the_report_data, pra_ID_holder_of_the_report_data_present,
							pra_HF_holder_of_the_report_data, pra_HF_holder_of_the_report_data_present,
							pra_passport_data, pra_passport_data_present, pra_company_name_of_the_report_holder_data,
							pra_company_name_of_the_report_holder_data_present, pra_first_name_of_the_report_owner_data,
							pra_first_name_of_the_report_owner_data_present, pra_last_name_of_the_report_owner_data,
							pra_last_name_of_the_report_owner_data_present, pra_report_driver_license_number_data,
							pra_report_driver_license_number_data_present, pra_resettlement_data,
							pra_resettlement_data_present, pra_street_data, pra_street_data_present,
							pra_house_number_data, pra_house_number_data_present, pra_apartment_number_data,
							pra_apartment_number_data_present, pra_zip_data, pra_zip_data_present,
							pra_other_address_data, pra_other_address_data_present, pra_phone_data,
							pra_phone_data_present, pra_mobile_phone_data, pra_mobile_phone_data_present, pra_mail_data,
							pra_mail_data_present);

					// step3

					ModelStep3 step3 = new ModelStep3(step_3_last_name_data, step_3_last_name_data_present,
							step_3_first_name_data, step_3_first_name_data_present, step_3_attorney_license_number_data,
							step_3_attorney_license_number_data_present, step_3_resettlement_data,
							step_3_resettlement_data_present, step_3_street_data, step_3_street_data_present,
							step_3_house_number_data, step_3_house_number_data_present, step_3_apartment_number_data,
							step_3_apartment_number_data_present, step_3_zip_data, step_3_zip_data_present,
							step_3_other_address_data, step_3_other_address_data_present, step_3_phone_data,
							step_3_phone_data_present, step_3_mobile_phone_data, step_3_mobile_phone_data_present,
							step_3_mail_data, step_3_mail_data_present);

					// step4
					ModelStep4 step4 = new ModelStep4(reason_details_data, reason_details_data_present);

					// step5

					ModelStep5 step5 = new ModelStep5(file_photo_report_data, file_photo_report_data_present,
							file_photo_id_signature_data, file_photo_id_signature_data_present,
							Power_of_attorney_from_customer_data, Power_of_attorney_from_customer_data_present,
							file_signed_contract_of_the_tenant_data, file_signed_contract_of_the_tenant_data_present,
							file_rental_company_reporting_data, file_rental_company_reporting_data_present,

							file_actual_driver_committe_the_offence_data,
							file_actual_driver_committe_the_offence_data_present, file_driver_license_photo_data,
							file_driver_license_photo_data_present, file_request_to_be_judged_data,
							file_request_to_be_judged_data_present, file_confirmation_of_payment_data,
							file_confirmation_of_payment_data_present, file_proof_data, file_proof_data_present,
							file_transfer_of_ownership_data, file_transfer_of_ownership_data_present,
							file_protocol_sign_by_lowyer_data, file_protocol_sign_by_lowyer_data_present,
							file_address_approval_data, file_address_approval_data_present);

					if (isStepThreeExsist == true) {
						System.out.println("---------------Step 3 Data is Exists------------");
						modelData = new ModelData(id_data, step1, step2, step3, step4, step5, isStepThreeExsist);
					} else {
						System.out.println("---------------Step 3 Data is not Exists------------");
						modelData = new ModelData(id_data, step1, step2, step4, step5, isStepThreeExsist);
					}
				}

				// step 1
				report_number_data = "";

				referral_data = "";
				// step 2
				type_of_applicant_data = "";

				identity_type_of_report_holder_data = "";

				ID_holder_of_the_report_data = "";

				HF_holder_of_the_report_data = "";

				company_name_of_the_report_holder_data = "";

				first_name_of_the_report_owner_data = "";

				last_name_of_the_report_owner_data = "";

				report_driver_license_number_data = "";

				resettlement_data = "";

				street_data = "";

				house_number_data = "";

				apartment_number_data = "";

				zip_data = "";

				other_address_data = "";

				phone_data = "";

				mobile_phone_data = "";

				mail_data = "";

				// step2_pra data
				pra_identity_type_of_report_holder_data = "";
				pra_ID_holder_of_the_report_data = "";
				pra_HF_holder_of_the_report_data = "";
				pra_passport_data = "";
				pra_company_name_of_the_report_holder_data = "";
				pra_first_name_of_the_report_owner_data = "";
				pra_last_name_of_the_report_owner_data = "";
				pra_report_driver_license_number_data = "";
				pra_resettlement_data = "";
				pra_street_data = "";
				pra_house_number_data = "";
				pra_apartment_number_data = "";
				pra_zip_data = "";
				pra_other_address_data = "";
				pra_phone_data = "";
				pra_mobile_phone_data = "";
				pra_mail_data = "";

				// step 3
				step_3_last_name_data = "";

				step_3_first_name_data = "";

				step_3_attorney_license_number_data = "";

				step_3_resettlement_data = "";

				step_3_street_data = "";

				step_3_house_number_data = "";

				step_3_apartment_number_data = "";

				step_3_zip_data = "";

				step_3_other_address_data = "";

				step_3_phone_data = "";

				step_3_mobile_phone_data = "";

				step_3_mail_data = "";

				// step 4
				reason_details_data = "";

				// step5
				file_photo_report_data = "";

				file_photo_id_signature_data = "";

				Power_of_attorney_from_customer_data = "";

				file_signed_contract_of_the_tenant_data = "";

				file_rental_company_reporting_data = "";

				// adding new data in step 5

				file_actual_driver_committe_the_offence_data = "";
				file_driver_license_photo_data = "";
				file_request_to_be_judged_data = "";
				file_confirmation_of_payment_data = "";
				file_proof_data = "";
				file_transfer_of_ownership_data = "";
				file_protocol_sign_by_lowyer_data = "";
				file_address_approval_data = "";

				System.out.println("all varialble reset successfully");
				// System.out.println("---------------Out side of if else
				// condition------------");

				// Add All Data into HashMap
				mapAlldata.put((Integer) step.get("id"), modelData);
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public static void check_site_loaded_successfully_or_not(WebDriver driver, WebDriverWait wait,
			BufferedWriter bufferedWriter2, ModelData responseData) {
		String site_url_get = driver.getCurrentUrl();
		if (MyRunnable.site_url.equals(site_url_get)) {
			System.out.println("====================================================================");
			System.out.println("Site Loaded successfully, user id is :=> " + responseData.getId());
			System.out.println("user Report number is :=> " + responseData.getStep1().getReportNumber());
			System.out.println("====================================================================");
		} else {
			System.err.println("==================================================================");
			System.err.println("Site was not loaded properly, user id is :=> " + responseData.getId());
			System.err.println("user Report number is :=> " + responseData.getStep1().getReportNumber());
			System.err.println("==================================================================");
			driver.navigate().to(MyRunnable.site_url);
			check_site_loaded_successfully_or_not(driver, wait, bufferedWriter2, responseData);
		}
	}

}
