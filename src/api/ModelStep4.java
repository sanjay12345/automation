package api;

public class ModelStep4 {

	private String reasonDetails;
	private boolean reason_details_data;

	/**
	 *
	 * @param reasonDetails
	 */
	public ModelStep4(String reasonDetails, boolean reason_details_data) {
		this.reasonDetails = reasonDetails;
		this.reason_details_data = reason_details_data;
	}

	public String getReasonDetails() {
		return reasonDetails;
	}

	public boolean isReasonDetails() {
		return this.reason_details_data;
	}

	public void setReasonDetails(String reasonDetails) {
		this.reasonDetails = reasonDetails;
	}

}