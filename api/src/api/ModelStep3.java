package api;

public class ModelStep3 {
	
	private String lastName;
	boolean step_3_last_name_data_present;
	
	private String firstName;
	boolean step_3_first_name_data_present;
	
	private String attorneyLicenseNumber;
	boolean step_3_attorney_license_number_data_present; 
	
	private String resettlement;
	boolean step_3_resettlement_data_present;
	
	private String street;
	boolean step_3_street_data_present; 
	
	private String houseNumber;
	boolean step_3_house_number_data_present;
	
	private String apartmentNumber;
	boolean step_3_apartment_number_data_present; 
	
	private String zip;
	boolean step_3_zip_data_present; 
	
	private String otherAddress;
	boolean step_3_other_address_data_present; 
	
	private String phone;
	boolean step_3_phone_data_present;
	
	private String mobilePhone;
	boolean step_3_mobile_phone_data_present; 
	
	private String mail;
	boolean step_3_mail_data_present;
	


	/**
	*
	* @param resettlement
	* @param zip
	* @param lastName
	* @param firstName
	* @param mobilePhone
	* @param mail
	* @param phone
	* @param street
	* @param otherAddress
	* @param houseNumber
	* @param attorneyLicenseNumber
	* @param apartmentNumber
	*/
	
	public ModelStep3(String lastName, boolean step_3_last_name_data_present, String firstName, boolean step_3_first_name_data_present, String attorneyLicenseNumber, boolean step_3_attorney_license_number_data_present, String resettlement, boolean step_3_resettlement_data_present, String street, boolean step_3_street_data_present, String houseNumber, boolean step_3_house_number_data_present, String apartmentNumber, boolean step_3_apartment_number_data_present, String zip, boolean step_3_zip_data_present, String otherAddress, boolean step_3_other_address_data_present, String phone, boolean step_3_phone_data_present, String mobilePhone, boolean step_3_mobile_phone_data_present, String mail, boolean step_3_mail_data_present) {
		
        this.lastName = lastName;
        this.step_3_last_name_data_present = step_3_last_name_data_present;
        this.firstName = firstName;
        this.step_3_first_name_data_present = step_3_first_name_data_present;
        this.attorneyLicenseNumber = attorneyLicenseNumber;
        this.step_3_attorney_license_number_data_present = step_3_attorney_license_number_data_present;
        this.resettlement = resettlement;
        this.step_3_resettlement_data_present = step_3_resettlement_data_present;
        this.street = street;
        this.step_3_street_data_present = step_3_street_data_present;
        this.houseNumber = houseNumber;
        this.step_3_house_number_data_present = step_3_house_number_data_present;
        this.apartmentNumber = apartmentNumber;
        this.step_3_apartment_number_data_present = step_3_apartment_number_data_present;
        this.zip = zip;
        this.step_3_zip_data_present = step_3_zip_data_present;
        this.otherAddress = otherAddress;
        this.step_3_other_address_data_present = step_3_other_address_data_present;
        this.phone = phone;
        this.step_3_phone_data_present = step_3_phone_data_present;
        this.mobilePhone = mobilePhone;
        this.step_3_mobile_phone_data_present = step_3_mobile_phone_data_present;
        this.mail = mail;
        this.step_3_mail_data_present = step_3_mail_data_present;
        
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isStep_3_last_name_data_present() {
        return step_3_last_name_data_present;
    }

    public void setStep_3_last_name_data_present(boolean step_3_last_name_data_present) {
        this.step_3_last_name_data_present = step_3_last_name_data_present;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public boolean isStep_3_first_name_data_present() {
        return step_3_first_name_data_present;
    }

    public void setStep_3_first_name_data_present(boolean step_3_first_name_data_present) {
        this.step_3_first_name_data_present = step_3_first_name_data_present;
    }

    public String getAttorneyLicenseNumber() {
        return attorneyLicenseNumber;
    }

    public void setAttorneyLicenseNumber(String attorneyLicenseNumber) {
        this.attorneyLicenseNumber = attorneyLicenseNumber;
    }

    public boolean isStep_3_attorney_license_number_data_present() {
        return step_3_attorney_license_number_data_present;
    }

    public void setStep_3_attorney_license_number_data_present(boolean step_3_attorney_license_number_data_present) {
        this.step_3_attorney_license_number_data_present = step_3_attorney_license_number_data_present;
    }

    public String getResettlement() {
        return resettlement;
    }

    public void setResettlement(String resettlement) {
        this.resettlement = resettlement;
    }

    public boolean isStep_3_resettlement_data_present() {
        return step_3_resettlement_data_present;
    }

    public void setStep_3_resettlement_data_present(boolean step_3_resettlement_data_present) {
        this.step_3_resettlement_data_present = step_3_resettlement_data_present;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public boolean isStep_3_street_data_present() {
        return step_3_street_data_present;
    }

    public void setStep_3_street_data_present(boolean step_3_street_data_present) {
        this.step_3_street_data_present = step_3_street_data_present;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public boolean isStep_3_house_number_data_present() {
        return step_3_house_number_data_present;
    }

    public void setStep_3_house_number_data_present(boolean step_3_house_number_data_present) {
        this.step_3_house_number_data_present = step_3_house_number_data_present;
    }

    public String getApartmentNumber() {
        return apartmentNumber;
    }

    public void setApartmentNumber(String apartmentNumber) {
        this.apartmentNumber = apartmentNumber;
    }

    public boolean isStep_3_apartment_number_data_present() {
        return step_3_apartment_number_data_present;
    }

    public void setStep_3_apartment_number_data_present(boolean step_3_apartment_number_data_present) {
        this.step_3_apartment_number_data_present = step_3_apartment_number_data_present;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public boolean isStep_3_zip_data_present() {
        return step_3_zip_data_present;
    }

    public void setStep_3_zip_data_present(boolean step_3_zip_data_present) {
        this.step_3_zip_data_present = step_3_zip_data_present;
    }

    public String getOtherAddress() {
        return otherAddress;
    }

    public void setOtherAddress(String otherAddress) {
        this.otherAddress = otherAddress;
    }

    public boolean isStep_3_other_address_data_present() {
        return step_3_other_address_data_present;
    }

    public void setStep_3_other_address_data_present(boolean step_3_other_address_data_present) {
        this.step_3_other_address_data_present = step_3_other_address_data_present;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isStep_3_phone_data_present() {
        return step_3_phone_data_present;
    }

    public void setStep_3_phone_data_present(boolean step_3_phone_data_present) {
        this.step_3_phone_data_present = step_3_phone_data_present;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public boolean isStep_3_mobile_phone_data_present() {
        return step_3_mobile_phone_data_present;
    }

    public void setStep_3_mobile_phone_data_present(boolean step_3_mobile_phone_data_present) {
        this.step_3_mobile_phone_data_present = step_3_mobile_phone_data_present;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public boolean isStep_3_mail_data_present() {
        return step_3_mail_data_present;
    }

    public void setStep_3_mail_data_present(boolean step_3_mail_data_present) {
        this.step_3_mail_data_present = step_3_mail_data_present;
    }
    

}
