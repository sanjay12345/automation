package api;

import java.util.HashMap;
import java.util.Map;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class CallApi extends TimerTask {

	static boolean isStepThreeExsist = false;

	static int API_in_Catch = 1;
	// static ModelData response;
	// public static String Storege_folder_path = "D:\\AAAA\\";

	// public static int userCount = 1;

	public static data xp = new data();

	// static String site_url =
	// "https://forms.gov.il/globaldata/getsequence/gethtmlform.aspx?formType=driverapply@police.gov.il";

	public static String id_data = "";

	public static long statusdata;
	// step 1
	public static String report_number_data = "";
	public static boolean report_number_data_present;

	public static String referral_data = "";
	public static boolean referral_data_present;
	// step 2
	public static String type_of_applicant_data = "";
	public static boolean type_of_applicant_data_present;

	public static String identity_type_of_report_holder_data = "";
	public static boolean identity_type_of_report_holder_data_present;

	public static String ID_holder_of_the_report_data = "";
	public static boolean ID_holder_of_the_report_data_present;

	public static String HF_holder_of_the_report_data = "";
	public static boolean HF_holder_of_the_report_data_present;

	public static String company_name_of_the_report_holder_data = "";
	public static boolean company_name_of_the_report_holder_data_present;

	public static String first_name_of_the_report_owner_data = "";
	public static boolean first_name_of_the_report_owner_data_present;

	public static String last_name_of_the_report_owner_data = "";
	public static boolean last_name_of_the_report_owner_data_present;

	public static String report_driver_license_number_data = "";
	public static boolean report_driver_license_number_data_present;

	public static String resettlement_data = "";
	public static boolean resettlement_data_present;

	public static String street_data = "";
	public static boolean street_data_present;

	public static String house_number_data = "";
	public static boolean house_number_data_present;

	public static String apartment_number_data = "";
	public static boolean apartment_number_data_present;

	public static String zip_data = "";
	public static boolean zip_data_present;

	public static String other_address_data = "";
	public static boolean other_address_data_present;

	public static String phone_data = "";
	public static boolean phone_data_present;

	public static String mobile_phone_data = "";
	public static boolean mobile_phone_data_present;

	public static String mail_data = "";
	public static boolean mail_data_present;

	// step 2_pra data

	public static String pra_identity_type_of_report_holder_data = "";
	public static boolean pra_identity_type_of_report_holder_data_present;

	public static String pra_ID_holder_of_the_report_data = "";
	public static boolean pra_ID_holder_of_the_report_data_present;

	public static String pra_HF_holder_of_the_report_data = "";
	public static boolean pra_HF_holder_of_the_report_data_present;

	public static String pra_passport_data = "";
	public static boolean pra_passport_data_present;

	public static String pra_company_name_of_the_report_holder_data = "";
	public static boolean pra_company_name_of_the_report_holder_data_present;

	public static String pra_first_name_of_the_report_owner_data = "";
	public static boolean pra_first_name_of_the_report_owner_data_present;

	public static String pra_last_name_of_the_report_owner_data = "";
	public static boolean pra_last_name_of_the_report_owner_data_present;

	public static String pra_report_driver_license_number_data = "";
	public static boolean pra_report_driver_license_number_data_present;

	public static String pra_resettlement_data = "";
	public static boolean pra_resettlement_data_present;

	public static String pra_street_data = "";
	public static boolean pra_street_data_present;

	public static String pra_house_number_data = "";
	public static boolean pra_house_number_data_present;

	public static String pra_apartment_number_data = "";
	public static boolean pra_apartment_number_data_present;

	public static String pra_zip_data = "";
	public static boolean pra_zip_data_present;

	public static String pra_other_address_data = "";
	public static boolean pra_other_address_data_present;

	public static String pra_phone_data = "";
	public static boolean pra_phone_data_present;

	public static String pra_mobile_phone_data = "";
	public static boolean pra_mobile_phone_data_present;

	public static String pra_mail_data = "";
	public static boolean pra_mail_data_present;

	// step 3
	public static String step_3_last_name_data = "";
	public static boolean step_3_last_name_data_present;

	public static String step_3_first_name_data = "";
	public static boolean step_3_first_name_data_present;

	public static String step_3_attorney_license_number_data = "";
	public static boolean step_3_attorney_license_number_data_present;

	public static String step_3_resettlement_data = "";
	public static boolean step_3_resettlement_data_present;

	public static String step_3_street_data = "";
	public static boolean step_3_street_data_present;

	public static String step_3_house_number_data = "";
	public static boolean step_3_house_number_data_present;

	public static String step_3_apartment_number_data = "";
	public static boolean step_3_apartment_number_data_present;

	public static String step_3_zip_data = "";
	public static boolean step_3_zip_data_present;

	public static String step_3_other_address_data = "";
	public static boolean step_3_other_address_data_present;

	public static String step_3_phone_data = "";
	public static boolean step_3_phone_data_present;

	public static String step_3_mobile_phone_data = "";
	public static boolean step_3_mobile_phone_data_present;

	public static String step_3_mail_data = "";
	public static boolean step_3_mail_data_present;

	// step 4
	public static String reason_details_data = "";
	public static boolean reason_details_data_present;

	// step5
	public static String file_photo_report_data = "";
	public static boolean file_photo_report_data_present;

	public static String file_photo_id_signature_data = "";
	public static boolean file_photo_id_signature_data_present;

	public static String Power_of_attorney_from_customer_data = "";
	public static boolean Power_of_attorney_from_customer_data_present;

	public static String file_signed_contract_of_the_tenant_data = "";
	public static boolean file_signed_contract_of_the_tenant_data_present;

	public static String file_rental_company_reporting_data = "";
	public static boolean file_rental_company_reporting_data_present;

	// Step 5 new data
	public static String file_actual_driver_committe_the_offence_data= "";
	public static boolean file_actual_driver_committe_the_offence_data_present;
	 
	public static String file_driver_license_photo_data= "";
	public static boolean file_driver_license_photo_data_present;
	 
	public static String file_request_to_be_judged_data= "";
	public static boolean file_request_to_be_judged_data_present;
	 
	public static String file_confirmation_of_payment_data= "";
	public static boolean file_confirmation_of_payment_data_present;
	 
	public static String file_proof_data= "";
	public static boolean file_proof_data_present;
	 
	public static String file_transfer_of_ownership_data= "";
	public static boolean file_transfer_of_ownership_data_present;
	 
	public static String file_protocol_sign_by_lowyer_data= "";
	public static boolean file_protocol_sign_by_lowyer_data_present;
	 
	public static String file_address_approval_data= "";
	public static boolean file_address_approval_data_present;
	 

	// store all data into HashMap
	public static Map<Integer, Object> mapAlldata = new HashMap<Integer, Object>();

	public static boolean refreshpage;

	// public static BufferedWriter bufferedWriter2;

	// this is the variable to store the project location path.

	public static String Storege_folder_with_report_number_path = "D:\\AAAA\\" + report_number_data + "\\";

	static ExecutorService executor;

	public void run() {
		try {
			RequestApiData();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void RequestApiData() throws InterruptedException {
		try {
			// Send_HTTP_Request2.call_me();

			methodsclass.call_me4();

			int userCount = Constants.ID_COUNT_LIST.size();

			executor = Executors.newFixedThreadPool(userCount);

			System.out.println("This is the total count is : " + Constants.ID_COUNT_LIST.size());
			System.out.println("Processing Users : " + Constants.PROGRAM_IN_EXECUTION.size());

			Constants.processCount++;

			for (int i = 0; i < userCount; i++) {

				if (Constants.ID_COUNT_LIST.size() > 0) {
					if (!Constants.PROGRAM_IN_EXECUTION.contains(Constants.ID_COUNT_LIST.get(0))) {
						
						System.out.println("lOOP EXECUTION  TIME : " + i);
						Constants.PROGRAM_IN_EXECUTION.add(Constants.ID_COUNT_LIST.get(0));
						Constants.ID_COUNT_LIST.remove(0);
						ModelData responseData = (ModelData) mapAlldata.get(Constants.PROGRAM_IN_EXECUTION.get(0));
						
						
						Runnable worker = new MyRunnable(responseData);
						executor.execute(worker);
					}
					
				}
			}

		} catch (Exception e) {

			System.err.println("===========================================");
			System.err.println("there is some issue when calling API " + API_in_Catch);
			System.err.println("automation is waitng for 2 minuts");
			System.err.println("===========================================");
			API_in_Catch++;
			// e.printStackTrace();
			refreshpage = true;
			// TimeUnit.MINUTES.sleep(2);
		//	Thread.sleep(20000);
		}
		// END==========================THIS CODE IS USE TO GET DATA FROM
		// API=============================

		// }
	}

}
