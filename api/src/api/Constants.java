package api;

import java.util.ArrayList;
import java.util.List;

public class Constants {
	// this is list of all user id 
	public static List<Integer> ID_COUNT_LIST = new ArrayList<Integer>();
	public static List<Integer> PROGRAM_IN_EXECUTION = new ArrayList<Integer>();
	 
	//this is count how many time process is submited
	public static long processCount = 0; 
	 
}
