package api;

public class ModelStep5 {
	
	private String filePhotoReport;
	private boolean file_photo_report_data_present;
	
	private String filePhotoIdSignature;
	private boolean file_photo_id_signature_data_present;
	
	private String filePowerOfAttorneyFromCustomer;
	private boolean Power_of_attorney_from_customer_data_present;
	
	private String fileSignedContractOfTheTenant;
	private boolean file_signed_contract_of_the_tenant_data_present;
	
	private String fileRentalCompanyReporting;
	private boolean file_rental_company_reporting_data_present;
	
	private String file_actual_driver_committe_the_offence_data;
	private boolean file_actual_driver_committe_the_offence_data_present;
	 
	private String file_driver_license_photo_data;
	private boolean file_driver_license_photo_data_present;
	 
	private String file_request_to_be_judged_data;
	private boolean file_request_to_be_judged_data_present;
	 
	private String file_confirmation_of_payment_data;
	private boolean file_confirmation_of_payment_data_present;
	 
	private String file_proof_data;
	private boolean file_proof_data_present;
	 
	private String file_transfer_of_ownership_data;
	private boolean file_transfer_of_ownership_data_present;
	 
	private String file_protocol_sign_by_lowyer_data;
	private boolean file_protocol_sign_by_lowyer_data_present;
	 
	private String file_address_approval_data;
	private boolean file_address_approval_data_present;
	 

	/**
	 *
	 * @param filePowerOfAttorneyFromCustomer
	 * @param filePhotoReport
	 * @param filePhotoIdSignature
	 * @param fileRentalCompanyReporting
	 * @param fileSignedContractOfTheTenant
	 */
	public ModelStep5(
			String filePhotoReport, boolean file_photo_report_data_present, 
			String filePhotoIdSignature, boolean file_photo_id_signature_data_present, 
			String filePowerOfAttorneyFromCustomer, boolean power_of_attorney_from_customer_data_present, 
			String fileSignedContractOfTheTenant, boolean file_signed_contract_of_the_tenant_data_present, 
			String fileRentalCompanyReporting, boolean file_rental_company_reporting_data_present,
			
			String file_actual_driver_committe_the_offence_data,  boolean file_actual_driver_committe_the_offence_data_present,  
			String file_driver_license_photo_data,  boolean file_driver_license_photo_data_present,  
			String file_request_to_be_judged_data,  boolean file_request_to_be_judged_data_present,  
			String file_confirmation_of_payment_data,  boolean file_confirmation_of_payment_data_present,  
			String file_proof_data,  boolean file_proof_data_present,  
			String file_transfer_of_ownership_data,  boolean file_transfer_of_ownership_data_present,  
			String file_protocol_sign_by_lowyer_data,  boolean file_protocol_sign_by_lowyer_data_present,  
			String file_address_approval_data,  boolean file_address_approval_data_present  
			) {
		

		
		
		
        this.filePhotoReport = filePhotoReport;
        this.file_photo_report_data_present = file_photo_report_data_present;
        
        this.filePhotoIdSignature = filePhotoIdSignature;
        this.file_photo_id_signature_data_present = file_photo_id_signature_data_present;
        
        this.filePowerOfAttorneyFromCustomer = filePowerOfAttorneyFromCustomer;
        this.Power_of_attorney_from_customer_data_present = power_of_attorney_from_customer_data_present;
        
        this.fileSignedContractOfTheTenant = fileSignedContractOfTheTenant;
        this.file_signed_contract_of_the_tenant_data_present = file_signed_contract_of_the_tenant_data_present;
       
        this.fileRentalCompanyReporting = fileRentalCompanyReporting;
        this.file_rental_company_reporting_data_present = file_rental_company_reporting_data_present;
        
        this.file_actual_driver_committe_the_offence_data = file_actual_driver_committe_the_offence_data;
        this.file_actual_driver_committe_the_offence_data_present = file_actual_driver_committe_the_offence_data_present;
         
        this.file_driver_license_photo_data = file_driver_license_photo_data;
        this.file_driver_license_photo_data_present = file_driver_license_photo_data_present;
         
        this.file_request_to_be_judged_data = file_request_to_be_judged_data;
        this.file_request_to_be_judged_data_present = file_request_to_be_judged_data_present;
         
        this.file_confirmation_of_payment_data = file_confirmation_of_payment_data;
        this.file_confirmation_of_payment_data_present = file_confirmation_of_payment_data_present;
         
        this.file_proof_data = file_proof_data;
        this.file_proof_data_present = file_proof_data_present;
         
        this.file_transfer_of_ownership_data = file_transfer_of_ownership_data;
        this.file_transfer_of_ownership_data_present = file_transfer_of_ownership_data_present;
         
        this.file_protocol_sign_by_lowyer_data = file_protocol_sign_by_lowyer_data;
        this.file_protocol_sign_by_lowyer_data_present = file_protocol_sign_by_lowyer_data_present;
         
        this.file_address_approval_data = file_address_approval_data;
        this.file_address_approval_data_present = file_address_approval_data_present;
        
        
        System.out.println("=>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> this is my modal data "+file_actual_driver_committe_the_offence_data);
    }

    public String getFilePhotoReport() {
        return filePhotoReport;
    }

    public void setFilePhotoReport(String filePhotoReport) {
        this.filePhotoReport = filePhotoReport;
    }

    public boolean isFile_photo_report_data_present() {
        return file_photo_report_data_present;
    }

    public void setFile_photo_report_data_present(boolean file_photo_report_data_present) {
        this.file_photo_report_data_present = file_photo_report_data_present;
    }

    public String getFilePhotoIdSignature() {
        return filePhotoIdSignature;
    }

    public void setFilePhotoIdSignature(String filePhotoIdSignature) {
        this.filePhotoIdSignature = filePhotoIdSignature;
    }

    public boolean isFile_photo_id_signature_data_present() {
        return file_photo_id_signature_data_present;
    }

    public void setFile_photo_id_signature_data_present(boolean file_photo_id_signature_data_present) {
        this.file_photo_id_signature_data_present = file_photo_id_signature_data_present;
    }

    public String getFilePowerOfAttorneyFromCustomer() {
        return this.filePowerOfAttorneyFromCustomer;
    }

    public void setFilePowerOfAttorneyFromCustomer(String filePowerOfAttorneyFromCustomer) {
        this.filePowerOfAttorneyFromCustomer = filePowerOfAttorneyFromCustomer;
    }

    public boolean isPower_of_attorney_from_customer_data_present() {
        return Power_of_attorney_from_customer_data_present;
    }

    public void setPower_of_attorney_from_customer_data_present(boolean power_of_attorney_from_customer_data_present) {
        Power_of_attorney_from_customer_data_present = power_of_attorney_from_customer_data_present;
    }

    public String getFileSignedContractOfTheTenant() {
        return fileSignedContractOfTheTenant;
    }

    public void setFileSignedContractOfTheTenant(String fileSignedContractOfTheTenant) {
        this.fileSignedContractOfTheTenant = fileSignedContractOfTheTenant;
    }

    public boolean isFile_signed_contract_of_the_tenant_data_present() {
        return file_signed_contract_of_the_tenant_data_present;
    }

    public void setFile_signed_contract_of_the_tenant_data_present(boolean file_signed_contract_of_the_tenant_data_present) {
        this.file_signed_contract_of_the_tenant_data_present = file_signed_contract_of_the_tenant_data_present;
    }

    public String getFileRentalCompanyReporting() {
        return fileRentalCompanyReporting;
    }

    public void setFileRentalCompanyReporting(String fileRentalCompanyReporting) {
        this.fileRentalCompanyReporting = fileRentalCompanyReporting;
    }

    public boolean isFile_rental_company_reporting_data_present() {
        return file_rental_company_reporting_data_present;
    }

    public void setFile_rental_company_reporting_data_present(boolean file_rental_company_reporting_data_present) {
        this.file_rental_company_reporting_data_present = file_rental_company_reporting_data_present;
    }
    
    public String getFile_actual_driver_committe_the_offence_data() {
        return file_actual_driver_committe_the_offence_data;
    }

    public void setFile_actual_driver_committe_the_offence_data(String file_actual_driver_committe_the_offence_data) {
        this.file_actual_driver_committe_the_offence_data = file_actual_driver_committe_the_offence_data;
    }

    public boolean isFile_actual_driver_committe_the_offence_data_present() {
        return file_actual_driver_committe_the_offence_data_present;
    }

    public void setFile_actual_driver_committe_the_offence_data_present(boolean file_actual_driver_committe_the_offence_data_present) {
        this.file_actual_driver_committe_the_offence_data_present = file_actual_driver_committe_the_offence_data_present;
    }

    public String getFile_driver_license_photo_data() {
        return file_driver_license_photo_data;
    }

    public void setFile_driver_license_photo_data(String file_driver_license_photo_data) {
        this.file_driver_license_photo_data = file_driver_license_photo_data;
    }

    public boolean isFile_driver_license_photo_data_present() {
        return file_driver_license_photo_data_present;
    }

    public void setFile_driver_license_photo_data_present(boolean file_driver_license_photo_data_present) {
        this.file_driver_license_photo_data_present = file_driver_license_photo_data_present;
    }

    public String getFile_request_to_be_judged_data() {
        return file_request_to_be_judged_data;
    }

    public void setFile_request_to_be_judged_data(String file_request_to_be_judged_data) {
        this.file_request_to_be_judged_data = file_request_to_be_judged_data;
    }

    public boolean isFile_request_to_be_judged_data_present() {
        return file_request_to_be_judged_data_present;
    }

    public void setFile_request_to_be_judged_data_present(boolean file_request_to_be_judged_data_present) {
        this.file_request_to_be_judged_data_present = file_request_to_be_judged_data_present;
    }

    public String getFile_confirmation_of_payment_data() {
        return file_confirmation_of_payment_data;
    }

    public void setFile_confirmation_of_payment_data(String file_confirmation_of_payment_data) {
        this.file_confirmation_of_payment_data = file_confirmation_of_payment_data;
    }

    public boolean isFile_confirmation_of_payment_data_present() {
        return file_confirmation_of_payment_data_present;
    }

    public void setFile_confirmation_of_payment_data_present(boolean file_confirmation_of_payment_data_present) {
        this.file_confirmation_of_payment_data_present = file_confirmation_of_payment_data_present;
    }

    public String getFile_proof_data() {
        return file_proof_data;
    }

    public void setFile_proof_data(String file_proof_data) {
        this.file_proof_data = file_proof_data;
    }

    public boolean isFile_proof_data_present() {
        return file_proof_data_present;
    }

    public void setFile_proof_data_present(boolean file_proof_data_present) {
        this.file_proof_data_present = file_proof_data_present;
    }

    public String getFile_transfer_of_ownership_data() {
        return file_transfer_of_ownership_data;
    }

    public void setFile_transfer_of_ownership_data(String file_transfer_of_ownership_data) {
        this.file_transfer_of_ownership_data = file_transfer_of_ownership_data;
    }

    public boolean isFile_transfer_of_ownership_data_present() {
        return file_transfer_of_ownership_data_present;
    }

    public void setFile_transfer_of_ownership_data_present(boolean file_transfer_of_ownership_data_present) {
        this.file_transfer_of_ownership_data_present = file_transfer_of_ownership_data_present;
    }

    public String getFile_protocol_sign_by_lowyer_data() {
        return file_protocol_sign_by_lowyer_data;
    }

    public void setFile_protocol_sign_by_lowyer_data(String file_protocol_sign_by_lowyer_data) {
        this.file_protocol_sign_by_lowyer_data = file_protocol_sign_by_lowyer_data;
    }

    public boolean isFile_protocol_sign_by_lowyer_data_present() {
        return file_protocol_sign_by_lowyer_data_present;
    }

    public void setFile_protocol_sign_by_lowyer_data_present(boolean file_protocol_sign_by_lowyer_data_present) {
        this.file_protocol_sign_by_lowyer_data_present = file_protocol_sign_by_lowyer_data_present;
    }

    public String getFile_address_approval_data() {
        return file_address_approval_data;
    }

    public void setFile_address_approval_data(String file_address_approval_data) {
        this.file_address_approval_data = file_address_approval_data;
    }

    public boolean isFile_address_approval_data_present() {
        return file_address_approval_data_present;
    }

    public void setFile_address_approval_data_present(boolean file_address_approval_data_present) {
        this.file_address_approval_data_present = file_address_approval_data_present;
    }

}
