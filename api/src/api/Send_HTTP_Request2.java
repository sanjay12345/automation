package api;

import java.io.IOException;
import java.util.Timer;

import org.json.JSONException;

public class Send_HTTP_Request2 {
	// this variable is use to get the project folder path, than we use this variable to access the chrome driver
	public static String path;

	// error site URL :- https://forms.gov.il/globaldata/getsequence/Error.aspx?eId=FormLoadError&ref=http%3a%2f%2fforms.gov.il%2fglobaldata%2fgetsequence%2fgethtmlform.aspx%3fformType%3ddriverapply%40police.gov.il
	static String site_url = "https://forms.gov.il/globaldata/getsequence/gethtmlform.aspx?formType=driverapply@police.gov.il";

	static boolean firstTime = true;
	public static void main(String[] args) throws InterruptedException, IOException, JSONException {

		// START====================THIS CODE IS USE TO GET DATA FROM API
		if(firstTime){
			firstTime = false;
			Constants.PROGRAM_IN_EXECUTION.clear();
		}
		CallApi.RequestApiData();
		// END====================THIS CODE IS USE TO GET DATA FROM API

		// below function will call CallApi.RequestApiData(); method in every 5
		// second

		Timer timer = new Timer();
		timer.schedule(new CallApi(), 0, 10000);
		
	}

}