package api;

public class ModelData {
	private String id;
	private boolean isStepThreeExsist = false;
	private ModelStep1 step1;
	private ModelStep2 step2;
	private ModelStep3 step3;
	private ModelStep4 step4;
	private ModelStep5 step5;

	public ModelData(String id, ModelStep1 step1, ModelStep2 step2, ModelStep3 step3, ModelStep4 step4,
			ModelStep5 step5, boolean isStepThreeExsist) {
		this.id = id;
		this.step1 = step1;
		this.step2 = step2;
		this.step3 = step3;
		this.step4 = step4;
		this.step5 = step5;
		this.isStepThreeExsist = isStepThreeExsist;
	}
	
	public ModelData(String id, ModelStep1 step1, ModelStep2 step2, ModelStep4 step4,
			ModelStep5 step5, boolean isStepThreeExsist) {
		this.id = id;
		this.step1 = step1;
		this.step2 = step2;
		this.step4 = step4;
		this.step5 = step5;
		this.isStepThreeExsist = isStepThreeExsist;
	}
	
	public String getId(){
		return this.id;
	}
	
	public ModelStep1 getStep1(){
		return this.step1;
	}
	
	public ModelStep2 getStep2(){
		return this.step2;
	}
	
	public ModelStep3 getStep3(){
		return this.step3;
	}
	
	public ModelStep4 getStep4(){
		return this.step4;
	}
	
	public ModelStep5 getStep5(){
		return this.step5;
	}
	
	public boolean isStepThreeExsist(){
		return this.isStepThreeExsist;
	}
}
