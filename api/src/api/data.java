package api;

public class data {
	
     // input box
    String report_number = "//input[@id='MisparDoch']";
    
    // check box
	String check_box = "//div[@class='checkBox']//label";//body[@id='mainBody']/div[@id='bodyPrint']/div[@id='OuterDiv']/div[@id='scrolldiv']/div[@id='mainDiv']/div[@class='mobile-wrap']/div[@id='main']/div[@id='user']/div[@id='detailsRulesDiv']/div[@id='topContainer']/div[@class='content-container']/div[@id='tabpanel_']/div[@id='instructions']/div[@id='pleaseNote']/div[@id='tabpanel_']/div[@class='row']/div[@class='col-md-12']/div[@class='checkBox']/label/font[1]
	
	
	//step1 Dropdown
	String Experiment_referel_inputbox ="//input[@id='Pn_kod_sug_pniyah']";
	String Dropdown_arrow1 = "//button[@id='arrow_Pn_kod_sug_pniyah']";
	String Dropdownoption1 = "//div[@id='main']//li[1]";
	String Dropdownoption2 = "//div[@id='main']//li[2]";
	String Dropdownoption3 = "//div[@id='main']//li[3]";
	String Dropdownoption4 = "//div[@id='main']//li[4]";
	String Dropdownoption5 = "//div[@id='main']//li[5]";
	String Dropdownoption6 = "//div[@id='main']//li[6]";
	String Dropdownoption7 = "//div[@id='main']//li[7]";
	String Dropdownoption8 = "//div[@id='main']//li[8]";
	
	// next button
	String Next_BTN = "//input[@id='nextTab']";
	
	// stpe2
	// Type of applicant redio button.
	String Type_of_applicant1 ="//input[@id='senderType1']";
	String Type_of_applicant2 ="//input[@id='senderType2']";
	String Type_of_applicant3 ="//input[@id='senderType3']";
	String Type_of_applicant4 ="//input[@id='senderType4']";
	String Type_of_applicant5 ="//input[@id='senderType5']";
	String Type_of_applicant6 ="//input[@id='senderType6']";
	String Type_of_applicant7 ="//input[@id='senderType7']";
	
	//Identity type of report holder redio button.
	String identity_type_of_report_holder1 = "//input[@id='IDType1']";
	String identity_type_of_report_holder2 = "//input[@id='IDType3']";
	
	//ID holder's report input box
	String ID_holders_report ="//input[@id='Id_number']";
	
	// HF_holder_of_the_report_data input box
	String HF_holder_of_the_report_inputbox = "//input[@id='Id_number']";
	
	// company_name_of_the_report_holder
	String company_name_of_the_report_holder_inputbox="//input[@id='Company_name']";
	
	//first_name_of_the_report_owner
	String first_name_of_the_report_owner_inputbox = "//input[@id='First_name']";
	
	//last_name_of_the_report_owner
	String last_name_of_the_report_owner_inputbox = "//input[@id='Last_name']";
	
	//report_driver_license_number
	String report_driver_license_number_inputbox = "//input[@id='License_number']";
	
	// Resettlement
	String resettlement_inputbox = "//input[@id='City']";
	
	//Street
	String Street_input_box = "//input[@id='Street']";
	
	//House number
    String House_number_inputbox = "//input[@id='houseNum']";

	//apartment_number
    String apartment_number="//input[@id='apptNum']";
    
    //zip
    String zip ="//input[@id='zipCode']";
    
    //other_address
    String other_address = "//input[@id='Other_address']";
 
    //phone
    String phone_num = "//input[@id='Home_phone']";
    
    //mobile_number
    String mobile_number="//input[@id='cell_phone_nuber']";
    
    //mail 
    String mail ="//input[@id='Email_address']";
    
    
    // step 2 pra data
    // pra_ID_holder_of_the_report select redio button.
    String pra_ID_holder_of_the_report_is_id = "//input[@id='IDType1_hasava']";
    String pra_ID_holder_of_the_report_is_passport = "//input[@id='IDType2_hasava']";
    String pra_ID_holder_of_the_report_hf_private_company = "//input[@id='IDType3_hasava']";
    
    String pra_company_name_of_the_report_holder = "//input[@id='Company_name_hasava']";
    
    String pra_first_name_of_the_report_owner = "//input[@id='First_name_hasava']";
    
    String pra_last_name_of_the_report_owner = "//input[@id='Last_name_hasava']";
    
    String pra_report_driver_license_number = "pra_report_driver_license_number";
    
    String pra_resettlement = "//input[@id='City_hasava']";
    
    String pra_street = "//input[@id='Street_hasava']";
    
    String pra_house_number = "//input[@id='houseNum_hasava']";
    
    String pra_apartment_number = "//input[@id='apptNum_hasava']";
    
    String pra_zip = "//input[@id='zipCode_hasava']";
    
    String pra_other_address = "//input[@id='Other_address_hasava']";
    
    String pra_phone = "//input[@id='Home_phone_hasava']";
    
    String pra_mobile_phone = "//input[@id='cell_phone_nuber_hasava']";
    
    String pra_mail = "//input[@id='Email_address_hasava']";
    
    
    // step 3 elements
    String step_3_last_name_input_box = "//input[@id='LawyerLastName']"; 
    
    String step_3_first_name_input_box = "//input[@id='LawyerFirstName']";  
    
    String step_3_attorney_license_number_input_box = "//input[@id='LawyerLicense']";
  
    String step_3_resettlement_input_box = "//input[@id='City_Lawyer']";
    
    String step_3_street_input_box = "//input[@id='Street_Lawyer']";
  
    String step_3_house_number_input_box = "//input[@id='houseNum_Lawyer']";
    
    String step_3_apartment_number_input_box = "//input[@id='apptNum_Lawyer']"; 
    
    String step_3_zip_input_box ="//input[@id='zipCode_Lawyer']";
    
    String step_3_other_address_input_box = "//input[@id='Other_address_Lawyer']";    
    
    String step_3_phone_input_box = "//input[@id='Home_phone_Lawyer']";   
    
    String step_3_mobile_phone_input_box = "//input[@id='cell_phone_nuber_Lawyer']";   
    
    String step_3_mail_input_box = "//input[@id='Email_address_Lawyer']";
    
    // step4 input box
    String step4_Detail_inputbox = "//textarea[@id='Claim_details']";
    
    //attachment_input_box step5
    // 
	String file_photo_report_inputbox = "//input[@id='ReportPhoto_file']";   // verifyed this xpath
                                                                        
	String file_photo_id_signature_inputbox = "//input[@id='IDAndSignature_file']";// verifyed this xpath
	                                        
	String attachment_input_box = "//input[@id='ActualDriverCommitteTheOffence_file']";

	String Power_of_attorney_from_customer_inputbox = "//input[@id='ClientPowerOFAttorney_file']";

	String signed_contract_of_the_tenant_inputbox = "//input[@id='CopyContract_file']";

	String file_rental_company_reporting_inputbox = "//input[@id='RentReport_file']";
	
	String file_actual_driver_committe_the_offence ="//input[@id='ActualDriverCommitteTheOffence_file']";  // verifyed this xpath
                                               
    String file_driver_license_photo ="//input[@id='DriverLicensePhoto_file']";  // verifyed this xpath
                                      
    String file_request_to_be_judged = "//input[@id='RequestToBeJudged_file']"; // verifyed this xpath
                                       
    String file_confirmation_of_payment = "//input[@id='ConfirmationOfPayment_file']"; // verifyed this xpath
                                         
    
    String file_proof = "//input[@id='Proof_file']";  // verifyed this xpath
                        
    String file_transfer_of_ownership = "//input[@id='TransferOfOwnership_file']";
    
    String file_protocol_sign_by_lowyer = "//input[@id='ProtocolSignByLowyer_file']";
    
    String file_address_approval = "//input[@id='AddressApproval_file']"; // verifyed this xpath
    
	String validation_popup = "//div[@id='alertDialog']";

	String error_dilog_popup = "//div[@class='ui-dialog ui-corner-all ui-widget ui-widget-content ui-front ui-dialog-buttons ui-draggable']//button[@class='confirm-button ui-button ui-corner-all ui-widget']";
	                           //div[@class='ui-dialog ui-corner-all ui-widget ui-widget-content ui-front ui-dialog-buttons ui-draggable']
// after submit data 
	String after_submit_form_dilog = "//div[@id='alertDialog']";
	
	String Confirm_button ="//div[@class='ui-dialog ui-corner-all ui-widget ui-widget-content ui-front ui-dialog-buttons ui-draggable']//div[@class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix']//div[@class='ui-dialog-buttonset']//button[@class='confirm-button ui-button ui-corner-all ui-widget']//font//font[contains(text(),'Confirmation')]";

   String save_button = "//span[@class='extend-operations-toolbar']//span//a[@id='_tb_save']//span[@class='bg']//font//font[contains(text(),'Save')]";
   
   String send_button = "//button[@id='sendBtn']";
   
   String Heder_send_button = "//span[@class='text-toolbar bg']//font//font[contains(text(),'Send')]";




//   ישלח אישור לכתובת המייל המצויינת בטופס. מספר הבקשה לברורים הוא
}
