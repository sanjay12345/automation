package api;

public class ModelStep2 {
	

	//variable with status
    private boolean type_of_applicant_data_present;
    private boolean ID_holder_of_the_report_data_present;
    private boolean HF_holder_of_the_report_data_present;
    private boolean company_name_of_the_report_holder_data_present;
    private boolean first_name_of_the_report_owner_data_present;
    private boolean last_name_of_the_report_owner_data_present;
    private boolean report_driver_license_number_data_present;
    private boolean resettlement_data_present;
    private boolean street_data_present;
    private boolean house_number_data_present;
    private boolean apartment_number_data_present;
    private boolean zip_data_present;
    private boolean other_address_data_present;
    private boolean phone_data_present;
    private boolean mobile_phone_data_present;
    private boolean mail_data_present;
    private boolean identity_type_of_report_holder_data_present;

    //variable with value
    private String identityTypeOfReportHolder;
    private String typeOfApplicant;
    private String iDHolderOfTheReport;
    private String hFHolderOfTheReport;
    private String companyNameOfTheReportHolder;
    private String firstNameOfTheReportOwner;
    private String lastNameOfTheReportOwner;
    private String reportDriverLicenseNumber;
    private String resettlement;
    private String street;
    private String houseNumber;
    private String apartmentNumber;
    private String zip;
    private String otherAddress;
    private String phone;
    private String mobilePhone;
    private String mail;
    
    
    private String pra_identity_type_of_report_holder_data = "";
    private String pra_ID_holder_of_the_report_data = "";
    private String pra_HF_holder_of_the_report_data = "";
    private String pra_passport_data = "";
    private String pra_company_name_of_the_report_holder_data = "";
    private String pra_first_name_of_the_report_owner_data = "";
    private String pra_last_name_of_the_report_owner_data = "";
    private String pra_report_driver_license_number_data = "";
    private String pra_resettlement_data = "";
    private String pra_street_data = "";
    private String pra_house_number_data = "";
    private String pra_apartment_number_data = "";
    private String pra_zip_data = "";
    private String pra_other_address_data = "";
    private String pra_phone_data = "";
    private String pra_mobile_phone_data = "";
    private String pra_mail_data = "";
    
    private boolean pra_identity_type_of_report_holder_data_present;
    private boolean pra_ID_holder_of_the_report_data_present;
    private boolean pra_HF_holder_of_the_report_data_present;
    private boolean pra_passport_data_present;
    private boolean pra_company_name_of_the_report_holder_data_present;
    private boolean pra_first_name_of_the_report_owner_data_present;
    private boolean pra_last_name_of_the_report_owner_data_present;
    private boolean pra_report_driver_license_number_data_present;
    private boolean pra_resettlement_data_present;
    private boolean pra_street_data_present;
    private boolean pra_house_number_data_present;
    private boolean pra_apartment_number_data_present;
    private boolean pra_zip_data_present;
    private boolean pra_other_address_data_present;    
    private boolean pra_phone_data_present;
    private boolean pra_mobile_phone_data_present;
    private boolean pra_mail_data_present;
    
    public ModelStep2(String typeOfApplicant, boolean type_of_applicant_data_present,
                      String iDHolderOfTheReport, boolean ID_holder_of_the_report_data_present,
                      String hFHolderOfTheReport, boolean HF_holder_of_the_report_data_present,
                      String companyNameOfTheReportHolder, boolean company_name_of_the_report_holder_data_present,
                      String firstNameOfTheReportOwner, boolean first_name_of_the_report_owner_data_present,
                      String lastNameOfTheReportOwner, boolean last_name_of_the_report_owner_data_present,
                      String reportDriverLicenseNumber, boolean report_driver_license_number_data_present,
                      String resettlement, boolean resettlement_data_present,
                      String street, boolean street_data_present,
                      String houseNumber, boolean house_number_data_present,
                      String apartmentNumber, boolean apartment_number_data_present,
                      String zip, boolean zip_data_present,
                      String otherAddress, boolean other_address_data_present,
                      String phone, boolean phone_data_present,
                      String mobilePhone, boolean mobile_phone_data_present,
                      String mail, boolean mail_data_present,
                      String identityTypeOfReportHolder, boolean identity_type_of_report_holder_data_present,
                      
                      String pra_identity_type_of_report_holder_data,boolean pra_identity_type_of_report_holder_data_present,
                      String pra_ID_holder_of_the_report_data, boolean pra_ID_holder_of_the_report_data_present,
                      String pra_HF_holder_of_the_report_data,boolean pra_HF_holder_of_the_report_data_present,
                      String pra_passport_data, boolean pra_passport_data_present,
                      String pra_company_name_of_the_report_holder_data, boolean pra_company_name_of_the_report_holder_data_present, 
                      String pra_first_name_of_the_report_owner_data, boolean pra_first_name_of_the_report_owner_data_present,
                      String pra_last_name_of_the_report_owner_data, boolean pra_last_name_of_the_report_owner_data_present,
                      String pra_report_driver_license_number_data, boolean pra_report_driver_license_number_data_present,
                      String pra_resettlement_data, boolean pra_resettlement_data_present,
                      String pra_street_data,  boolean pra_street_data_present,
                      String pra_house_number_data, boolean pra_house_number_data_present,
                      String pra_apartment_number_data, boolean pra_apartment_number_data_present,
                      String pra_zip_data, boolean pra_zip_data_present, 
                      String pra_other_address_data, boolean pra_other_address_data_present,
                      String pra_phone_data, boolean pra_phone_data_present,
                      String pra_mobile_phone_data, boolean pra_mobile_phone_data_present,
                      String pra_mail_data, boolean pra_mail_data_present
                      ) {

        this.identityTypeOfReportHolder = identityTypeOfReportHolder;
        this.typeOfApplicant = typeOfApplicant;
        this.iDHolderOfTheReport = iDHolderOfTheReport;
        this.hFHolderOfTheReport = hFHolderOfTheReport;
        this.companyNameOfTheReportHolder = companyNameOfTheReportHolder;
        this.firstNameOfTheReportOwner = firstNameOfTheReportOwner;
        this.lastNameOfTheReportOwner = lastNameOfTheReportOwner;
        this.reportDriverLicenseNumber = reportDriverLicenseNumber;
        this.resettlement = resettlement;
        this.street = street;
        this.houseNumber = houseNumber;
        this.apartmentNumber = apartmentNumber;
        this.zip = zip;
        this.otherAddress = otherAddress;
        this.phone = phone;
        this.mobilePhone = mobilePhone;
        this.mail = mail;
        
        //extra string value
        this.pra_identity_type_of_report_holder_data = pra_identity_type_of_report_holder_data;
        this.pra_ID_holder_of_the_report_data = pra_ID_holder_of_the_report_data;
        this.pra_HF_holder_of_the_report_data = pra_HF_holder_of_the_report_data;
        this.pra_passport_data = pra_passport_data;
        this.pra_company_name_of_the_report_holder_data = pra_company_name_of_the_report_holder_data;
        this.pra_first_name_of_the_report_owner_data = pra_first_name_of_the_report_owner_data;
        this.pra_last_name_of_the_report_owner_data = pra_last_name_of_the_report_owner_data;
        this.pra_report_driver_license_number_data = pra_report_driver_license_number_data;
        this.pra_resettlement_data = pra_resettlement_data;
        this.pra_street_data = pra_street_data;
        this.pra_house_number_data = pra_house_number_data;
        this.pra_apartment_number_data = pra_apartment_number_data;
        this.pra_zip_data = pra_zip_data;
        this.pra_other_address_data = pra_other_address_data;
        this.pra_phone_data = pra_phone_data;
        this.pra_mobile_phone_data = pra_mobile_phone_data;
        this.pra_mail_data = pra_mail_data;

        //boolean value
        this.type_of_applicant_data_present = type_of_applicant_data_present;
        this.ID_holder_of_the_report_data_present = ID_holder_of_the_report_data_present;
        this.HF_holder_of_the_report_data_present = HF_holder_of_the_report_data_present;
        this.company_name_of_the_report_holder_data_present = company_name_of_the_report_holder_data_present;
        this.first_name_of_the_report_owner_data_present = first_name_of_the_report_owner_data_present;
        this.last_name_of_the_report_owner_data_present = last_name_of_the_report_owner_data_present;
        this.report_driver_license_number_data_present = report_driver_license_number_data_present;
        this.resettlement_data_present = resettlement_data_present;
        this.street_data_present = street_data_present;
        this.house_number_data_present = house_number_data_present;
        this.apartment_number_data_present = apartment_number_data_present;
        this.zip_data_present = zip_data_present;
        this.other_address_data_present = other_address_data_present;
        this.phone_data_present = phone_data_present;
        this.mobile_phone_data_present = mobile_phone_data_present;
        this.mail_data_present = mail_data_present;
        this.identity_type_of_report_holder_data_present = identity_type_of_report_holder_data_present;
        
        this.pra_identity_type_of_report_holder_data_present = pra_identity_type_of_report_holder_data_present;
        this.pra_ID_holder_of_the_report_data_present = pra_ID_holder_of_the_report_data_present;
        this.pra_HF_holder_of_the_report_data_present = pra_HF_holder_of_the_report_data_present;
        this.pra_passport_data_present = pra_passport_data_present;
        this.pra_company_name_of_the_report_holder_data_present = pra_company_name_of_the_report_holder_data_present;
        this.pra_first_name_of_the_report_owner_data_present = pra_first_name_of_the_report_owner_data_present;
        this.pra_last_name_of_the_report_owner_data_present = pra_last_name_of_the_report_owner_data_present;
        this.pra_report_driver_license_number_data_present = pra_report_driver_license_number_data_present;
        this.pra_resettlement_data_present = pra_resettlement_data_present;
        this.pra_street_data_present = pra_street_data_present;
        this.pra_house_number_data_present = pra_house_number_data_present;
        this.pra_apartment_number_data_present = pra_apartment_number_data_present;
        this.pra_zip_data_present = pra_zip_data_present;
        this.pra_other_address_data_present = pra_other_address_data_present;    
        this.pra_phone_data_present = pra_phone_data_present;
        this.pra_mobile_phone_data_present = pra_mobile_phone_data_present;
        this.pra_mail_data_present = pra_mail_data_present;
    }

    public boolean isType_of_applicant_data_present() {
        return type_of_applicant_data_present;
    }

    public void setType_of_applicant_data_present(boolean type_of_applicant_data_present) {
        this.type_of_applicant_data_present = type_of_applicant_data_present;
    }
    
    public boolean isID_holder_of_the_report_data_present() {
        return ID_holder_of_the_report_data_present;
    }

    public void setID_holder_of_the_report_data_present(boolean ID_holder_of_the_report_data_present) {
        this.ID_holder_of_the_report_data_present = ID_holder_of_the_report_data_present;
    }

    public boolean isHF_holder_of_the_report_data_present() {
        return HF_holder_of_the_report_data_present;
    }

    public void setHF_holder_of_the_report_data_present(boolean HF_holder_of_the_report_data_present) {
        this.HF_holder_of_the_report_data_present = HF_holder_of_the_report_data_present;
    }

    public boolean isCompany_name_of_the_report_holder_data_present() {
        return company_name_of_the_report_holder_data_present;
    }

    public void setCompany_name_of_the_report_holder_data_present(boolean company_name_of_the_report_holder_data_present) {
        this.company_name_of_the_report_holder_data_present = company_name_of_the_report_holder_data_present;
    }

    public boolean isFirst_name_of_the_report_owner_data_present() {
        return first_name_of_the_report_owner_data_present;
    }

    public void setFirst_name_of_the_report_owner_data_present(boolean first_name_of_the_report_owner_data_present) {
        this.first_name_of_the_report_owner_data_present = first_name_of_the_report_owner_data_present;
    }

    public boolean isLast_name_of_the_report_owner_data_present() {
        return last_name_of_the_report_owner_data_present;
    }

    public void setLast_name_of_the_report_owner_data_present(boolean last_name_of_the_report_owner_data_present) {
        this.last_name_of_the_report_owner_data_present = last_name_of_the_report_owner_data_present;
    }

    public boolean isReport_driver_license_number_data_present() {
        return report_driver_license_number_data_present;
    }

    public void setReport_driver_license_number_data_present(boolean report_driver_license_number_data_present) {
        this.report_driver_license_number_data_present = report_driver_license_number_data_present;
    }

    public boolean isResettlement_data_present() {
        return resettlement_data_present;
    }

    public void setResettlement_data_present(boolean resettlement_data_present) {
        this.resettlement_data_present = resettlement_data_present;
    }

    public boolean isStreet_data_present() {
        return street_data_present;
    }

    public void setStreet_data_present(boolean street_data_present) {
        this.street_data_present = street_data_present;
    }

    public boolean isHouse_number_data_present() {
        return house_number_data_present;
    }

    public void setHouse_number_data_present(boolean house_number_data_present) {
        this.house_number_data_present = house_number_data_present;
    }

    public boolean isApartment_number_data_present() {
        return apartment_number_data_present;
    }

    public void setApartment_number_data_present(boolean apartment_number_data_present) {
        this.apartment_number_data_present = apartment_number_data_present;
    }

    public boolean isZip_data_present() {
        return zip_data_present;
    }

    public void setZip_data_present(boolean zip_data_present) {
        this.zip_data_present = zip_data_present;
    }

    public boolean isOther_address_data_present() {
        return other_address_data_present;
    }

    public void setOther_address_data_present(boolean other_address_data_present) {
        this.other_address_data_present = other_address_data_present;
    }

    public boolean isPhone_data_present() {
        return phone_data_present;
    }

    public void setPhone_data_present(boolean phone_data_present) {
        this.phone_data_present = phone_data_present;
    }

    public boolean isMobile_phone_data_present() {
        return mobile_phone_data_present;
    }

    public void setMobile_phone_data_present(boolean mobile_phone_data_present) {
        this.mobile_phone_data_present = mobile_phone_data_present;
    }

    public boolean isMail_data_present() {
        return mail_data_present;
    }

    public void setMail_data_present(boolean mail_data_present) {
        this.mail_data_present = mail_data_present;
    }

    public boolean isIdentity_type_of_report_holder_data_present() {
        return identity_type_of_report_holder_data_present;
    }

    public void setIdentity_type_of_report_holder_data_present(boolean identity_type_of_report_holder_data_present) {
        this.identity_type_of_report_holder_data_present = identity_type_of_report_holder_data_present;
    }

    public String getIdentityTypeOfReportHolder() {
        return identityTypeOfReportHolder;
    }

    public void setIdentityTypeOfReportHolder(String identityTypeOfReportHolder) {
        this.identityTypeOfReportHolder = identityTypeOfReportHolder;
    }

    public String getTypeOfApplicant() {
        return typeOfApplicant;
    }

    public void setTypeOfApplicant(String typeOfApplicant) {
        this.typeOfApplicant = typeOfApplicant;
    }

    public String getIDHolderOfTheReport() {
        return iDHolderOfTheReport;
    }

    public void setiDHolderOfTheReport(String iDHolderOfTheReport) {
        this.iDHolderOfTheReport = iDHolderOfTheReport;
    }

    public String getHFHolderOfTheReport() {
        return hFHolderOfTheReport;
    }

    public void sethFHolderOfTheReport(String hFHolderOfTheReport) {
        this.hFHolderOfTheReport = hFHolderOfTheReport;
    }

    public String getCompanyNameOfTheReportHolder() {
        return companyNameOfTheReportHolder;
    }

    public void setCompanyNameOfTheReportHolder(String companyNameOfTheReportHolder) {
        this.companyNameOfTheReportHolder = companyNameOfTheReportHolder;
    }

    public String getFirstNameOfTheReportOwner() {
        return firstNameOfTheReportOwner;
    }

    public void setFirstNameOfTheReportOwner(String firstNameOfTheReportOwner) {
        this.firstNameOfTheReportOwner = firstNameOfTheReportOwner;
    }

    public String getLastNameOfTheReportOwner() {
        return lastNameOfTheReportOwner;
    }

    public void setLastNameOfTheReportOwner(String lastNameOfTheReportOwner) {
        this.lastNameOfTheReportOwner = lastNameOfTheReportOwner;
    }

    public String getReportDriverLicenseNumber() {
        return reportDriverLicenseNumber;
    }

    public void setReportDriverLicenseNumber(String reportDriverLicenseNumber) {
        this.reportDriverLicenseNumber = reportDriverLicenseNumber;
    }

    public String getResettlement() {
        return resettlement;
    }

    public void setResettlement(String resettlement) {
        this.resettlement = resettlement;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getApartmentNumber() {
        return apartmentNumber;
    }

    public void setApartmentNumber(String apartmentNumber) {
        this.apartmentNumber = apartmentNumber;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getOtherAddress() {
        return otherAddress;
    }

    public void setOtherAddress(String otherAddress) {
        this.otherAddress = otherAddress;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
    
    //extras step data methods
    public String getPra_identity_type_of_report_holder_data() {
        return pra_identity_type_of_report_holder_data;
    }

    public void setPra_identity_type_of_report_holder_data(String pra_identity_type_of_report_holder_data) {
        this.pra_identity_type_of_report_holder_data = pra_identity_type_of_report_holder_data;
    }

    public String getPra_ID_holder_of_the_report_data() {
        return pra_ID_holder_of_the_report_data;
    }

    public void setPra_ID_holder_of_the_report_data(String pra_ID_holder_of_the_report_data) {
        this.pra_ID_holder_of_the_report_data = pra_ID_holder_of_the_report_data;
    }

    public String getPra_HF_holder_of_the_report_data() {
        return pra_HF_holder_of_the_report_data;
    }

    public void setPra_HF_holder_of_the_report_data(String pra_HF_holder_of_the_report_data) {
        this.pra_HF_holder_of_the_report_data = pra_HF_holder_of_the_report_data;
    }

    public String getPra_passport_data() {
        return pra_passport_data;
    }

    public void setPra_passport_data(String pra_passport_data) {
        this.pra_passport_data = pra_passport_data;
    }

    public String getPra_company_name_of_the_report_holder_data() {
        return pra_company_name_of_the_report_holder_data;
    }

    public void setPra_company_name_of_the_report_holder_data(String pra_company_name_of_the_report_holder_data) {
        this.pra_company_name_of_the_report_holder_data = pra_company_name_of_the_report_holder_data;
    }

    public String getPra_first_name_of_the_report_owner_data() {
        return pra_first_name_of_the_report_owner_data;
    }

    public void setPra_first_name_of_the_report_owner_data(String pra_first_name_of_the_report_owner_data) {
        this.pra_first_name_of_the_report_owner_data = pra_first_name_of_the_report_owner_data;
    }

    public String getPra_last_name_of_the_report_owner_data() {
        return pra_last_name_of_the_report_owner_data;
    }

    public void setPra_last_name_of_the_report_owner_data(String pra_last_name_of_the_report_owner_data) {
        this.pra_last_name_of_the_report_owner_data = pra_last_name_of_the_report_owner_data;
    }

    public String getPra_report_driver_license_number_data() {
        return pra_report_driver_license_number_data;
    }

    public void setPra_report_driver_license_number_data(String pra_report_driver_license_number_data) {
        this.pra_report_driver_license_number_data = pra_report_driver_license_number_data;
    }

    public String getPra_resettlement_data() {
        return pra_resettlement_data;
    }

    public void setPra_resettlement_data(String pra_resettlement_data) {
        this.pra_resettlement_data = pra_resettlement_data;
    }

    public String getPra_street_data() {
        return pra_street_data;
    }

    public void setPra_street_data(String pra_street_data) {
        this.pra_street_data = pra_street_data;
    }

    public String getPra_house_number_data() {
        return pra_house_number_data;
    }

    public void setPra_house_number_data(String pra_house_number_data) {
        this.pra_house_number_data = pra_house_number_data;
    }

    public String getPra_apartment_number_data() {
        return pra_apartment_number_data;
    }

    public void setPra_apartment_number_data(String pra_apartment_number_data) {
        this.pra_apartment_number_data = pra_apartment_number_data;
    }

    public String getPra_zip_data() {
        return pra_zip_data;
    }

    public void setPra_zip_data(String pra_zip_data) {
        this.pra_zip_data = pra_zip_data;
    }

    public String getPra_other_address_data() {
        return pra_other_address_data;
    }

    public void setPra_other_address_data(String pra_other_address_data) {
        this.pra_other_address_data = pra_other_address_data;
    }

    public String getPra_phone_data() {
        return pra_phone_data;
    }

    public void setPra_phone_data(String pra_phone_data) {
        this.pra_phone_data = pra_phone_data;
    }

    public String getPra_mobile_phone_data() {
        return pra_mobile_phone_data;
    }

    public void setPra_mobile_phone_data(String pra_mobile_phone_data) {
        this.pra_mobile_phone_data = pra_mobile_phone_data;
    }

    public String getPra_mail_data() {
        return pra_mail_data;
    }

    public void setPra_mail_data(String pra_mail_data) {
        this.pra_mail_data = pra_mail_data;
    }

    public boolean isPra_identity_type_of_report_holder_data_present() {
        return pra_identity_type_of_report_holder_data_present;
    }

    public void setPra_identity_type_of_report_holder_data_present(boolean pra_identity_type_of_report_holder_data_present) {
        this.pra_identity_type_of_report_holder_data_present = pra_identity_type_of_report_holder_data_present;
    }

    public boolean isPra_ID_holder_of_the_report_data_present() {
        return pra_ID_holder_of_the_report_data_present;
    }

    public void setPra_ID_holder_of_the_report_data_present(boolean pra_ID_holder_of_the_report_data_present) {
        this.pra_ID_holder_of_the_report_data_present = pra_ID_holder_of_the_report_data_present;
    }

    public boolean isPra_HF_holder_of_the_report_data_present() {
        return pra_HF_holder_of_the_report_data_present;
    }

    public void setPra_HF_holder_of_the_report_data_present(boolean pra_HF_holder_of_the_report_data_present) {
        this.pra_HF_holder_of_the_report_data_present = pra_HF_holder_of_the_report_data_present;
    }

    public boolean isPra_passport_data_present() {
        return pra_passport_data_present;
    }

    public void setPra_passport_data_present(boolean pra_passport_data_present) {
        this.pra_passport_data_present = pra_passport_data_present;
    }

    public boolean isPra_company_name_of_the_report_holder_data_present() {
        return pra_company_name_of_the_report_holder_data_present;
    }

    public void setPra_company_name_of_the_report_holder_data_present(boolean pra_company_name_of_the_report_holder_data_present) {
        this.pra_company_name_of_the_report_holder_data_present = pra_company_name_of_the_report_holder_data_present;
    }

    public boolean isPra_first_name_of_the_report_owner_data_present() {
        return pra_first_name_of_the_report_owner_data_present;
    }

    public void setPra_first_name_of_the_report_owner_data_present(boolean pra_first_name_of_the_report_owner_data_present) {
        this.pra_first_name_of_the_report_owner_data_present = pra_first_name_of_the_report_owner_data_present;
    }

    public boolean isPra_last_name_of_the_report_owner_data_present() {
        return pra_last_name_of_the_report_owner_data_present;
    }

    public void setPra_last_name_of_the_report_owner_data_present(boolean pra_last_name_of_the_report_owner_data_present) {
        this.pra_last_name_of_the_report_owner_data_present = pra_last_name_of_the_report_owner_data_present;
    }

    public boolean isPra_report_driver_license_number_data_present() {
        return pra_report_driver_license_number_data_present;
    }

    public void setPra_report_driver_license_number_data_present(boolean pra_report_driver_license_number_data_present) {
        this.pra_report_driver_license_number_data_present = pra_report_driver_license_number_data_present;
    }

    public boolean isPra_resettlement_data_present() {
        return pra_resettlement_data_present;
    }

    public void setPra_resettlement_data_present(boolean pra_resettlement_data_present) {
        this.pra_resettlement_data_present = pra_resettlement_data_present;
    }

    public boolean isPra_street_data_present() {
        return pra_street_data_present;
    }

    public void setPra_street_data_present(boolean pra_street_data_present) {
        this.pra_street_data_present = pra_street_data_present;
    }

    public boolean isPra_house_number_data_present() {
        return pra_house_number_data_present;
    }

    public void setPra_house_number_data_present(boolean pra_house_number_data_present) {
        this.pra_house_number_data_present = pra_house_number_data_present;
    }

    public boolean isPra_apartment_number_data_present() {
        return pra_apartment_number_data_present;
    }

    public void setPra_apartment_number_data_present(boolean pra_apartment_number_data_present) {
        this.pra_apartment_number_data_present = pra_apartment_number_data_present;
    }

    public boolean isPra_zip_data_present() {
        return pra_zip_data_present;
    }

    public void setPra_zip_data_present(boolean pra_zip_data_present) {
        this.pra_zip_data_present = pra_zip_data_present;
    }

    public boolean isPra_other_address_data_present() {
        return pra_other_address_data_present;
    }

    public void setPra_other_address_data_present(boolean pra_other_address_data_present) {
        this.pra_other_address_data_present = pra_other_address_data_present;
    }

    public boolean isPra_phone_data_present() {
        return pra_phone_data_present;
    }

    public void setPra_phone_data_present(boolean pra_phone_data_present) {
        this.pra_phone_data_present = pra_phone_data_present;
    }

    public boolean isPra_mobile_phone_data_present() {
        return pra_mobile_phone_data_present;
    }

    public void setPra_mobile_phone_data_present(boolean pra_mobile_phone_data_present) {
        this.pra_mobile_phone_data_present = pra_mobile_phone_data_present;
    }

    public boolean isPra_mail_data_present() {
        return pra_mail_data_present;
    }

    public void setPra_mail_data_present(boolean pra_mail_data_present) {
        this.pra_mail_data_present = pra_mail_data_present;
    }

   
}
